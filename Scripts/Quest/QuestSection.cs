﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestSection
{
    public string   sectionID;
    public string   description;
    public int      goalCount;
    public bool     complete;

    public List<QuestGoal> goals = new List<QuestGoal>();

    public QuestSection(QuestSectionData sectionData)
    {
        sectionID   = sectionData.sectionID;
        description = sectionData.description;
        goalCount   = sectionData.goalCount;
        complete    = false;

        for(int i = 0; i< goalCount; i++)
        {
            QuestGoal temp = new QuestGoal(TableMng.Ins.questGoalTb[string.Format("{0}_{1}", sectionID, i)]);
            goals.Add(temp);
        }
    }

    public bool CheckComplete()
    {
        for(int i = 0; i < goals.Count; i++)
        {
            if (goals[i].complete == false)
                return false;
        }
        complete = true;
        UIMng.Ins.questHud.CompleteCheck();
        return true;
    }
}
