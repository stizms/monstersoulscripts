﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGoal
{
    public string       goalID;
    public eGoalType    goalType;
    public string       description;
    public bool         complete;
    public int          currentAmount;
    public int          requiredAmount;
    public eMonsterID   targetMonster;  // goalType이 KillGoal 이라면 사용 ( 타겟 적 )
    public eItemID  targetCollection;   // goalType이 CollectionGoal 이라면 사용 ( 타겟 수집품 )

    public QuestGoal(QuestGoalData goalData)
    {
        goalID           = goalData.goalID;
        goalType         = goalData.goalType;
        description      = goalData.description;
        complete         = false;
        currentAmount    = 0;
        requiredAmount   = goalData.requiredAmount;
        targetMonster    = goalData.targetMonster;
        targetCollection = goalData.targetCollection;
    }

    // 끝났는지 체크
    public void CheckComplete()
    {
        if (currentAmount >= requiredAmount)    complete = true;
    }

    // 몬스터가 죽었을 때 호출 되는 함수
    public void MonsterDied(eMonsterID deathEnemy)
    {
        if (goalType != eGoalType.HuntGoal) return;

        if(targetMonster == deathEnemy && complete == false)
        {
            currentAmount++;
            UIMng.Ins.alarmHud.AddQuestAlarm(this);
            CheckComplete();
        }
    }

    // 아이템을 주웠을 때 호출되는 함수
    public void PickedUpItem(eItemID pickedItem,int amount)
    {
        if (goalType != eGoalType.CollectionGoal)   return;
        if(targetCollection == pickedItem && complete == false)
        {
            if (currentAmount + amount > requiredAmount)
                currentAmount = requiredAmount;
            else
                currentAmount += amount;
            UIMng.Ins.alarmHud.AddQuestAlarm(this);
            CheckComplete();
        }
    }
}
