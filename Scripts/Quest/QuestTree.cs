﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class QuestNode
{
    public Quest            quest;
    public List<QuestNode>  nextQuests;

    public QuestNode(eQuestID questID)
    {
        quest       = new Quest(questID);
        nextQuests  = new List<QuestNode>();

        if(UserDataMng.Ins.nowCharacter.clearQuestList.ContainsKey(questID))
        {
            quest.complete = true;
            OpenQuest();
        }
    }

    // 해당 퀘스트를 클리어하면 연결된 퀘스트를 오픈 해주어라.
    public void OpenQuest()
    {
        if (quest.complete)
        {
            foreach(var questList in quest.openQuest)
            {
                if (questList == eQuestID.None) continue;
                else
                    nextQuests.Add(new QuestNode(questList));
            }
        }
        else
            return;
    }
}

public class QuestTree
{
    public QuestNode root = null;
    private Queue<QuestNode> questQueue;
    
    public QuestTree()
    {
        root = new QuestNode(eQuestID.Q0);
    }

    // 너비우선 순회
    public List<QuestNode> GetQuestList()
    {
        questQueue = new Queue<QuestNode>();
        List<QuestNode> questList = new List<QuestNode>();

        questQueue.Enqueue(root);
        while (questQueue.Count >0)
        {
            QuestNode temp = questQueue.Dequeue();
            questList.Add(temp);
            foreach(var node in temp.nextQuests)
            {
                questQueue.Enqueue(node);
            }
        }
        return questList;
    }
}

