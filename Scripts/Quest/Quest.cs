﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest
{
    public eQuestID             questID;
    public string               questName;
    public string               description;
    public eQuestType           questType;
    public Queue<QuestSection>  sections = new Queue<QuestSection>();
    public int                  sectionCount;   // 섹션 개수
    public int                  rewardMoney;
    public int                  questLife;
    public List<eQuestID>       openQuest = new List<eQuestID>();
    public int                  hunterRank;
    public bool                 rankUpFlag;
    public eScene               relatedDungeon;
    public bool                 isBossQuest;
    public eQuestTarget         mainTarget;
    public bool                 complete;
    

    public Quest(eQuestID id)
    {
        if (id == eQuestID.None)    return;

        Init(TableMng.Ins.questTb[id]);
        for (int i = 0; i < sectionCount; i++)
        {
            QuestSection temp = new QuestSection(TableMng.Ins.questSectionTb[string.Format("{0}_{1}", questID, i)]);
            sections.Enqueue(temp);
        }
    }

    private void Init(QuestData data)
    {
        questID         = data.questID;
        questName       = data.questName;
        description     = data.description;
        questType       = data.questType;
        sectionCount    = data.sectionCount;
        rewardMoney     = data.rewardMoney;
        questLife       = data.questLife;
        foreach(var questList in data.openQuest)
        {
            openQuest.Add(questList);
        }
        hunterRank      = data.hunterRank;
        rankUpFlag      = data.rankUpFlag;
        relatedDungeon  = data.relatedDungeon;
        isBossQuest     = data.isBossQuest;
        mainTarget      = data.mainTarget;
        complete        = false;
    }

    // 초기화
    public void ResetQuest()
    {
        sections.Clear();

        for (int i = 0; i < sectionCount; i++)
        {
            QuestSection temp = new QuestSection(TableMng.Ins.questSectionTb[string.Format("{0}_{1}", questID, i)]);
            sections.Enqueue(temp);
        }
    }

    public void Clear()
    {
        DungeonMng.Ins.QuestClear();
        complete = true;
        UserDataMng.Ins.nowCharacter.AddClearQuestList(questID);
    }
}
