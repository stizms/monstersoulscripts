﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseWeapon : MonoBehaviour
{
    // 웨폰 이름
    virtual public eEquipment weaponID { get { return (eEquipment)Enum.Parse(typeof(eEquipment), parsingWeaponName); } }
    // 파싱할 때 필요
    private string parsingWeaponName;
    
    // 무기 카테고리 ( 대검,차지액스 등 )
    public eWeaponCategory  weaponCategory;
    public float            originDamage;
    public float            currentDamage;
    public eRare            rare;
    public eElement         element;
    public float            MAX_Sharpness;
    public float            currentSharpness;

    #region INSPECTOR

    // 무기 리소스의 발도 & 납도 위치 정보 ( 스크럽터블 오브젝트 )
    public WeaponPos        weaponPos;
    public Collider         weaponCollider;
    public ParticleSystem   trail;

    #endregion

    public CharacterMovement movement;

    void Awake()
    {
        parsingWeaponName = name.Substring(0, name.IndexOf("(Clone)"));

        WeaponData weaponTD = TableMng.Ins.weaponTb[weaponID];
        weaponCategory      = weaponTD.weaponCategory;
        originDamage        = weaponTD.damage;
        currentDamage       = originDamage;
        rare                = weaponTD.rare;
        element             = weaponTD.element;
        MAX_Sharpness       = weaponTD.sharpness;
        currentSharpness    = MAX_Sharpness;
        trail.Stop();
    }

    // 무기 시작 위치 세팅
    public virtual void SetSheatingPos(Character character)
    {
        transform.parent        = character.backWeaponPos;
        transform.localScale    = new Vector3(1, 1, 1);
        transform.localPosition = weaponPos.unEquipPos;
        transform.localRotation = Quaternion.Euler(weaponPos.unEquipRot);
    }

    // 무기 뽑앗을 때 위치 세팅
    public virtual void SetDrawPos(Character character)
    {
        transform.parent          = character.rightWeaponPos;
        transform.localPosition   = weaponPos.equipPos;
        transform.localRotation   = Quaternion.Euler(weaponPos.equipRot);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Monster"))
        {
            Vector3 pos = collision.contacts[0].point;
            if(collision.gameObject.GetComponent<BaseCollider>().Hit(pos, movement, currentDamage))
            {
                SoundMng.Ins.InternalSoundPlay(eSound.HitSound.ToDesc(), movement.playerAudioSource);
                if(currentSharpness > 0)
                {
                    currentSharpness--;
                    // 10은 각 예리도 마다의 할당량 ( 10의 텀으로 예리도가 바뀜 )
                    if (currentSharpness % 10 == 0)
                        ChangeDamage();
                    UIMng.Ins.dungeon.characterInfoUI.sharpnessUI.UpdateSharpnessGaze();
                }
            }
        }
    }

    private void ChangeDamage()
    {
        // 0부터 빨주노초파흰보 순
        // 각 예리도에서 originDamage에 비해 데미지가 몇배가 되는지 설정

        switch(currentSharpness/10)
        {
            case 1:
                currentDamage = (int)(originDamage * 0.5f);
                break;
            case 2:
                currentDamage = (int)(originDamage * 0.75f);
                break;
            case 3:
                currentDamage = originDamage;
                break;
            case 4:
                currentDamage = (int)originDamage * 1.05f;
                break;
            case 5:
                currentDamage = (int)originDamage * 1.2f;
                break;
            case 6:
                currentDamage = (int)originDamage * 1.32f;
                break;
            case 7:
                currentDamage = (int)originDamage * 1.39f;
                break;
        }
    }

    public void SharpnessFullRecovery()
    {
        currentSharpness = MAX_Sharpness;
        ChangeDamage();
        UIMng.Ins.dungeon.characterInfoUI.sharpnessUI.GazeUpdateWithWhetStone();
    }
}