﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New WeaponPos", menuName = "WeaponPos")]
public class WeaponPos : ScriptableObject
{
    public Vector3 equipPos;
    public Vector3 equipRot;
    public Vector3 unEquipPos;
    public Vector3 unEquipRot;
}
