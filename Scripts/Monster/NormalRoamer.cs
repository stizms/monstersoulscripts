﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalRoamer : BaseRoamer
{
    public override Vector3 GetRoamingPoint()
    {
        Vector2 temp = (Random.insideUnitCircle * 12f);
        Vector3 targetPos = transform.position + new Vector3(temp.x, 0f, temp.y);
        return new Vector3(targetPos.x, 0, targetPos.z);
    }
}
