﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BTAI;
using UnityEngine.Playables;
using System.Linq;
using Cinemachine;

public class Rathian : BaseMonster
{
    #region INSPECTOR

    public Transform projectilePos;     // 파이어볼 생성 포지션

    #endregion

    private int     escapeCount         = 1;        // 최대 도망 횟수
    private int     flyingAttackCount   = 2;  // 두번 공격하고 땅으로 내려옴
    private float   biteDistance        = 13f;
    private bool    isFlying            = false;
    
    private void OnEnable()
    {
        if (isFirstTime)
        {
            Init();

            ai = BT.Root();
            ai.OpenBranch(
                // 살아 있는 동안
                BT.While(IsAlive).OpenBranch(
                    BT.Selector().OpenBranch(
                        // 도망갈 각이라면!
                        BT.If(CheckEscape).OpenBranch(
                            BT.SetBool(anim, "Fly_Stationary", false),
                            BT.SetBool(anim, "Walk", false),
                            BT.SetBool(anim, "Fly", true),
                            BT.Call(SetRotatingFalse),
                            BT.Call(SetStateEscape)
                        ),
                        BT.If(IsEscape).OpenBranch(
                            BT.Sequence().OpenBranch(
                                BT.Condition(IsNearTargetPos),
                                BT.Root().OpenBranch(
                                    BT.SetBool(anim, "Fly", false),
                                    BT.WaitForAnimatorState(anim, "Idle"),
                                    BT.Wait(2),
                                    BT.Call(SetStateIdle)
                                )
                            )
                        ),
                        BT.If(IsContacting).OpenBranch(
                            BT.If(IsNearContactingPos).OpenBranch(
                                BT.SetBool(anim, "Walk", false),
                                BT.Call(SetStateIdle)
                            )
                        ),
                        BT.If(IsRotating).OpenBranch(
                            BT.If(IsInSight).OpenBranch(
                                BT.Call(SetRotatingFalse)
                            )
                        ),
                        // 감지 거리 안에 적이 있는 경우
                        BT.Selector().OpenBranch(
                            BT.If(IsInDetectionRange).OpenBranch(
                                BT.Sequence().OpenBranch(
                                    BT.Condition(IsBGMChange),
                                    BT.Call(BGMChange)
                                ),
                                BT.Selector().OpenBranch(
                                    BT.If(IsInAttackRange).OpenBranch(
                                        BT.Selector().OpenBranch(
                                            BT.Sequence().OpenBranch(
                                                BT.Condition(IsInSight),
                                                BT.Selector().OpenBranch(
                                                    BT.If(IsFlying).OpenBranch(
                                                        BT.Call(() => { agent.SetDestination(transform.position); }),
                                                        BT.Repeat(flyingAttackCount).OpenBranch(
                                                            BT.SetBool(anim, "Fly_Stationary", true),
                                                            BT.Call(SetRotatingTrue),
                                                            BT.Wait(2),
                                                            BT.Call(SetRotatingFalse),
                                                            BT.Call(SetStateAttack),
                                                            BT.Trigger(anim, "SpecialAttack"),
                                                            BT.WaitForAnimatorState(anim, "FlyStationary"),
                                                            BT.Call(SetStateIdle)
                                                        ),
                                                        // 땅으로 착륙!
                                                        BT.SetBool(anim, "Fly_Stationary", false),
                                                        BT.SetBool(anim, "Fly", false),
                                                        BT.SetBool(anim, "Walk", false),
                                                        BT.WaitForAnimatorState(anim, "Idle"),
                                                        BT.Call(SetFlyState),
                                                        BT.Call(SetStateIdle)
                                                    ),
                                                    // 랜덤으로 날거나, 땅에서 공격
                                                    BT.RandomSequence(new int[] { 6, 5 }).OpenBranch(
                                                        BT.Root().OpenBranch(
                                                            BT.SetBool(anim, "Walk", false),
                                                            BT.Call(SetStateAttack),
                                                            BT.Selector().OpenBranch(
                                                                BT.If(IsInBiteRange).OpenBranch(
                                                                    BT.RandomSequence(new int[] { 1, 2 }).OpenBranch(
                                                                        BT.Wait(0f),
                                                                        BT.Wait(1f)
                                                                    ),
                                                                    BT.Trigger(anim, "NormalAttack"),
                                                                    BT.WaitForAnimatorState(anim, "Idle"),
                                                                    BT.Wait(1f),
                                                                    BT.Call(SetStateIdle)
                                                                ),
                                                                BT.RandomSequence(new int[] { 5, 6 }).OpenBranch(
                                                                    // 더 가까이 가기
                                                                    BT.Root().OpenBranch(
                                                                        BT.SetBool(anim, "Walk", true),
                                                                        BT.Call(SetStateContacting)
                                                                    ),
                                                                    // 멀리서 공격하기
                                                                    BT.Root().OpenBranch(
                                                                        BT.Trigger(anim, "SpecialAttack"),
                                                                        BT.WaitForAnimatorState(anim, "Idle"),
                                                                        BT.Wait(1f),
                                                                        BT.Call(SetStateIdle)
                                                                    )
                                                                )
                                                            )
                                                        ),
                                                        BT.Root().OpenBranch(
                                                            BT.SetBool(anim, "Fly", true),
                                                            BT.SetBool(anim, "Fly_Stationary", true),
                                                            BT.Call(SetFlyState)
                                                        )
                                                    )
                                                )
                                            ),
                                            BT.Root().OpenBranch(
                                                BT.Call(SetStateIdle),
                                                BT.Call(SetRotatingTrue),
                                                BT.SetBool(anim, "Walk", false),
                                                BT.If(IsFlying).OpenBranch(
                                                    BT.SetBool(anim, "Fly_Stationary",true)
                                                )
                                            )
                                        )
                                    ),
                                    // 체이싱 구간
                                    BT.If(IsFlying).OpenBranch(
                                        BT.SetBool(anim, "Fly_Stationary", false),
                                        BT.SetBool(anim, "Fly", true),
                                        BT.Call(SetSpeedToChasing),
                                        BT.Call(SetStateChasing)
                                    ),
                                    // 나는 중이 아니라면
                                    BT.Root().OpenBranch(
                                        BT.SetBool(anim, "Walk", true),
                                        BT.Call(SetSpeedToChasing),
                                        BT.Call(SetStateChasing)
                                    )
                                )
                            ),
                            // 감지거리 밖인 경우
                            BT.Selector().OpenBranch(
                                // 날고 있는 경우 땅에 착륙하기
                                BT.Sequence().OpenBranch(
                                    BT.Condition(IsFlying),
                                    BT.SetBool(anim, "Fly", false),
                                    BT.SetBool(anim, "Fly_Stationary", false),
                                    BT.SetBool(anim, "Walk", false),
                                    BT.Call(() => { agent.SetDestination(transform.position); }),
                                    BT.WaitForAnimatorState(anim, "Idle"),
                                    BT.Call(SetFlyState),
                                    BT.Call(SetStateIdle)
                                ),
                                // 날고 있지 않은 경우

                                // 로밍중인가?
                                BT.If(IsRoaming).OpenBranch(
                                    // 로밍 목적지에 도착했나? 도착했다면 IDLE로 바꿔라
                                    BT.If(IsNearTargetPos).OpenBranch(
                                        BT.SetBool(anim, "Walk", false),
                                        BT.Call(SetStateIdle)
                                    )
                                ),
                                // 로밍 중이 아니라면
                                BT.RandomSequence(new int[] { 5, 11 }).OpenBranch(
                                    // 로밍 포인트 받고 로밍하기
                                    BT.Root().OpenBranch(
                                        BT.SetBool(anim, "Walk", true),
                                        BT.Call(SetSpeedToNormal),
                                        BT.Call(SetStateRoaming)
                                    ),
                                    // IDLE 유지
                                    BT.Root().OpenBranch(
                                        BT.SetBool(anim, "Walk", false),
                                        BT.Call(SetStateIdle)
                                    )
                                )
                            )
                        )
                    )
                ),
                // 죽었음
                BT.Call(Death),
                BT.Trigger(anim, "Death"),
                BT.Trigger(anim, "NormalAttack", false),
                BT.Trigger(anim, "SpecialAttack", false),
                BT.Wait(10),
                BT.RunCoroutine(DeathEffect),
                BT.Terminate()
            );

        }
        else
            isFirstTime = true;
    }


    #region StateFunc

    protected bool IsFlying()
    {
        return isFlying;
    }

    protected void SetFlyState()
    {
        if (isFlying)
            isFlying = false;
        else
        {
            isFlying = true;
            agent.SetDestination(transform.position);
        }
    }

    protected bool CheckEscape()
    {
        if (currentHP < MAX_HP * 0.5f && escapeCount > 0)
        {
            escapeCount--;
            return true;
        }
        return false;
    }

    protected bool IsEscape()
    {
        if (monsterState == eMonsterState.Escape) return true;
        else
            return false;
    }

    protected void SetStateEscape()
    {
        if (monsterState == eMonsterState.Escape) return;
        else
        {
            agent.SetDestination(roamingSpawner.transform.position);
            monsterState = eMonsterState.Escape;
        }
    }

    protected void SetStateContacting()
    {
        if (monsterState == eMonsterState.Contacting) return;
        else
        {
            agent.SetDestination(target.transform.position);
            monsterState = eMonsterState.Contacting;
        }
    }

    protected bool IsContacting()
    {
        if (monsterState == eMonsterState.Contacting)
            return true;
        else
            return false;
    }

    #endregion

    protected bool IsInBiteRange()
    {
        if (target == null) return false;

        if (((target.transform.position.x - transform.position.x) * (target.transform.position.x - transform.position.x))
            + ((target.transform.position.z - transform.position.z) * (target.transform.position.z - transform.position.z))
                < biteDistance * biteDistance)
            return true;
        else
            return false;
       
    }

    // 목적지 도착했는지?
    protected bool IsNearContactingPos()
    {
        if (((agent.destination.x - transform.position.x) * (agent.destination.x - transform.position.x))
                + ((agent.destination.z - transform.position.z) * (agent.destination.z - transform.position.z)) < biteDistance*biteDistance)
            return true;
        else
            return false;
    }

    private bool IsBGMChange()
    {
        return !SoundMng.Ins.isBossBGM;
    }

    private void BGMChange()
    {
        SoundMng.Ins.isBossBGM = true;
        SoundMng.Ins.ExternalSoundPlay(eSound.MeetBossBGM.ToDesc(), eChannel.BackGround);
    }

    // Animation Events
    #region Animation Events

    public void ShootProjectile()
    {
        SoundMng.Ins.InternalSoundPlay(eSound.FireBallSound.ToDesc(), monsterAuidoSource);
        ProjectileMng.Ins.GetProjectile(projectileID, projectilePos.position, target.transform.position);
    }
    #endregion
}
