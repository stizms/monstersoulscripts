﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageCollider : MonoBehaviour
{
    #region INSPECTOR

    public BaseMonster my;

    #endregion

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Character"))
        {
            other.GetComponent<CharacterMovement>().GetDamaged(my.damage);
        }
    }

}
