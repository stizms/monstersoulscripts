﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCollider : MonoBehaviour
{
    #region INSPECTOR

    public BaseMonster my;

    #endregion
    public bool isWeakness = false;

    protected void Awake()
    {
        SetWeakness();
    }

    public virtual void SetWeakness() { }
    public virtual bool Hit(Vector3 pos, CharacterMovement player,float damage) { return false; }
}
