﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadCollider : BaseCollider
{
    public override void SetWeakness()
    {
        if (my.isHeadWeakness)
            isWeakness = true;
    }

    public override bool Hit(Vector3 pos, CharacterMovement player,float damage)
    {
        if (my.isDamaged == true) return false;

        EffectMng.Ins.DisplayEffect(pos, eEffect.AttackEffect);
        my.isDamaged = true;
        player.HitTargetList.Add(this);
        float totalDamage = damage - my.headDef;
        if (totalDamage < 0f)
            totalDamage = 0f;
        if (my.currentHP > 0f)
            DamageTextMng.Ins.GetDamageText(pos, totalDamage, isWeakness);
        my.GetDamaged(totalDamage);
        return true;
    }
}
