﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BTAI;
using UnityEngine.AI;

public class BaseMonster : MonoBehaviour, ILooting
{
    #region INSPECTOR - Shader

    protected MaterialPropertyBlock materialProb;
    public List<Renderer>   renderers   = new List<Renderer>();
    public float            dissolve    = 0f;
    public float            speed       = 0.3f;

    #endregion

    #region INSPECTOR

    public List<Collider>   colliders       = new List<Collider>();
    public List<Looting>    lootingTrigger  = new List<Looting>();
    public Collider         attackCollider;
    public AudioSource      monsterAuidoSource;
    public Transform        timeLineFocus;
    public Animator         anim;

    #endregion


    // 몬스터 정보
    public eMonsterID    monsterID;
    public string        monsterName;
    public string        monsterKind;
    public float         MAX_HP;
    public float         currentHP;
    public float         damage;
    public float         headDef;
    public float         bodyDef;
    public float         tailDef;
    public float         detectionRange;
    public float         attackRange;
    public float         normalMoveSpeed;
    public float         chasingMoveSpeed;
    public int           lootingCount;
    public int           availableLootingCount;
    public eProjectile   projectileID;
    public eItemID       lootingItem { get; set; }
    public eItemID       rewardItem;
    public bool          isHeadWeakness;
    public bool          isBodyWeakness;
    public bool          isTailWeakness;
    public eMonsterState monsterState;
    

    public    BaseRoamer    roamingSpawner;
    public    Character     target;
    protected bool          isFirstTime = false;    // 초기 NavMesh 문제 생기는것 방지 용 ( 첫 OnEnable은 무시 )
    protected Root          ai;                     // Behaviour Tree 루트
    protected NavMeshAgent  agent;

    [HideInInspector]
    public bool isDamaged   = false;
    [HideInInspector]
    public bool isRotating  = false;

    // 파싱할 때 필요
    private string parsingMonsterName;

    private void Awake()
    {
        materialProb = new MaterialPropertyBlock();
        agent = GetComponent<NavMeshAgent>();

        parsingMonsterName = name.Substring(0, name.IndexOf("(Clone)"));

        MonsterData monsterData = TableMng.Ins.monsterTb[(eMonsterID)Enum.Parse(typeof(eMonsterID), parsingMonsterName)];
        monsterID             = monsterData.monsterID;
        monsterName           = monsterData.monsterName;
        monsterKind           = monsterData.monsterKind;
        MAX_HP                = monsterData.MAX_HP;
        currentHP             = MAX_HP;
        damage                = monsterData.damage;
        headDef               = monsterData.headDef;
        bodyDef               = monsterData.bodyDef;
        tailDef               = monsterData.tailDef;
        detectionRange        = monsterData.detectionRange;
        attackRange           = monsterData.attackRange;
        normalMoveSpeed       = monsterData.normalMoveSpeed;
        chasingMoveSpeed      = monsterData.chasingMoveSpeed;
        lootingCount          = monsterData.lootingCount;
        availableLootingCount = lootingCount;
        projectileID          = monsterData.projectileID;
        lootingItem           = monsterData.lootingItem;
        rewardItem            = monsterData.rewardItem;
        isHeadWeakness        = monsterData.isHeadWeakness;
        isBodyWeakness        = monsterData.isBodyWeakness;
        isTailWeakness        = monsterData.isTailWeakness;
        monsterState          = eMonsterState.Idle;
        
    }

    private void Start()
    {
        target = DungeonMng.Ins.dungeonScene.player;
        foreach (var node in lootingTrigger)
        {
            node.lootingInfo = this;
        }
    }

    private void OnDisable()
    {
        if(SoundMng.Ins.gameEffectChannel.Contains(monsterAuidoSource))
            SoundMng.Ins.gameEffectChannel.Remove(monsterAuidoSource);
    }

    // 몬스터 정보 초기화
    public void Init()
    {
        if (!isFirstTime)   return;
        
        for(int i = 0; i < renderers.Count; i++)
        {
            renderers[i].SetPropertyBlock(null);
        }

        for (int i = 0; i < colliders.Count; i++)
        {
            colliders[i].isTrigger = false;
        }

        currentHP             = MAX_HP;
        monsterState          = eMonsterState.Idle;
        isDamaged             = false;
        isRotating            = false;
        availableLootingCount = lootingCount;
        agent.enabled         = true;

        monsterAuidoSource.volume = SoundMng.Ins.soundEffectVolume;
        SoundMng.Ins.gameEffectChannel.Add(monsterAuidoSource);
    }

    private void Update()
    {
        if(!GameMng.Ins.isInteracting)
        {
            if (ai != null)
                ai.Tick();
            if (isRotating)
                LookTarget();
        }
    }
    
    public void ReturnToPool()
    {
        gameObject.SetActive(false);
        agent.enabled       = false;
        transform.position  = transform.parent.position;
    }

    // 플레이어와의 내적
    protected bool IsInSight()
    {
        if (target != null)
        {
            // 거리*방향벡터
            Vector3 targetDistance = target.transform.position - transform.position;
            // 거리
            float distance = Mathf.Sqrt((target.transform.position.x - transform.position.x) * (target.transform.position.x - transform.position.x) +
                (target.transform.position.z - transform.position.z) * (target.transform.position.z - transform.position.z));

            if (((transform.forward.x * targetDistance.x + transform.forward.z * targetDistance.z) / distance) >= 0.95f)
                return true;
            return false;
        }
        else
            return false;
    }

    // 플레이어 쳐다보는 함수
    protected void LookTarget()
    {
        if (target == null) return;
        Vector3 v3_Direction = (target.transform.position - transform.position).normalized;  //방향벡터 
        v3_Direction.y = 0;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(v3_Direction), 3f * Time.deltaTime);
    }

    // 체력이 있는지 체크
    protected bool IsAlive()
    {
        if (currentHP > 0) return true;
        else
            return false;
    }

    // 감지 범위 내에 위치한지 체크
    protected bool IsInDetectionRange()
    {
        if (target != null)
        {
            if (((target.transform.position.x - transform.position.x) * (target.transform.position.x - transform.position.x))
                + ((target.transform.position.z - transform.position.z) * (target.transform.position.z - transform.position.z))
                 < detectionRange * detectionRange)
                return true;
            else
                return false;
        }
        else
            return false;
    }

    // 공격 가능 범위 내 위치한지 체크
    protected bool IsInAttackRange()
    {
        if (target != null)
        {
            if (((target.transform.position.x - transform.position.x) * (target.transform.position.x - transform.position.x))
                + ((target.transform.position.z - transform.position.z) * (target.transform.position.z - transform.position.z))
                 < attackRange * attackRange)
                return true;
            else
                return false;
        }
        else
            return false;
    }

    // 목적지 도착했는지?
    protected bool IsNearTargetPos()
    {
        if ((agent.destination.x - transform.position.x) < 3f && (agent.destination.z - transform.position.z) < 3f)
            return true;
        else
            return false;
    }


    // 데미지 받는 함수
    public void GetDamaged(float damage)
    {
        currentHP -= damage;
    }
    
    protected void Death()
    {
        isRotating      = false;
        monsterState    = eMonsterState.Death;
        agent.enabled   = false;

        AttackColliderOff();
        MonsterMng.Ins.activeMonsterPool.Remove(this);
        for(int i = 0; i < colliders.Count; i++)
        {
            colliders[i].isTrigger = true;
        }
        
        DungeonMng.Ins.dungeonItemList.GetItem(rewardItem, 1);  // 던전 보상은 무조건 1개씩 들어감
        QuestMng.Ins.MonsterDied(monsterID);
    }

    protected IEnumerator<BTState> DeathEffect()
    {
        while(true)
        {
            bool isFlag = true;
            for (int i = 0; i < renderers.Count; i++)
            {
                renderers[i].GetPropertyBlock(materialProb);
                dissolve = materialProb.GetFloat("_DissolveCutoff");
                dissolve += speed * Time.deltaTime;
                if (dissolve > 1f)
                    dissolve = 1f;
                materialProb.SetFloat("_DissolveCutoff", dissolve);
                renderers[i].SetPropertyBlock(materialProb);
                if (dissolve < 1f)
                    isFlag = false;
            }
            if (isFlag == true)
                break;
            yield return BTState.Continue;
        }
            ReturnToPool();
    }

    // 룩앳 관리용 불변수
    protected void SetRotatingFalse()
    {
        isRotating = false;
    }
    protected void SetRotatingTrue()
    {
        isRotating = true;
    }

    protected bool IsRotating()
    {
        return isRotating;
    }

    #region ChangeSpeed

    protected void SetSpeedToNormal()
    {
        agent.speed = normalMoveSpeed;
    }

    protected void SetSpeedToChasing()
    {
        agent.speed = chasingMoveSpeed;
    }

    #endregion

    #region StateFunc

    protected bool IsIdle()
    {
        if (monsterState == eMonsterState.Idle)
            return true;
        return false;
    }

    protected bool IsRoaming()
    {
        if (monsterState == eMonsterState.Roaming)
            return true;
        return false;
    }

    protected bool IsAttack()
    {
        if (monsterState == eMonsterState.Attack)
            return true;
        return false;
    }

    public void SetStateIdle()
    {
        if (monsterState == eMonsterState.Idle) return;

        agent.SetDestination(transform.position);
        monsterState = eMonsterState.Idle;
    }

    protected void SetStateRoaming()
    {
        if (monsterState == eMonsterState.Roaming) return;

        agent.SetDestination(roamingSpawner.GetRoamingPoint());
        monsterState = eMonsterState.Roaming;
    }

    protected void SetStateAttack()
    {
        if (target == null) return;
        agent.SetDestination(transform.position);
        monsterState = eMonsterState.Attack;
    }

    protected void SetStateChasing()
    {
        if (target == null) return;
        agent.SetDestination(target.transform.position);
        monsterState = eMonsterState.Chasing;
    }

    #endregion

    #region ILooting

    public eLootingCategory lootingCategory { get {return eLootingCategory.Monster; } }

    public bool CheckCanLooting()
    {
        if (availableLootingCount > 0) return true;
        else
            return false;
    }


    public string GetSpriteName()
    {
        return monsterID.ToString();
    }

    public string GetObjectName()
    {
        return monsterName;
    }

    public eItemID LootItem(out int amount)
    {
        amount = 0;
        // 여기서 랜덤으로 뿌려줌
        if (availableLootingCount <= 0) return eItemID.None;

        amount = UnityEngine.Random.Range(1, 4);
        int difference = TableMng.Ins.itemTb[lootingItem].characterLimit - UserDataMng.Ins.nowCharacter.characterInventory.GetItemCount(lootingItem);
        if (difference < amount)
            amount = difference;
        availableLootingCount--;
        if (availableLootingCount == 0)
            UIMng.Ins.lootingHud.NonVisualizeLootingHud();
        return lootingItem;
    }

    #endregion

    #region AnimationEvents

    public void AttackColliderOn()
    {
        if(monsterState!=eMonsterState.Death)
            attackCollider.enabled = true;
    }

    public void AttackColliderOff()
    {
        attackCollider.enabled = false;
    }

    #endregion

}