﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BTAI;

public class LizardKnight : BaseMonster
{
    private void OnEnable()
    {
        if (isFirstTime)
        {
            // 초기 세팅
            Init();

            ai = BT.Root();
            ai.OpenBranch(
                // 살아있는 경우 실행
                BT.While(IsAlive).OpenBranch(
                    BT.If(IsRotating).OpenBranch(
                        BT.If(IsInSight).OpenBranch(
                            BT.Call(SetRotatingFalse)
                        )
                    ),
                    // 감지 거리 안에 적이 있는 경우
                    BT.Selector().OpenBranch(
                        BT.If(IsInDetectionRange).OpenBranch(
                            // 공격 범위 안에 있으면 공격, 아니면 추격
                            BT.Selector().OpenBranch(
                                BT.If(IsInAttackRange).OpenBranch(
                                    BT.Selector().OpenBranch(
                                        BT.Sequence().OpenBranch(
                                            BT.Condition(IsInSight),
                                            BT.Root().OpenBranch(
                                                BT.SetBool(anim, "Run", false),
                                                BT.Call(SetStateAttack),
                                                BT.RandomSequence(new int[] { 1, 2 }).OpenBranch(
                                                    BT.Wait(0f),
                                                    BT.Wait(1f)
                                                ),
                                                BT.RandomSequence(new int[] { 6, 5 }).OpenBranch(
                                                    BT.SetInt(anim, "AttackCategory", (int)eMonsterAttack.NormalAttack1),
                                                    BT.SetInt(anim, "AttackCategory", (int)eMonsterAttack.NormalAttack2)
                                                ),
                                                BT.Trigger(anim, "IsAttack"),
                                                BT.WaitForAnimatorState(anim, "IDLE"),
                                                BT.Wait(2f),
                                                BT.SetInt(anim, "AttackCategory", (int)eMonsterAttack.None),
                                                BT.Call(SetStateIdle)
                                            )
                                        ),
                                        BT.Root().OpenBranch(
                                            BT.Call(SetStateIdle),
                                            BT.Call(SetRotatingTrue),
                                            BT.SetBool(anim,"Walk",false),
                                            BT.SetBool(anim, "Run", false),
                                            BT.SetInt(anim, "AttackCategory", (int)eMonsterAttack.None)
                                        )
                                    )
                                ),
                                BT.Selector().OpenBranch(
                                    BT.Root().OpenBranch(
                                        BT.SetBool(anim, "Walk", false),
                                        BT.SetBool(anim, "Run", true),
                                        BT.Call(SetSpeedToChasing),
                                        BT.Call(SetStateChasing)
                                    )
                                )
                            )
                        ),
                        // 감지거리 밖인 경우
                        BT.Selector().OpenBranch(
                            // 로밍 중인가?
                            BT.If(IsRoaming).OpenBranch(
                                // 로밍 목적지에 도착했나? 도착했다면 IDLE로 바꿔라
                                BT.If(IsNearTargetPos).OpenBranch(
                                    BT.SetBool(anim, "Walk", false),
                                    BT.Call(SetStateIdle)
                                )
                            ),
                            // 로밍중이 아니라면 아래의 행동 중 하나를 하여라
                            BT.RandomSequence(new int[] { 5, 11 }).OpenBranch(
                                // 로밍 포인트 받고 로밍하기
                                BT.Root().OpenBranch(
                                    BT.SetBool(anim, "Run", false),
                                    BT.SetBool(anim, "Walk", true),
                                    BT.Call(SetSpeedToNormal),
                                    BT.Call(SetStateRoaming)
                                ),
                                // IDLE 유지
                                BT.Root().OpenBranch(
                                    BT.SetBool(anim, "Walk", false),
                                    BT.SetBool(anim, "Run", false),
                                    BT.Call(SetStateIdle)
                                )
                            )
                        )
                    )
                ),
                BT.Call(Death),
                BT.Trigger(anim,"IsDeath"),
                BT.Trigger(anim,"IsAttack",false),
                BT.SetInt(anim, "AttackCategory", (int)eMonsterAttack.None),
                BT.Wait(10),
                BT.RunCoroutine(DeathEffect),
                BT.Terminate()
            );
        }
        else
            isFirstTime = true;
    }
}