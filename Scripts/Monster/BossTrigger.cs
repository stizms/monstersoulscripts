﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Playables;
using System.Linq;

public class BossTrigger : MonoBehaviour
{
    #region INSPECTOR

    public MonsterSpawner           bossSpawner;
    public CinemachineVirtualCamera CM_Camera1;
    public CinemachineVirtualCamera CM_Camera2; // ( 2는 줌 카메라 )
    public PlayableDirector         bossTimeLine;

    // 타임라인이 시작 된 뒤 몇 초 뒤에 함수 표효를 시작할 것인지?  ( 음수일 경우 표효 하지 않음 )
    public float roarDelay      = 0f;
    public float shakeDuration  = 0f;
    public float shakeAmplitude = 0f;   // 진폭
    public float shakeFrequency = 0f;   // 빈도

    #endregion

    private bool    isCreated        = false;
    private float   shakeElapsedTime = 0f;
    private CinemachineBasicMultiChannelPerlin roarNoiseCamera;
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Character") && !isCreated)
        {
            StartBossCutScene();
        }
    }

    public void StartBossCutScene()
    {
        isCreated = true;
        GameMng.Ins.isInteracting = true;

        BaseMonster monster = bossSpawner.SetMonsterPos();
        UIMng.Ins.dungeon.bossName.SetData(string.Format("{0}{1}", monster.monsterID, "_Icon"), monster.monsterKind, monster.monsterName);
        CM_Camera1.LookAt = monster.timeLineFocus;
        CM_Camera2.LookAt = monster.timeLineFocus;

        var binding = bossTimeLine.playableAsset.outputs;
        List<PlayableBinding> bindingList = binding.ToList();
        bossTimeLine.SetGenericBinding(bindingList[0].sourceObject, Camera.main.GetComponent<CinemachineBrain>());
        bossTimeLine.SetGenericBinding(bindingList[1].sourceObject, monster.anim);
        bossTimeLine.SetGenericBinding(bindingList[2].sourceObject, UIMng.Ins.dungeon.bossName.gameObject);
        bossTimeLine.SetGenericBinding(bindingList[3].sourceObject, monster.anim);
        bossTimeLine.SetGenericBinding(bindingList[4].sourceObject, monster.monsterAuidoSource);

        UIMng.Ins.dungeon.TurnOffAllUI();
        bossTimeLine.Play();
        StartCoroutine(RoarFunc(roarDelay));
        StartCoroutine(CheckTimelineEnd(monster));
    }

    IEnumerator RoarFunc(float delayTime)
    {
        if(delayTime < 0f)  yield break;
        else
        {
            roarNoiseCamera = CM_Camera2.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            yield return new WaitForSeconds(delayTime);
            shakeElapsedTime = shakeDuration;
            while(true)
            {
                yield return null;
                if(shakeElapsedTime > 0f)
                {
                    roarNoiseCamera.m_AmplitudeGain = shakeAmplitude;
                    roarNoiseCamera.m_FrequencyGain = shakeFrequency;
                    shakeElapsedTime -= Time.deltaTime;
                }
                else
                {
                    roarNoiseCamera.m_AmplitudeGain = 0f;
                    shakeElapsedTime = 0f;
                    break;
                }
            }
        }
    }

    IEnumerator CheckTimelineEnd(BaseMonster monster)
    {
        while(true)
        {
            yield return null;
            if (bossTimeLine.state != PlayState.Playing)
            {
                monster.ReturnToPool();
                bossSpawner.SetBossPos(-140f);
                UIMng.Ins.dungeon.TurnOnAllUI();
                bossTimeLine.gameObject.SetActive(false);
                GameMng.Ins.isInteracting = false;
                gameObject.SetActive(false);
                break;
            }
        }
    }
}
