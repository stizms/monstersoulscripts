﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseProjectile : MonoBehaviour
{
    private Vector3 destinationPos;
    private Vector3 direction = Vector3.zero;
    public  float   damage;
    public  float   speed;

    public void SetData(eProjectile projectileID, Vector3 pos)
    {
        ProjectileData tempProjectile = TableMng.Ins.projectileTb[projectileID];
        damage           = tempProjectile.damage;
        speed            = tempProjectile.speed;
        destinationPos   = pos;
        destinationPos.y += 0.5f;
        direction        = (destinationPos - transform.position).normalized;
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Character"))
        {
            other.GetComponent<CharacterMovement>().GetDamaged(damage);
            ProjectileMng.Ins.ReturnToPool(this);
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
            ProjectileMng.Ins.ReturnToPool(this);
    }

    protected void Update()
    { 
        transform.position += direction * speed * Time.deltaTime;
    }
}
