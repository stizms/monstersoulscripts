﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseRoamer : MonoBehaviour
{
    public abstract Vector3 GetRoamingPoint();
}
