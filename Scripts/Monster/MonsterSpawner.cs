﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
    #region INSPECTOR

    // 해당 위치에 생성할 몬스터 타입
    public eMonsterID createMonsterType;
    public BaseRoamer roamingSpawner;

    #endregion

    public BaseMonster SetMonsterPos()
    {
        BaseMonster monster         = MonsterMng.Ins.GetMonster(createMonsterType);
        monster.transform.position  = transform.position;
        monster.transform.rotation  = Quaternion.Euler(new Vector3(0, Random.Range(0,360), 0));
        monster.roamingSpawner      = roamingSpawner;
        monster.gameObject.SetActive(true);
        return monster;
    }

    // 로테이션을 고정 위치로 두기 위함 ( 타임라인과의 연계를 위해 )
    public BaseMonster SetBossPos(float yAxis)
    {
        BaseMonster monster         = MonsterMng.Ins.GetMonster(createMonsterType);
        monster.transform.position  = transform.position;
        monster.transform.rotation  = Quaternion.Euler(new Vector3(0, yAxis, 0));
        monster.roamingSpawner      = roamingSpawner;
        monster.gameObject.SetActive(true);
        return monster;
    }
}
