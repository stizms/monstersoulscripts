﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvents : MonoBehaviour
{
    #region INSPECTOR

    public Character            character;
    public CharacterMovement    movement;
    public AudioSource          audioSource;

    #endregion

    // 아이템 먹기
    public void ConsumeItem()
    {
        // 현재 선택되어 있는 아이템 먹기
        ConsumableItemInfoData consumeItem = TableMng.Ins.consumeItemInfoTb[character.selectedItem];
        switch (consumeItem.effectType)
        {
            case eEffectType.Recovery:
                SoundMng.Ins.InternalSoundPlay(eSound.PotionSound.ToDesc(), audioSource);
                EffectMng.Ins.DisplayEffect(character.consumeEffectPos.position, eEffect.GreenConsumeEffect);
                if (character.currentHp + consumeItem.effectAmount > character.currentMaxHp)
                    character.recoveryHp = character.currentMaxHp;
                else
                    character.recoveryHp = character.currentHp + consumeItem.effectAmount;
                character.hpState = eHPState.Recovery;
                UIMng.Ins.dungeon.characterInfoUI.characterStateUI.GetRecovery(consumeItem.effectAmount);
                break;
            case eEffectType.HP_Grow:
                SoundMng.Ins.InternalSoundPlay(eSound.PotionSound.ToDesc(), audioSource);
                EffectMng.Ins.DisplayEffect(character.consumeEffectPos.position, eEffect.BlueConsumeEffect);
                if (character.currentMaxHp + consumeItem.effectAmount > Define.MAX_HP)
                    character.currentMaxHp = Define.MAX_HP;
                else
                    character.currentMaxHp += consumeItem.effectAmount;
                UIMng.Ins.dungeon.characterInfoUI.characterStateUI.GetGrowMaxHp();
                character.usingItem = eItemID.None;
                break;
            case eEffectType.Stamina_Grow:
                SoundMng.Ins.InternalSoundPlay(eSound.RationSound.ToDesc(), audioSource);
                EffectMng.Ins.DisplayEffect(character.consumeEffectPos.position, eEffect.YellowConsumeEffect);
                if (character.currentMaxStamina + consumeItem.effectAmount > Define.MAX_STAMINA)
                    character.currentMaxStamina = Define.MAX_STAMINA;
                else
                    character.currentMaxStamina += consumeItem.effectAmount;
                UIMng.Ins.dungeon.characterInfoUI.characterStateUI.GetGrowMaxStamina();
                character.usingItem = eItemID.None;
                break;
            case eEffectType.Sharping:
                SoundMng.Ins.InternalSoundPlay(eSound.WhetStoneSound.ToDesc(), audioSource);
                EffectMng.Ins.DisplayEffect(character.consumeEffectPos.position, eEffect.YellowConsumeEffect);
                character.equipWeapon.SharpnessFullRecovery();
                character.usingItem = eItemID.None;
                break;
        }
    }

    // 무기의 부모를 바꾸는 과정 ( 애니메이션 이벤트로 호출 )
    public void SwitchWeaponState()
    {
        switch (character.weaponState)
        {
            case eUsingWeaponState.DRAW:
                character.weaponState = eUsingWeaponState.SHEATING;
                character.equipWeapon.SetSheatingPos(character);
                break;
            case eUsingWeaponState.SHEATING:
                character.weaponState = eUsingWeaponState.DRAW;
                character.equipWeapon.SetDrawPos(character);
                break;
        }
    }

    public void CheckComboAttack()
    {
        if (movement.isAttack == true)
        {
            switch ((eAttackCategory)character.playerAnimator.GetInteger("AttackCategory"))
            {
                case eAttackCategory.Attack1:
                    character.playerAnimator.SetInteger("AttackCategory",(int)eAttackCategory.Attack2);
                    break;
                case eAttackCategory.Attack2:
                    character.playerAnimator.SetInteger("AttackCategory", (int)eAttackCategory.Attack3);
                    break;
                case eAttackCategory.Attack3:
                    character.playerAnimator.SetInteger("AttackCategory", (int)eAttackCategory.Attack4);
                    break;
                case eAttackCategory.Attack4:
                    character.playerAnimator.SetInteger("AttackCategory", (int)eAttackCategory.None);
                    character.playerState = ePlayerState.IDLE;
                    character.playerAnimator.SetBool("IsAttack", false);
                    break;
            }
            movement.isAttack = false;
        }
        else
        {
            character.playerAnimator.SetInteger("AttackCategory", (int)eAttackCategory.None);
            character.playerState = ePlayerState.IDLE;
            character.playerAnimator.SetBool("IsAttack", false);
        }
    }

    public void WeaponColliderOff()
    {
        movement.WeaponColliderOff();
    }

    public void WeaponColliderOn()
    {
        movement.WeaponColliderOn();
    }

    // 데미지 주기
    public void ClearHitTargetList()
    {
        movement.ClearHitTargetList();
    }

    public void SetWeaponStateSheating()
    {
        character.weaponState = eUsingWeaponState.SHEATING;
    }

    public void SetWeaponStateDraw()
    {
        character.weaponState = eUsingWeaponState.DRAW;
    }

    // 살아나기
    public void Revive()
    {
        movement.Revive();
    }

    // 루팅
    public void LootItem()
    {
        if (character.lootingTrigger == null) return;

        int amount = 0;
        eItemID lootingItem = character.lootingTrigger.LootItem(out amount);
        amount = UserDataMng.Ins.nowCharacter.characterInventory.GetItem(lootingItem, amount);
        if (amount <= 0) return;
        UIMng.Ins.alarmHud.AddItemAlarm(lootingItem,amount);
        QuestMng.Ins.PickedUpItem(lootingItem,amount);
    }

    public void UnLock()
    {
        movement.dontMoveFlag = false;
        Camera.main.GetComponent<CinemachineBrain>().enabled = true;
        // 몬스터 타겟 재 설정
        MonsterMng.Ins.SetTarget();
    }
}