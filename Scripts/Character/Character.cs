﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    #region INSPECTOR

    //왼손 오른손
    public Transform    leftWeaponPos;
    public Transform    rightWeaponPos;
    // 등
    public Transform    backWeaponPos;
    // 머리 위 ( hud 뜨는 곳 )
    public Transform    hudPos;

    // 소모 아이템 사용시 Effect 뜨는 곳
    public Transform    consumeEffectPos;
    #endregion

    // 캐릭터 정보
    public UserData             characterInfo = null;

    // 캐릭터 상태
    public ePlayerState         playerState = ePlayerState.IDLE;
    public eUsingWeaponState    weaponState = eUsingWeaponState.SHEATING;
    public eHPState             hpState     = eHPState.IDLE;

    // HP & 스테미나

    public float    currentMaxHp        = 100f;
    public float    currentMaxStamina   = 100f;
    public float    currentHp           = 100f;
    public float    currentStamina      = 100f;
    
    // 치료되어야하는 최종 Hp목적지
    public float    recoveryHp = 0f;

    // 착용 장비
    public BaseWeapon           equipWeapon = null;
    public BaseEquipment        equipHelmet = null;
    public BaseEquipment        equipVest   = null;
    public BaseEquipment        equipGlove  = null;
    public BaseEquipment        equipBelt   = null;
    public BaseEquipment        equipPants  = null;

    public Animator             playerAnimator;

    // 현재 사용할 아이템
    public eItemID  selectedItem        = eItemID.None;
    public eItemID  usingItem           = eItemID.None;

    // 방어력
    public float    defense             = 0f;
    // 내성
    public float    fireTolerance       = 0f;
    public float    waterTolerance      = 0f;
    public float    thunderTolerance    = 0f;
    public float    iceTolerance        = 0f;
    
    public BaseNPC  nearNPC         = null; // 근처에 있는 NPC ( TriggerStay를 안쓰고 여기서 인풋을 받기 위함 )
    public Looting  lootingTrigger  = null; // 루팅 체크하는 트리거

    private void Awake()
    {
        characterInfo = UserDataMng.Ins.characterSlot[UserDataMng.Ins.selectedIndex];
        if(characterInfo != null)
        {
            EquipAllEquipment();
        }
    }
    
    // 플레이어 정보 초기화 ( 다시 태어날 때 )
    public void ReviveInit()
    {
        playerState                     = ePlayerState.IDLE;
        weaponState                     = eUsingWeaponState.SHEATING;
        hpState                         = eHPState.IDLE;
        currentMaxHp                    = 100f;
        currentHp                       = currentMaxHp;
        currentMaxStamina               = 100f;
        currentStamina                  = currentMaxStamina;
        equipWeapon.currentSharpness    = equipWeapon.MAX_Sharpness;
    }

    #region EquipEquipment

    // 초기 모든 장비 장착하는 함수
    public void EquipAllEquipment()
    {
        EquipWeapon(characterInfo.equipWeapon);
        EquipHelmet(characterInfo.equipHelmet);
        EquipVest(characterInfo.equipVest);
        EquipGlove(characterInfo.equipGlove);
        EquipBelt(characterInfo.equipBelt);
        EquipPants(characterInfo.equipPants);
    }

    // 무기 바꿔줘! -> userData 무기 갱신 -> 무기 착용
    // 무기 장착 ( 마이 하우스에서만 변경 가능 )
    public void EquipWeapon(eEquipment weaponID)
    {
        UnEquipWeapon();
        characterInfo.equipWeapon = weaponID;
        equipWeapon = EquipmentMng.Ins.GetWeapon(weaponID);
        // 애니메이션 초기화 ( 캐릭터 선택창에서 버그 방지 )
        if(playerAnimator != null)
            SetWeaponAnimParam();

        // 장비 위치 설정
        equipWeapon.SetSheatingPos(this);
    }
    
    // 무기 해제 ( 리소스 매니저로 보내버림 )
    private void UnEquipWeapon()
    {
        if (equipWeapon != null)
        {
            equipWeapon.transform.parent = EquipmentMng.Ins.poolingPos;
            equipWeapon.transform.localPosition = Vector3.zero;
            characterInfo.equipWeapon = eEquipment.None;
            equipWeapon = null;
        }
    }

    public void EquipHelmet(eEquipment helmetID)
    {
        UnEquipHelmet();
        if (helmetID == eEquipment.None) return;
        characterInfo.equipHelmet = helmetID;
        equipHelmet = EquipmentMng.Ins.GetEquipment(helmetID);

        EquipmentData helmetEquipment = TableMng.Ins.equipmentTb[characterInfo.equipHelmet];
        defense             += helmetEquipment.def;
        fireTolerance       += helmetEquipment.fireTolerance;
        waterTolerance      += helmetEquipment.waterTolerance;
        thunderTolerance    += helmetEquipment.thunderTolerance;
        iceTolerance        += helmetEquipment.iceTolerance;
    }

    public void UnEquipHelmet()
    {
        if (characterInfo.equipHelmet == eEquipment.None) return;
        EquipmentData helmetEquipment = TableMng.Ins.equipmentTb[characterInfo.equipHelmet];
        if (equipHelmet != null)
        {
            defense             -= helmetEquipment.def;
            fireTolerance       -= helmetEquipment.fireTolerance;
            waterTolerance      -= helmetEquipment.waterTolerance;
            thunderTolerance    -= helmetEquipment.thunderTolerance;
            iceTolerance        -= helmetEquipment.iceTolerance;
        }
        characterInfo.equipHelmet = eEquipment.None;
        equipHelmet = null;
    }

    public void EquipVest(eEquipment vestID)
    {
        UnEquipVest();
        if (vestID == eEquipment.None) return;
        characterInfo.equipVest = vestID;
        equipVest = EquipmentMng.Ins.GetEquipment(vestID);

        EquipmentData vesrEquipment = TableMng.Ins.equipmentTb[characterInfo.equipVest];
        defense             += vesrEquipment.def;
        fireTolerance       += vesrEquipment.fireTolerance;
        waterTolerance      += vesrEquipment.waterTolerance;
        thunderTolerance    += vesrEquipment.thunderTolerance;
        iceTolerance        += vesrEquipment.iceTolerance;
    }

    public void UnEquipVest()
    {
        if (characterInfo.equipVest == eEquipment.None) return;
        EquipmentData vesrEquipment = TableMng.Ins.equipmentTb[characterInfo.equipVest];
        if (equipVest != null)
        {
            defense             -= vesrEquipment.def;
            fireTolerance       -= vesrEquipment.fireTolerance;
            waterTolerance      -= vesrEquipment.waterTolerance;
            thunderTolerance    -= vesrEquipment.thunderTolerance;
            iceTolerance        -= vesrEquipment.iceTolerance;
        }
        characterInfo.equipVest = eEquipment.None;
        equipVest = null;
    }

    public void EquipGlove(eEquipment gloveID)
    {
        UnEquipGlove();
        if (gloveID == eEquipment.None) return;
        characterInfo.equipGlove = gloveID;
        equipGlove = EquipmentMng.Ins.GetEquipment(gloveID);

        EquipmentData gloveEquipment = TableMng.Ins.equipmentTb[characterInfo.equipGlove];
        defense             += gloveEquipment.def;
        fireTolerance       += gloveEquipment.fireTolerance;
        waterTolerance      += gloveEquipment.waterTolerance;
        thunderTolerance    += gloveEquipment.thunderTolerance;
        iceTolerance        += gloveEquipment.iceTolerance;
    }

    public void UnEquipGlove()
    {
        if (characterInfo.equipGlove == eEquipment.None) return;
        EquipmentData gloveEquipment = TableMng.Ins.equipmentTb[characterInfo.equipGlove];
        if (equipGlove != null)
        {
            defense             -= gloveEquipment.def;
            fireTolerance       -= gloveEquipment.fireTolerance;
            waterTolerance      -= gloveEquipment.waterTolerance;
            thunderTolerance    -= gloveEquipment.thunderTolerance;
            iceTolerance        -= gloveEquipment.iceTolerance;
        }
        characterInfo.equipGlove = eEquipment.None;
        equipGlove = null;
    }

    public void EquipBelt(eEquipment beltID)
    {
        UnEquipBelt();
        if (beltID == eEquipment.None) return;
        characterInfo.equipBelt = beltID;
        equipBelt = EquipmentMng.Ins.GetEquipment(beltID);

        EquipmentData beltEquipment = TableMng.Ins.equipmentTb[characterInfo.equipBelt];
        defense             += beltEquipment.def;
        fireTolerance       += beltEquipment.fireTolerance;
        waterTolerance      += beltEquipment.waterTolerance;
        thunderTolerance    += beltEquipment.thunderTolerance;
        iceTolerance        += beltEquipment.iceTolerance;
    }

    public void UnEquipBelt()
    {
        if (characterInfo.equipBelt == eEquipment.None) return;
        EquipmentData beltEquipment = TableMng.Ins.equipmentTb[characterInfo.equipBelt];
        if (equipBelt != null)
        {
            defense             -= beltEquipment.def;
            fireTolerance       -= beltEquipment.fireTolerance;
            waterTolerance      -= beltEquipment.waterTolerance;
            thunderTolerance    -= beltEquipment.thunderTolerance;
            iceTolerance        -= beltEquipment.iceTolerance;
        }
        characterInfo.equipBelt = eEquipment.None;
        equipBelt = null;
    }

    public void EquipPants(eEquipment pantsID)
    {
        UnEquipPants();
        if (pantsID == eEquipment.None) return;
        characterInfo.equipPants = pantsID;
        equipPants = EquipmentMng.Ins.GetEquipment(pantsID);

        EquipmentData pantsEquipment = TableMng.Ins.equipmentTb[characterInfo.equipPants];
        defense             += pantsEquipment.def;
        fireTolerance       += pantsEquipment.fireTolerance;
        waterTolerance      += pantsEquipment.waterTolerance;
        thunderTolerance    += pantsEquipment.thunderTolerance;
        iceTolerance        += pantsEquipment.iceTolerance;
    }

    public void UnEquipPants()
    {
        if (characterInfo.equipPants == eEquipment.None) return;
        EquipmentData pantsEquipment = TableMng.Ins.equipmentTb[characterInfo.equipPants];
        if (equipPants != null)
        {
            defense             -= pantsEquipment.def;
            fireTolerance       -= pantsEquipment.fireTolerance;
            waterTolerance      -= pantsEquipment.waterTolerance;
            thunderTolerance    -= pantsEquipment.thunderTolerance;
            iceTolerance        -= pantsEquipment.iceTolerance;
        }
        characterInfo.equipPants = eEquipment.None;
        equipPants = null;
    }

    #endregion

    // 애니메이션 파라미터 초기화
    private void SetWeaponAnimParam()
    {
        playerAnimator.SetBool("GreatSword", false);
        playerAnimator.SetBool("ChargeAxe", false);
        playerAnimator.SetBool("Lance", false);

        switch (equipWeapon.weaponCategory)
        {
            case eWeaponCategory.GreatSword:
                playerAnimator.SetBool("GreatSword", true);
                break;
            case eWeaponCategory.ChargeAxe:
                playerAnimator.SetBool("ChargeAxe", true);
                break;
            case eWeaponCategory.Lance:
                playerAnimator.SetBool("Lance", true);
                break;
        }
    }

}
