﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{

    #region INSPECTOR

    public  float moveSpeed             = 10f;
    public  float rotateSpeed           = 15f;
    public  float rollingSpeed          = 1.5f;
    public  float rollDuration          = 0.6f;
    public  float drawDuration          = 1f;
    public  float sheathingDuration     = 0.3f;
    public  float lootingDuration       = 1.1f;
    public  float damagedDuration       = 0.6f;
    public  float consumeDuration       = 1f;
    public  float gravity               = 20.0f;
    public  float damagedRecoverySpeed  = 2f;
    public  float staminaRecoverySpeed  = 15f;
    public  PlayerCamera playerCam;

    #endregion

    public  AudioSource playerAudioSource;

    private float   rollingDistance   = 30f;
    private float   h;
    private float   v;
    private Vector3 moveDir;
    private bool    isRecoveryStamina = true;
    private CharacterController controller;

    // 캐릭터 정보
    private Character characterInfo;
    public List<BaseCollider> HitTargetList = new List<BaseCollider>(); // 공격 받을 몬스터 모음
    
    public  bool isAttack       = false;    // 공격중인지?
    public  bool dontMoveFlag   = false;   // 움직임 제어

    private void Awake()
    {
        characterInfo       = GetComponent<Character>();
        controller          = GetComponent<CharacterController>();
        playerAudioSource   = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        playerAudioSource.volume = SoundMng.Ins.soundEffectVolume;
        SoundMng.Ins.gameEffectChannel.Add(playerAudioSource);
    }

    private void OnDisable()
    {
        SoundMng.Ins.gameEffectChannel.Remove(playerAudioSource);
    }

    private void Start()
    {
        characterInfo.equipWeapon.movement = this;
    }

    public void Update()
    {
        if (!GameMng.Ins.isInteracting)
        {
            //NPC근처에 있을 때 오른쪽 버튼을 누르면 해당 NPC와 인터랙팅 함.
            if (characterInfo.nearNPC != null && Input.GetMouseButtonDown(1))
                characterInfo.nearNPC.Interacting();

            // 옵션 호출
            if ((SceneMng.Ins.nowScene > eScene.Dungeon || SceneMng.Ins.nowScene == eScene.Lobby) && Input.GetKeyDown(KeyCode.Escape))
            {
                UIMng.Ins.optionUI.VisualizeMainOption();
                GameMng.Ins.CursorVisualize();
            }

            if (characterInfo.playerState != ePlayerState.Death)
            {
                Action();
                HPAction();
                StaminaAction();
            }
        }
    }

    private void HPAction()
    {
        if(characterInfo.hpState == eHPState.Recovery || characterInfo.hpState == eHPState.Damaged)
        {
            float recoverySpeed = 0f;
            
            if (characterInfo.hpState == eHPState.Damaged) recoverySpeed = damagedRecoverySpeed;
            else if (characterInfo.hpState == eHPState.Recovery)
                recoverySpeed = TableMng.Ins.consumeItemInfoTb[characterInfo.usingItem].effectAmount / TableMng.Ins.consumeItemInfoTb[characterInfo.usingItem].effectTime;
            
            if (characterInfo.currentHp < characterInfo.recoveryHp)
            {
                characterInfo.currentHp += recoverySpeed * Time.deltaTime;
                UIMng.Ins.dungeon.characterInfoUI.characterStateUI.UpdateGreenHp();
            }
            else
            {
                characterInfo.usingItem = eItemID.None;
                characterInfo.currentHp = characterInfo.recoveryHp;
                characterInfo.recoveryHp = 0f;
                UIMng.Ins.dungeon.characterInfoUI.characterStateUI.TurnOffAnotherGaze();
                characterInfo.hpState = eHPState.IDLE;
            }
        }
    }

    private void StaminaAction()
    {
        if(isRecoveryStamina)
        {
            if (characterInfo.currentStamina < characterInfo.currentMaxStamina)
            {
                characterInfo.currentStamina += staminaRecoverySpeed * Time.deltaTime;
                if (characterInfo.currentStamina >= characterInfo.currentMaxStamina)
                    characterInfo.currentStamina = characterInfo.currentMaxStamina;

                UIMng.Ins.dungeon.characterInfoUI.characterStateUI.UpdateStaminaGaze();
            }
        }
    }

    // 동작 함수
    private void Action()
    {
        if (controller.isGrounded)
        {
            if (characterInfo.playerState != ePlayerState.ROLLING && characterInfo.playerState != ePlayerState.Damaged && characterInfo.playerState != ePlayerState.Looting)
            {
                if (SceneMng.Ins.nowScene > eScene.Dungeon)
                {
                    // 발도 상태라면 공격 가능
                    if (characterInfo.weaponState == eUsingWeaponState.DRAW)
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            moveDir = Vector3.zero;
                            Attack();
                        }
                    }
                }

                // 구르거나 어택중이 아닐 때는 움직 일 수 있음
                if (characterInfo.playerState != ePlayerState.Attack && dontMoveFlag == false)
                {
                    h = Input.GetAxis("Horizontal");
                    v = Input.GetAxis("Vertical");

                    moveDir = new Vector3(Camera.main.transform.right.x * h, 0, Camera.main.transform.right.z * h);
                    moveDir += new Vector3(Camera.main.transform.forward.x * v, 0, Camera.main.transform.forward.z * v);

                    if (moveDir != Vector3.zero)
                    {
                        characterInfo.playerState = ePlayerState.WALK;
                        characterInfo.playerAnimator.SetBool("Walk", true);
                        Quaternion newRotation  = Quaternion.LookRotation(moveDir.normalized);
                        transform.rotation      = Quaternion.Slerp(transform.rotation, newRotation, rotateSpeed * Time.deltaTime);
                    }
                    // 움직이는 상태가 아니라면
                    else
                    {
                        characterInfo.playerAnimator.SetBool("Walk", false);
                        characterInfo.playerState = ePlayerState.IDLE;
                    }

                    if (SceneMng.Ins.nowScene > eScene.Dungeon)
                    {
                        // 발도
                        if (Input.GetMouseButtonDown(0) && characterInfo.playerState != ePlayerState.ROLLING && characterInfo.weaponState == eUsingWeaponState.SHEATING)
                        {
                            moveDir = Vector3.zero;
                            StartCoroutine(Drawing());
                        }
                        // 납도
                        if (Input.GetKeyDown(KeyCode.LeftShift) && characterInfo.playerState != ePlayerState.ROLLING && characterInfo.weaponState == eUsingWeaponState.DRAW)
                        {
                            moveDir = Vector3.zero;
                            StartCoroutine(Sheating());
                        }
                        // 루팅
                        if( Input.GetMouseButtonDown(1) && characterInfo.weaponState == eUsingWeaponState.SHEATING && characterInfo.lootingTrigger != null && characterInfo.lootingTrigger.CheckCanLooting())
                        {
                            eItemID tempItem = characterInfo.lootingTrigger.lootingInfo.lootingItem;
                            if (characterInfo.characterInfo.characterInventory.GetItemCount(tempItem) >= TableMng.Ins.itemTb[tempItem].characterLimit) return;
                            moveDir = Vector3.zero;
                            StartCoroutine(Looting());
                        }
                        // 구르기
                        if (Input.GetKeyDown(KeyCode.Space))
                        {
                            if(characterInfo.currentStamina >= Define.ROLLING_STAMINA)
                            {
                                StartCoroutine(Rolling());
                                moveDir = Vector3.MoveTowards(Vector3.zero, transform.forward * rollingDistance, rollingSpeed);
                            }
                        }
                        // 아이템 사용
                        if (Input.GetKeyDown(KeyCode.E) && characterInfo.weaponState == eUsingWeaponState.SHEATING)
                        {
                            if(characterInfo.selectedItem != eItemID.None)
                                GetConsume();
                        }
                    }
                }
            }
        }

        moveDir.y -= gravity * Time.deltaTime;
        controller.Move(moveDir * moveSpeed * Time.deltaTime);
    }

    private void Attack()
    {
        isAttack = true;

        if (characterInfo.playerState != ePlayerState.Attack)
        {
            characterInfo.playerState = ePlayerState.Attack;
            characterInfo.playerAnimator.SetBool("Walk", false);
            characterInfo.playerAnimator.SetBool("IsAttack", true);
            characterInfo.playerAnimator.SetInteger("AttackCategory", (int)eAttackCategory.Attack1);
            isAttack = false;
        }
    }

    // 다시 살아나기 ( 정보 초기화 )
    public void Revive()
    {
        characterInfo.ReviveInit();
        StateInitialize();
        characterInfo.equipWeapon.SetSheatingPos(characterInfo);
        UIMng.Ins.dungeon.characterInfoUI.Visualize();
    }

    IEnumerator Rolling()
    {
        isRecoveryStamina = false;
        characterInfo.playerAnimator.SetTrigger("Roll");
        characterInfo.playerState = ePlayerState.ROLLING;
        characterInfo.currentStamina -= Define.ROLLING_STAMINA;
        UIMng.Ins.dungeon.characterInfoUI.characterStateUI.UpdateStaminaGaze();
        SoundMng.Ins.InternalSoundPlay(eSound.RollingSound.ToDesc(), playerAudioSource);
        yield return new WaitForSeconds(rollDuration);
        characterInfo.playerState = ePlayerState.IDLE;
        isRecoveryStamina = true;
    }

    IEnumerator Drawing()
    {
        dontMoveFlag = true;
        characterInfo.playerAnimator.SetBool("Draw", true);
        SoundMng.Ins.InternalSoundPlay(eSound.WeaponDrawSound.ToDesc(), playerAudioSource);
        yield return new WaitForSeconds(drawDuration);
        if (characterInfo.playerState != ePlayerState.Damaged)
            dontMoveFlag = false;
    }

    IEnumerator Sheating()
    {
        dontMoveFlag = true;
        characterInfo.playerAnimator.SetBool("Draw", false);
        SoundMng.Ins.InternalSoundPlay(eSound.WeaponSheatingSound.ToDesc(), playerAudioSource);
        yield return new WaitForSeconds(sheathingDuration);
        if(characterInfo.playerState != ePlayerState.Damaged)   dontMoveFlag = false;
    }

    IEnumerator Looting()
    {
        dontMoveFlag = true;
        characterInfo.playerState = ePlayerState.Looting;
        characterInfo.playerAnimator.SetTrigger("Looting");
        SoundMng.Ins.InternalSoundPlay(eSound.LootingSound.ToDesc(), playerAudioSource);
        yield return new WaitForSeconds(lootingDuration);
        if (characterInfo.playerState != ePlayerState.Damaged)
        {
            characterInfo.playerState = ePlayerState.IDLE;
            dontMoveFlag = false;
        }
    }

    #region Damaged Func

    public void GetDamaged(float damage)
    {
        // 데미지 받는 중엔 데미지 받기 NO
        if (characterInfo.playerState == ePlayerState.Death || characterInfo.playerState == ePlayerState.Damaged || characterInfo.playerState == ePlayerState.ROLLING) return;
        StartCoroutine(Damaged(damage));
    }

    IEnumerator Damaged(float damage)
    {
        SoundMng.Ins.InternalSoundPlay(eSound.DamagedSound.ToDesc(), playerAudioSource);

        dontMoveFlag                = true;
        moveDir                     = Vector3.zero;
        characterInfo.playerState   = ePlayerState.Damaged;
        float recieveDamage         = damage - characterInfo.defense;
        if (recieveDamage < 0) recieveDamage = 0f;
        characterInfo.currentHp     -= recieveDamage;
        characterInfo.recoveryHp    = characterInfo.currentHp + (recieveDamage / 2f);
        characterInfo.hpState       = eHPState.Damaged;

        UIMng.Ins.dungeon.characterInfoUI.characterStateUI.GetDamaged();
        StateInitialize();

        if (characterInfo.currentHp <= 0)
        {
            QuestMng.Ins.deathCount++;

            if (QuestMng.Ins.deathCount >= QuestMng.Ins.nowQuest.quest.questLife)
            {
                SoundMng.Ins.ExternalSoundPlay(eSound.QuestFailed.ToDesc(), eChannel.BackGround);
                Camera.main.GetComponent<ScreenShot>().CaptureScreenShot(eQuestResultCategory.Fail);
                UIMng.Ins.dungeon.TurnOffAllUI();
            }
            else
                SoundMng.Ins.ExternalSoundPlay(eSound.DeadSound.ToDesc(), eChannel.BackGround);

            StartCoroutine(UIMng.Ins.dungeon.DeathFadeIn());
            characterInfo.playerState = ePlayerState.Death;
            characterInfo.playerAnimator.SetTrigger("Death");
            characterInfo.hpState = eHPState.IDLE;
            characterInfo.currentHp = 0f;
            characterInfo.recoveryHp = 0f;
            MonsterMng.Ins.MissTarget();
        }
        else
            characterInfo.playerAnimator.SetTrigger("Damaged");

        yield return new WaitForSeconds(damagedDuration);

        if(characterInfo.playerState != ePlayerState.Death)
        {
            characterInfo.playerState = ePlayerState.IDLE;
            dontMoveFlag = false;
        }
    }

    // 상태 초기화 함수
    private void StateInitialize()
    {
        if(characterInfo.selectedItem != eItemID.None)
        {
            if (!characterInfo.characterInfo.characterInventory.itemList.ContainsKey(characterInfo.selectedItem))
                characterInfo.selectedItem = eItemID.None;
        }
        WeaponColliderOff();
        ClearHitTargetList();
        characterInfo.playerAnimator.SetBool("Walk", false);
        characterInfo.playerAnimator.SetBool("IsAttack", false);
        characterInfo.playerAnimator.SetInteger("AttackCategory", (int)eAttackCategory.None);

        if (characterInfo.weaponState == eUsingWeaponState.SHEATING)
            characterInfo.playerAnimator.SetBool("Draw", false);
        else
            characterInfo.playerAnimator.SetBool("Draw", true);
    }

    #endregion

    #region Consume Func

    public void GetConsume()
    {
        // 데미지 받는 중엔 데미지 받기 NO
        if (characterInfo.playerState == ePlayerState.Consume || characterInfo.playerState == ePlayerState.Damaged || 
            characterInfo.playerState == ePlayerState.ROLLING || characterInfo.selectedItem == eItemID.None) return;

        StartCoroutine(Consume());
    }

    IEnumerator Consume()
    {
        dontMoveFlag                = true;
        moveDir                     = Vector3.zero;
        characterInfo.playerState   = ePlayerState.Consume;
        characterInfo.usingItem     = characterInfo.selectedItem;

        characterInfo.playerAnimator.SetTrigger("Consume");

        bool isRemain = true;

        if(characterInfo.selectedItem != eItemID.Whetstone)
        {
            // isRemain가 false면 해당 아이템을 다 사용했다는 것
            isRemain = characterInfo.characterInfo.characterInventory.DeductItemAmount(characterInfo.selectedItem, 1);

            // UI 업데이트
            ConsumeItemUI consumeItemUI = UIMng.Ins.dungeon.characterInfoUI.consumeItemUI;
            if (isRemain)
                consumeItemUI.UpdateItemAmountUI();
            else
            {
                consumeItemUI.ResetSelectedSlot();
                consumeItemUI.consumeList.RemoveNode(consumeItemUI.index);
                consumeItemUI.fixedIndexFlag = true;    // index 통제 플래그
            }
        }
        
        yield return new WaitForSeconds(consumeDuration);

        if (characterInfo.playerState != ePlayerState.Damaged )
        {
            if (!isRemain)  characterInfo.selectedItem = eItemID.None;

            characterInfo.playerState   = ePlayerState.IDLE;
            dontMoveFlag                = false;
        }
    }

    #endregion

    #region AnimationEvents
    
    public void ClearHitTargetList()
    {
        foreach (var node in HitTargetList)
        {
            node.my.isDamaged = false;
        }
        HitTargetList.Clear();
    }

    public void WeaponColliderOff()
    {
        characterInfo.equipWeapon.weaponCollider.enabled = false;
        characterInfo.equipWeapon.trail.Stop();
    }

    public void WeaponColliderOn()
    {
        SoundMng.Ins.InternalSoundPlay(eSound.AttackSound.ToDesc(), playerAudioSource);
        characterInfo.equipWeapon.weaponCollider.enabled = true;
        characterInfo.equipWeapon.trail.Play();
    }

    #endregion
}