﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerCamera : MonoBehaviour
{
    #region INSPECTOR

    private CinemachineFreeLook freeLook;

    #endregion

    private void Awake()
    {
        freeLook = GetComponent<CinemachineFreeLook>();    
    }

    public void ResetFirstPosition()
    {
        // 플레이어캠 원 위치로 돌리기 ( FreeLook Value 초기 수치 )
        freeLook.m_YAxis.Value = 0.4985347f;
        freeLook.m_XAxis.Value = -0.8369539f;
    }
}
