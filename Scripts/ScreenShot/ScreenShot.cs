﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ScreenShot : MonoBehaviour
{
    private int     resWidth;
    private int     resHeight;
    private string  path;

    private void Start()
    {
        resWidth    = Screen.width;
        resHeight   = Screen.height;
        path        = string.Format("{0}{1}", Application.dataPath, PATH.ScreenShotPath);
    }

    public void CaptureScreenShot(eQuestResultCategory questResultCategory)
    {
        StartCoroutine(Capture(questResultCategory));
    }

    IEnumerator Capture(eQuestResultCategory questResultCategory)
    {
        yield return new WaitForEndOfFrame();
        DirectoryInfo dir = new DirectoryInfo(path);
        if (!dir.Exists)
            Directory.CreateDirectory(path);
        string pngName = "";
        if (questResultCategory == eQuestResultCategory.Clear)
            pngName = "ClearMoment.png";
        else if (questResultCategory == eQuestResultCategory.Fail)
            pngName = "FailMoment.png";

        string name = string.Format("{0}{1}", path, pngName);
        Camera.main.targetTexture = RenderTexture.GetTemporary(resWidth, resHeight, 16);
        RenderTexture renderTexture = Camera.main.targetTexture;

        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.ARGB32, false);

        Camera.main.Render();
        RenderTexture.active = renderTexture;

        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        screenShot.Apply();

        byte[] bytes = screenShot.EncodeToPNG();
        File.WriteAllBytes(name, bytes);

        RenderTexture.ReleaseTemporary(renderTexture);
        Camera.main.targetTexture = null;
    }
}
