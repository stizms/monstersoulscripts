﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneMng : MonoBehaviour
{
    #region Singleton

    private static SceneMng instance = null;

    public static SceneMng Ins
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SceneMng>();
                if (instance == null)
                {
                    instance = new GameObject("SceneMng", typeof(SceneMng)).GetComponent<SceneMng>();
                    instance.transform.position = new Vector3(1000, 1000, 1000);
                }
            }
            return instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    #endregion

    [HideInInspector]
    public eScene nextScene = eScene.None;
    [HideInInspector]
    public eScene nowScene  = eScene.Intro;

    private const eScene loadingScene = eScene.Loading;
    
    // 전환 할 씬 리스트
    [HideInInspector]
    public List<string> loadSceneList = new List<string>();

    // 씬 전환이 완료되었음을 나타내는 Flag
    public bool loadingComplete = false;

    // 씬 전환 함수
    public void ChangeScene(eScene nextSceneID)
    {
        loadSceneList.Clear();

        nextScene = nextSceneID;
        
        // 던전 입장이라면 로딩 씬으로 가기 / 아니면 다음 씬 로딩
        if ((int)nextScene > (int)eScene.Dungeon || nextScene == eScene.Lobby)
            loadSceneList.Add(loadingScene.ToDesc());
        else
            loadSceneList.Add(nextScene.ToDesc());

        EquipmentMng.Ins.ClearPool();
        AtlasMng.Ins.ClearPool();

        nowScene = nextSceneID;
        StartCoroutine(LoadSceneAsync(loadSceneList,null));
    }

    //// 다음 씬이 로딩 완료 때까지 대기 ( 로딩씬의 경우 로딩되는 progress를 보여주기위해 Image 매개변수를 받음 )
    public IEnumerator LoadSceneAsync(List<string> loadingList, Image progress = null)
    {
        int loadIndex = 0;

        // active 된 씬 담아두기
        List<UnityEngine.SceneManagement.Scene> originScenes = new List<UnityEngine.SceneManagement.Scene>();
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            originScenes.Add(SceneManager.GetSceneAt(i));
        }

        List<AsyncOperation> sceneLoads = new List<AsyncOperation>();

        //씬 로딩 등록
        for (int i = loadingList.Count - 1; i >= 0; i--)
        {
            sceneLoads.Add(SceneManager.LoadSceneAsync(loadingList[i], LoadSceneMode.Additive));
            sceneLoads[loadIndex].allowSceneActivation = false;
            while (sceneLoads[loadIndex].progress < 0.8f)
            {
                if (progress != null)
                    progress.fillAmount = sceneLoads[loadIndex].progress * (1f / (float)loadingList.Count);
                yield return null;
            }
            if (progress != null)
            {
                progress.fillAmount = 1f / (float)(i);
            }
            loadIndex++;
        }
        
        // 전환해주기
        for (int i = 0; i < sceneLoads.Count; i++)
        {
            sceneLoads[i].allowSceneActivation = true;
            while (!sceneLoads[i].isDone) { yield return null; }
        }
        
        // 메인 씬 세팅
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(loadingList[0]));
        loadingComplete = true;

        // active 된 씬 제거
        for (int i = 0; i < originScenes.Count; i++)
        {
            AsyncOperation unloadingScene = SceneManager.UnloadSceneAsync(originScenes[i]);
            while (!unloadingScene.isDone) { yield return null; }
        }
    }
}
