﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageTextMng : MonoBehaviour
{
    #region Singleton

    private static DamageTextMng instance = null;
    public static DamageTextMng Ins
    {
        get
        {
            if (SceneMng.Ins.nowScene > eScene.Dungeon)
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<DamageTextMng>();
                    if (instance == null)
                    {
                        instance = new GameObject("DamageTextMng", typeof(DamageTextMng)).GetComponent<DamageTextMng>();
                        instance.transform.position = new Vector3(1000, 1000, 1000);
                    }

                }
            }
            else
            {
                Debug.Log("데미지 텍스트 매니저는 던전 씬에서만 사용이 가능한 매니저입니다.");
            }
            return instance;
        }
    }
    #endregion


    #region INSPECTOR

    public Transform canvas;

    #endregion

    private Color weaknessDamageColor   = new Color(0.8666f, 0.4549f, 0.0705f);
    private Color normalDamageColor     = new Color(0.8509f, 0.8274f, 0.8039f);

    // 데미지 텍스트 풀
    public List<DamageText> damageTextPool = new List<DamageText>();
    
    public void PoolingText()
    {
        for(int i = 0; i < Define.DamageText_Num; i++)
        {
            GameObject tempText = Instantiate(Resources.Load(string.Format("{0}{1}", PATH.DamageTextPath,"DamageText")), canvas) as GameObject;
            tempText.SetActive(false);
            damageTextPool.Add(tempText.GetComponent<DamageText>());
        }
    }

    // 텍스트 뿌려주기
    public void GetDamageText(Vector3 target,float damage,bool isWeakness)
    {
        Color color;
        if (isWeakness)
            color = weaknessDamageColor;
        else
            color = normalDamageColor;

        for (int i = 0; i< damageTextPool.Count; i++)
        {
            if(damageTextPool[i].gameObject.activeSelf == false)
            {
                damageTextPool[i].SetData(target,damage,color);
                StartCoroutine(damageTextPool[i].DamageTextRoutine());
                return;
            }
            if (i == damageTextPool.Count - 1)
                PoolingText();
        }
    }
}
