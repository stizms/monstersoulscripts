﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMng : MonoBehaviour
{
    #region Singleton

    private static UIMng instance = null;
    
    public static UIMng Ins
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<UIMng>();
                if (instance == null)
                {
                    instance = new GameObject("UIMng", typeof(UIMng)).GetComponent<UIMng>();
                    instance.transform.position = new Vector3(1000, 1000, 1000);
                }
            }
            return instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    #endregion


    // 전역적으로 쓰는 UI 모음
    #region INSPECTOR
    
    // 팝업
    public PopUpUI      oneButtonPopUp;
    public PopUpUI      twoButtonPopUp;

    public GameObject   mentionUI;
    public Text         mentionText;

    public SaveUI       saveUI;

    // NPC등 플레이어가 근처에있을때 뜨는 이름판
    public InteractiveUI mouseInteractiveUI;

    // HUD
    public QuestHud     questHud;
    public AlarmHud     alarmHud;
    public LootingHud   lootingHud;

    // 옵션
    public OptionUI     optionUI;

    #endregion

    // 각 씬 UI 관리자
    public CharacterChoiceUI    characterChoice;
    public LobbyUI              lobby;
    public DungeonUI            dungeon;
    
    // 퀘스트Hud 키고 끄기
    public void QuestHudOn()
    {
        questHud.gameObject.SetActive(true);
    }

    public void QuestHudOff()
    {
        questHud.gameObject.SetActive(false);
    }

    // 팝업창 끄기 ( 델리게이트에 사용될 함수 팝업UI 스크립트 내에서 작동 함 )
    public void OnPopupExit()
    {
        return;
    }

    public void SaveLogoOn()
    {
        saveUI.gameObject.SetActive(true);
    }

    public void SaveLogoOff()
    {
        saveUI.gameObject.SetActive(false);
    }

    // 팝업이 켜져있는지 확인
    public bool CheckPopUpActive()
    {
        if (!oneButtonPopUp.gameObject.activeSelf && !twoButtonPopUp.gameObject.activeSelf)
            return false;
        else
            return true;
    }

    public void VisualizeMention(string str)
    {
        mentionText.text = str;
        mentionUI.gameObject.SetActive(true);
    }

    public void NonVisualizeMention()
    {
        mentionUI.gameObject.SetActive(false);
    }
}
