﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class GameMng : MonoBehaviour
{
    #region Singleton

    private static GameMng instance = null;
    public static GameMng Ins
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameMng>();
                if (instance == null)
                {
                    instance = new GameObject("GameMng", typeof(GameMng)).GetComponent<GameMng>();
                    instance.transform.position = new Vector3(1000, 1000, 1000);
                }
            }
            return instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    #endregion

    // 처음 시작할 때는 로딩 과정을 거치지 않는 것을 제어
    public bool firstPlay       = true;
    // 현재 인터렉팅 중인지 여부
    public bool isInteracting   = false;

    // 커서 끄기
    public void CursorNonVisualize()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // 커서 키기
    public void CursorVisualize()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void CursorReset()
    {
        StartCoroutine(CursorResetCoroutine());
    }

    IEnumerator CursorResetCoroutine()
    {
        CursorNonVisualize();
        yield return new WaitForSeconds(0.1f);
        CursorVisualize();
    }

    public void Lock()
    {
        isInteracting = true;
        Camera.main.GetComponent<CinemachineBrain>().enabled = false;
    }

    public void UnLock()
    {
        isInteracting = false;
        Camera.main.GetComponent<CinemachineBrain>().enabled = true;
    }
}
