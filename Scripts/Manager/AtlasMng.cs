﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class AtlasMng : MonoBehaviour
{
    #region Singleton

    private static AtlasMng instance = null;

    public static AtlasMng Ins
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<AtlasMng>();
                if (instance == null)
                    instance = new GameObject("AtlasMng", typeof(AtlasMng)).GetComponent<AtlasMng>();
            }
            return instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    #endregion

    // 아틀라스 리스트
    public SpriteAtlas questUIAtlas;
    public SpriteAtlas characterChoiceAtlas;
    public SpriteAtlas monsterAtlas;
    public SpriteAtlas descriptionUIAtlas;
    public SpriteAtlas itemAtlas;
    public SpriteAtlas equipmentAtlas;
    public SpriteAtlas minimapAtlas;
    public SpriteAtlas inventoryAtlas;
    public SpriteAtlas characterInfoAtlas;
    public SpriteAtlas dungeonAtlas;

    public void ClearPool()
    {
        questUIAtlas            = null;
        characterChoiceAtlas    = null;
        monsterAtlas            = null;
        descriptionUIAtlas      = null;
        itemAtlas               = null;
        equipmentAtlas          = null;
        minimapAtlas            = null;
        inventoryAtlas          = null;
        characterInfoAtlas      = null;
        dungeonAtlas            = null;
    }

    public Sprite GetSprite(eAtlasCategory atlasCategory,string spriteName)
    {
        Sprite sprite = GetAtlas(atlasCategory).GetSprite(spriteName);
        if (sprite == null)
            Debug.LogError("아틀라스에 해당 스프라이트가 없습니다.");

        return sprite;
    }

    public SpriteAtlas GetAtlas(eAtlasCategory atlasCategory)
    {
        switch(atlasCategory)
        {
            case eAtlasCategory.Quest:
                if (questUIAtlas == null) questUIAtlas = Resources.Load(string.Format("{0}{1}", PATH.AtlasPath, "QuestAtlas")) as SpriteAtlas;
                return questUIAtlas;
            case eAtlasCategory.Description:
                if (descriptionUIAtlas == null) descriptionUIAtlas = Resources.Load(string.Format("{0}{1}", PATH.AtlasPath, "DescriptionUIAtlas")) as SpriteAtlas;
                return descriptionUIAtlas;
            case eAtlasCategory.CharacterChoice:
                if (characterChoiceAtlas == null) characterChoiceAtlas = Resources.Load(string.Format("{0}{1}", PATH.AtlasPath, "CharacterChoiceAtlas")) as SpriteAtlas;
                return characterChoiceAtlas;
            case eAtlasCategory.Monster:
                if (monsterAtlas == null) monsterAtlas = Resources.Load(string.Format("{0}{1}", PATH.AtlasPath, "MonsterAtlas")) as SpriteAtlas;
                return monsterAtlas;
            case eAtlasCategory.Item:
                if (itemAtlas == null) itemAtlas = Resources.Load(string.Format("{0}{1}", PATH.AtlasPath, "ItemAtlas")) as SpriteAtlas;
                return itemAtlas;
            case eAtlasCategory.Equipment:
                if (equipmentAtlas == null) equipmentAtlas = Resources.Load(string.Format("{0}{1}", PATH.AtlasPath, "EquipmentAtlas")) as SpriteAtlas;
                return equipmentAtlas;
            case eAtlasCategory.Minimap:
                if (minimapAtlas == null) minimapAtlas = Resources.Load(string.Format("{0}{1}", PATH.AtlasPath, "MinimapAtlas")) as SpriteAtlas;
                return minimapAtlas;
            case eAtlasCategory.Inventory:
                if (inventoryAtlas == null) inventoryAtlas = Resources.Load(string.Format("{0}{1}", PATH.AtlasPath, "InventoryAtlas")) as SpriteAtlas;
                return inventoryAtlas;
            case eAtlasCategory.CharacterInfo:
                if (characterInfoAtlas == null) characterInfoAtlas = Resources.Load(string.Format("{0}{1}", PATH.AtlasPath, "CharacterInfoAtlas")) as SpriteAtlas;
                return characterInfoAtlas;
            case eAtlasCategory.Dungeon:
                if (dungeonAtlas == null) dungeonAtlas = Resources.Load(string.Format("{0}{1}", PATH.AtlasPath, "DungeonAtlas")) as SpriteAtlas;
                return dungeonAtlas;
            default:
                Debug.LogError("해당 아틀라스가 없습니다.");
                return null;
        }
    }
}
