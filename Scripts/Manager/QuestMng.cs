﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestMng : MonoBehaviour
{
    #region Singleton

    private static QuestMng instance = null;
    public static QuestMng Ins
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<QuestMng>();
                if (instance == null)
                    instance = new GameObject("QuestMng", typeof(QuestMng)).GetComponent<QuestMng>();
            }
            return instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    #endregion

    // 접근 가능한 퀘스트 목록
    public QuestTree    availableQuest = new QuestTree();
    // 현재 퀘스트
    public QuestNode    nowQuest        = null;
    public bool         isQuestReady    = false;
    public int          deathCount      = 0;

    // 퀘스트 수주
    public void SetQuest(QuestNode quest)
    {
        nowQuest    = quest;
        deathCount  = 0;
    }

    // 퀘스트 포기
    public void GiveUpQuest()
    {
        nowQuest.quest.ResetQuest();
        nowQuest        = null;
        isQuestReady    = false;
    }

    // 적이 죽으면서 호출하는 함수 ( 죽이는 퀘스트가 있다면 실행 됨 )
    public void MonsterDied(eMonsterID monsterID)
    {
        if (nowQuest == null || nowQuest.quest.sections.Count <= 0) return;

        Quest quest = nowQuest.quest;
        for (int i = 0; i < quest.sections.Peek().goals.Count; i++)
        {
            quest.sections.Peek().goals[i].MonsterDied(monsterID);
            if (quest.sections.Peek().CheckComplete())
            {
                quest.sections.Dequeue();
                if (quest.sections.Count > 0)
                    UIMng.Ins.questHud.AddSectionUI(quest.sections.Peek());
                else
                {
                    SoundMng.Ins.ExternalSoundPlay(eSound.QuestClear.ToDesc(), eChannel.BackGround);
                    Camera.main.GetComponent<ScreenShot>().CaptureScreenShot(eQuestResultCategory.Clear);
                    quest.Clear();
                    return;
                }
            }
        }
    }
    
    //수집품을 수집하면 호출하는 함수 ( 수집하는 퀘스트가 있다면 호출 됨 )
    public void PickedUpItem(eItemID itemID,int amount)
    {
        if (nowQuest == null || nowQuest.quest.sections.Count <= 0) return;

        Quest quest = nowQuest.quest;
        for (int i = 0; i < quest.sections.Peek().goals.Count; i++)
        {
            quest.sections.Peek().goals[i].PickedUpItem(itemID,amount);
            if (quest.sections.Peek().CheckComplete())
            {
                quest.sections.Dequeue();
                if (quest.sections.Count > 0)
                    UIMng.Ins.questHud.AddSectionUI(quest.sections.Peek());
                else
                {
                    SoundMng.Ins.ExternalSoundPlay(eSound.QuestClear.ToDesc(), eChannel.BackGround);
                    Camera.main.GetComponent<ScreenShot>().CaptureScreenShot(eQuestResultCategory.Clear);
                    quest.Clear();
                    return;
                }
            }
        }
    }
}
