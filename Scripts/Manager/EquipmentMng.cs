﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EquipmentMng : MonoBehaviour
{
    #region Singleton

    private static EquipmentMng instance = null;
    
    public static EquipmentMng Ins
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<EquipmentMng>();
                if (instance == null)
                    instance = new GameObject("EquipmentMng", typeof(EquipmentMng)).GetComponent<EquipmentMng>();
            }
            return instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    #endregion


    #region INSPECTOR

    public Transform poolingPos;

    #endregion


    // 무기 리스트
    public List<BaseWeapon>     weaponList      = new List<BaseWeapon>();
    // 장비 리스트
    public List<BaseEquipment>  equipmentList   = new List<BaseEquipment>();

    public void ClearPool()
    {
        // poolingPos 초기화
        poolingPos.parent = null;
        SceneManager.MoveGameObjectToScene(poolingPos.gameObject, SceneManager.GetActiveScene());
        poolingPos = new GameObject("poolingPos").GetComponent<Transform>();
        poolingPos.parent = transform;

        weaponList.Clear();
        equipmentList.Clear();
    }

    // 요청받은 무기를 전해주는 역할
    public BaseWeapon GetWeapon(eEquipment weaponID)
    {
        for (int i = 0; i < weaponList.Count; i++)
        {
            if (weaponList[i].weaponID == weaponID)
                return weaponList[i];
        }
        // 여기까지 도착하면 없다는 뜻
        GameObject tempWeapon = Instantiate(Resources.Load(string.Format("{0}{1}", PATH.WeaponPath, weaponID.ToDesc())), poolingPos) as GameObject;
        weaponList.Add(tempWeapon.GetComponent<BaseWeapon>());
        return weaponList[weaponList.Count - 1];
    }

    public BaseEquipment GetEquipment(eEquipment equipmentID)
    {
        if (equipmentID == eEquipment.None) return null;
        for (int i = 0; i < equipmentList.Count; i++)
        {
            if (equipmentList[i].equipmentID == equipmentID)
                return equipmentList[i];
        }
        // 여기까지 도착하면 없다는 뜻
        GameObject tempEquipment = Instantiate(Resources.Load(string.Format("{0}{1}", PATH.EquipmentPath, equipmentID.ToDesc())), poolingPos) as GameObject;
        equipmentList.Add(tempEquipment.GetComponent<BaseEquipment>());
        return equipmentList[equipmentList.Count - 1];
    }
}
