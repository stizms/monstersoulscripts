﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class UserDataMng : MonoBehaviour
{
    #region Singleton
    private static UserDataMng instance = null;

    public static UserDataMng Ins
    {
        get
        {
            if( instance == null)
            {
                instance = FindObjectOfType<UserDataMng>();
                if (instance == null)
                {
                    instance = new GameObject("UserDataMng", typeof(UserDataMng)).GetComponent<UserDataMng>();
                    instance.transform.position = new Vector3(1000, 1000, 1000);
                }
            }
            return instance;
        }
    }

    private void Awake()
    {
        path = string.Format("{0}{1}", Application.dataPath, PATH.CSVPath);
        DontDestroyOnLoad(this);
    }
    #endregion

    private const int   characterSlotNum = 3;   // 사용자가 만들 수 있는 캐릭터의 총 개수
    public  UserData[]   characterSlot = new UserData[characterSlotNum];
    public  int          selectedIndex = 0;     // characterSlot을 클릭하면 값이 바뀜.
    public  UserData     nowCharacter = null;
    private string      path;                   //CSVData 경로 ( Awake에서 초기화 해줌 )

    
    public void ReadUserData()
    {
        string[] userDataSplits;

        // 데이터 카테고리
        eUserDataCategory category = eUserDataCategory.None;

        for (int i = 0; i < characterSlot.Length; i++)
        {
            string fileStr = string.Format("{0}UserData_{1}.csv", path, i);
            FileInfo fi = new FileInfo(fileStr);
            // csv 파일이 존재한다면 해당 슬롯에 데이터 삽입
            if (fi.Exists)
            {        
                UserData tempUserData = new UserData();
                tempUserData.itemBoxInventory.InventoryClear();
                tempUserData.characterInventory.ItemListClear();

                using (StreamReader userDataSR = new StreamReader(fileStr))
                {
                    while(userDataSR.Peek() >= 0 )
                    {
                        userDataSplits = userDataSR.ReadLine().Split(',');
                        // 줄의 0번째 요소가 카테고리와 동일하다면 이제 줄바꿈이라고 보면 됨
                        try { category = (eUserDataCategory)Enum.Parse(typeof(eUserDataCategory), userDataSplits[0]); }
                        catch
                        {
                            switch (category)
                            {
                                case eUserDataCategory.CharacterInfo:
                                    tempUserData.characterName = userDataSplits[0];
                                    tempUserData.gender = (eGender)Enum.Parse(typeof(eGender), userDataSplits[1]);
                                    tempUserData.money = int.Parse(userDataSplits[2]);
                                    tempUserData.hunterRank = int.Parse(userDataSplits[3]);
                                    break;
                                case eUserDataCategory.EquipItem:
                                    tempUserData.equipWeapon = (eEquipment)Enum.Parse(typeof(eEquipment), userDataSplits[0]);
                                    tempUserData.equipHelmet = (eEquipment)Enum.Parse(typeof(eEquipment), userDataSplits[1]);
                                    tempUserData.equipVest = (eEquipment)Enum.Parse(typeof(eEquipment), userDataSplits[2]);
                                    tempUserData.equipGlove = (eEquipment)Enum.Parse(typeof(eEquipment), userDataSplits[3]);
                                    tempUserData.equipBelt = (eEquipment)Enum.Parse(typeof(eEquipment), userDataSplits[4]);
                                    tempUserData.equipPants = (eEquipment)Enum.Parse(typeof(eEquipment), userDataSplits[5]);
                                    break;
                                case eUserDataCategory.WeaponInventory:
                                    eEquipment tempWeapon = (eEquipment)Enum.Parse(typeof(eEquipment), userDataSplits[0]);
                                    tempUserData.itemBoxInventory.GetEquipment(tempWeapon);
                                    break;
                                case eUserDataCategory.HelmetInventory:
                                    eEquipment tempHelmet = (eEquipment)Enum.Parse(typeof(eEquipment), userDataSplits[0]);
                                    tempUserData.itemBoxInventory.GetEquipment(tempHelmet);
                                    break;
                                case eUserDataCategory.VestInventory:
                                    eEquipment tempVest = (eEquipment)Enum.Parse(typeof(eEquipment), userDataSplits[0]);
                                    tempUserData.itemBoxInventory.GetEquipment(tempVest);
                                    break;
                                case eUserDataCategory.GloveInventory:
                                    eEquipment tempGlove = (eEquipment)Enum.Parse(typeof(eEquipment), userDataSplits[0]);
                                    tempUserData.itemBoxInventory.GetEquipment(tempGlove);
                                    break;
                                case eUserDataCategory.BeltInventory:
                                    eEquipment tempBelt = (eEquipment)Enum.Parse(typeof(eEquipment), userDataSplits[0]);
                                    tempUserData.itemBoxInventory.GetEquipment(tempBelt);
                                    break;
                                case eUserDataCategory.PantsInventory:
                                    eEquipment tempPants = (eEquipment)Enum.Parse(typeof(eEquipment), userDataSplits[0]);
                                    tempUserData.itemBoxInventory.GetEquipment(tempPants);
                                    break;
                                // 아이템 박스의 소모 & 재료성 아이템
                                case eUserDataCategory.ItemBoxItemInventory:
                                    tempUserData.itemBoxInventory.GetItem((eItemID)Enum.Parse(typeof(eItemID), userDataSplits[0]), int.Parse(userDataSplits[1]));
                                    break;
                                    // 캐릭터 인벤토리의 소모 & 재료성 아이템
                                case eUserDataCategory.CharacterItemInventory:
                                    tempUserData.characterInventory.GetItem((eItemID)Enum.Parse(typeof(eItemID), userDataSplits[0]), int.Parse(userDataSplits[1]));
                                    break;
                                case eUserDataCategory.ClearQuestList:
                                    eQuestID tempClearQuest = (eQuestID)Enum.Parse(typeof(eQuestID), userDataSplits[0]);
                                    tempUserData.clearQuestList.Add(tempClearQuest, tempClearQuest);
                                    break;
                                default:
                                    Debug.Log("UserDataCategory Error");
                                    break;
                            }
                        }
                        Array.Clear(userDataSplits, 0, userDataSplits.Length);
                    }
                }
                characterSlot[i] = tempUserData;
            }
            // csv 파일이 존재하지 않는다면 null 상태로 넘어감 ( 안써도 되지만 직관적으로 보이기 위함 )
            else
            {
                continue;
            }
        }
    }
    
    public void SaveUserData(UserData userData)
    {
        string fileStr = string.Format("{0}UserData_{1}.csv", path, selectedIndex.ToString());
        FileInfo fi = new FileInfo(fileStr);

        // 저장 ( 2번째 매개변수가 false면 덮어쓰기 )
        using (StreamWriter userDataSW = new StreamWriter(fileStr,false,System.Text.Encoding.UTF8))
        {
            // 캐릭터 기본 정보 저장
            userDataSW.WriteLine(eUserDataCategory.CharacterInfo.ToString());
            userDataSW.WriteLine(string.Format("{0},{1},{2},{3},", userData.characterName, userData.gender, userData.money, userData.hunterRank));

            // 착용 장비 정보 저장
            userDataSW.WriteLine(eUserDataCategory.EquipItem.ToString());
            userDataSW.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", userData.equipWeapon,userData.equipHelmet,userData.equipVest,userData.equipGlove,userData.equipBelt,userData.equipPants));

            // 아이템 박스 무기 아이템 저장
            userDataSW.WriteLine(eUserDataCategory.WeaponInventory.ToString());
            foreach(var node in userData.itemBoxInventory.weaponItemList)
            {
                userDataSW.WriteLine(string.Format("{0},", node.Key));
            }
            // 아이템 박스 헬멧 아이템 저장
            userDataSW.WriteLine(eUserDataCategory.HelmetInventory.ToString());
            foreach (var node in userData.itemBoxInventory.helmetItemList)
            {
                userDataSW.WriteLine(string.Format("{0},", node.Key));
            }
            // 아이템 박스 베스트 아이템 저장
            userDataSW.WriteLine(eUserDataCategory.VestInventory.ToString());
            foreach (var node in userData.itemBoxInventory.vestItemList)
            {
                userDataSW.WriteLine(string.Format("{0},", node.Key));
            }
            // 아이템 박스 글러브 아이템 저장
            userDataSW.WriteLine(eUserDataCategory.GloveInventory.ToString());
            foreach (var node in userData.itemBoxInventory.gloveItemList)
            {
                userDataSW.WriteLine(string.Format("{0},", node.Key));
            }
            // 아이템 박스 벨트 아이템 저장
            userDataSW.WriteLine(eUserDataCategory.BeltInventory.ToString());
            foreach (var node in userData.itemBoxInventory.beltItemList)
            {
                userDataSW.WriteLine(string.Format("{0},", node.Key));
            }
            // 아이템 박스 팬츠 아이템 저장
            userDataSW.WriteLine(eUserDataCategory.PantsInventory.ToString());
            foreach (var node in userData.itemBoxInventory.pantsItemList)
            {
                userDataSW.WriteLine(string.Format("{0},", node.Key));
            }


            // 아이템 박스 소모 & 재료성 아이템 저장
            userDataSW.WriteLine(eUserDataCategory.ItemBoxItemInventory.ToString());
            foreach(var node in userData.itemBoxInventory.itemList)
            {
                userDataSW.WriteLine(string.Format("{0},{1},", node.Key, node.Value));
            }

            // 캐릭터 인벤토리 소모 & 재료성 아이템 저장
            userDataSW.WriteLine(eUserDataCategory.CharacterItemInventory.ToString());
            foreach (var node in userData.characterInventory.itemList)
            {
                userDataSW.WriteLine(string.Format("{0},{1},", node.Key, node.Value));
            }

            userDataSW.WriteLine(eUserDataCategory.ClearQuestList.ToString());
            foreach (var node in userData.clearQuestList)
            {
                userDataSW.WriteLine(node.Key.ToString());
            }
        }
    }
    
    public void DeleteUserData()
    {
        string fileStr = string.Format("{0}UserData_{1}.csv", path, selectedIndex);
        FileInfo fi = new FileInfo(fileStr);

        // csv 파일이 존재한다면 삭제
        if (fi.Exists)
            File.Delete(fileStr);
        else
        {
            Debug.Log("삭제할 파일이 없음! 에러!");
            return;
        }

        characterSlot[selectedIndex] = null;
    }
}
