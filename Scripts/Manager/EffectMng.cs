﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectMng : MonoBehaviour
{
    #region Singleton

    private static EffectMng instance = null;
    public static EffectMng Ins
    {
        get
        {
            if(SceneMng.Ins.nowScene > eScene.Dungeon)
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<EffectMng>();
                    if (instance == null)
                    {
                        instance = new GameObject("ParticleMng", typeof(EffectMng)).GetComponent<EffectMng>();
                        instance.transform.position = new Vector3(1000, 1000, 1000);
                    }
                }
            }
            else
            {
                Debug.Log("던전 씬에서만 사용할 수 있습니다.");
                return null;
            }
            return instance;
        }
    }

    #endregion

    public Dictionary<eEffect, List<GameObject>> effectPool = new Dictionary<eEffect, List<GameObject>>();

    public void PoolingEffect(eEffect effectID)
    {
        for(int i = 0; i < Define.Particle_Num; i++)
        {
            GameObject effect = Instantiate(Resources.Load(string.Format("{0}{1}", PATH.EffectPath, effectID.ToString())), transform) as GameObject;
            effect.SetActive(false);

            if (!effectPool.ContainsKey(effectID))
                effectPool.Add(effectID, new List<GameObject>());

            effectPool[effectID].Add(effect);
        }
    }

    public void DisplayEffect(Vector3 pos, eEffect effectID)
    {
        if (effectPool.ContainsKey(effectID))
        {
            for(int i = 0; i < effectPool[effectID].Count; i++)
            {
                if(effectPool[effectID][i].activeSelf == false)
                {
                    effectPool[effectID][i].transform.position = pos;
                    effectPool[effectID][i].SetActive(true);
                    StartCoroutine(ReturnToPool(effectPool[effectID][i]));
                    break;
                }
                if( i == effectPool[effectID].Count -1)
                    PoolingEffect(effectID);
            }
        }
        else
        {
            PoolingEffect(effectID);
            DisplayEffect(pos,effectID);
        }
    }

    private IEnumerator ReturnToPool(GameObject target)
    {
        yield return new WaitForSeconds(1f);
        target.transform.position = target.transform.parent.position;
        target.SetActive(false);
    }
}
