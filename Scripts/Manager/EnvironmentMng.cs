﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentMng : MonoBehaviour
{
    #region Singleton

    private static EnvironmentMng instance = null;

    public static EnvironmentMng Ins
    {
        get
        {
            if (SceneMng.Ins.nowScene > eScene.Dungeon)
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<EnvironmentMng>();
                    if (instance == null)
                    {
                        instance = new GameObject("EnvironmentMng", typeof(EnvironmentMng)).GetComponent<EnvironmentMng>();
                        instance.transform.position = new Vector3(1000, 1000, 1000);
                    }
                }
                return instance;
            }
            else
            {
                Debug.Log("던전 씬에서만 사용할 수 있습니다.");
                return null;
            }
        }
    }

    #endregion

    // 몬스터 풀
    private Dictionary<eEnvironmentID, List<LootingEnvironment>> environmentPool = new Dictionary<eEnvironmentID, List<LootingEnvironment>>();

    public void PoolingEnvironment(eEnvironmentID environmentID)
    {
        for (int i = 0; i < Define.Environment_Pooling_Num; i++)
        {
            GameObject tempEnvironment = Instantiate(Resources.Load(string.Format("{0}{1}", PATH.EnvironmentPath, environmentID.ToString())), transform) as GameObject;
            tempEnvironment.SetActive(false);

            if (!environmentPool.ContainsKey(environmentID))
                environmentPool.Add(environmentID, new List<LootingEnvironment>());

            environmentPool[environmentID].Add(tempEnvironment.GetComponent<LootingEnvironment>());
        }
    }

    // 몬스터 생성 위치 지정된 transform에서 이것들 호출해서 몬스터 세팅함
    public LootingEnvironment GetEnvironment(eEnvironmentID environmentID)
    {
        if (!environmentPool.ContainsKey(environmentID) || environmentID == eEnvironmentID.None)
        {
            Debug.LogError("환경 풀에 등록되지 않은 환경오브젝트 종류입니다.");
            return null;
        }

        // 사용 가능한 몬스터를 찾아서 리턴 없다면 만들어서 리턴
        for (int i = 0; i < environmentPool[environmentID].Count; i++)
        {
            if (environmentPool[environmentID][i].gameObject.activeSelf == false)
            {
                return environmentPool[environmentID][i];
            }
            if (i == environmentPool[environmentID].Count - 1)
                PoolingEnvironment(environmentID);
        }

        Debug.Log("여기는 호출이 안됩니다.");
        return null;
    }
}
