﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 던전씬에서 사용하는 몬스터 매니저

public class MonsterMng : MonoBehaviour
{
    #region Singleton

    private static MonsterMng instance = null;

    public static MonsterMng Ins
    {
        get
        {
            if (SceneMng.Ins.nowScene > eScene.Dungeon)
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<MonsterMng>();
                    if (instance == null)
                    {
                        instance = new GameObject("MonsterMng", typeof(MonsterMng)).GetComponent<MonsterMng>();
                        instance.transform.position = new Vector3(1000, 1000, 1000);
                    }
                }
                return instance;
            }
            else
            {
                Debug.Log("던전 씬에서만 사용할 수 있습니다.");
                return null;
            }
        }
    }

    #endregion
    
    // 몬스터 풀
    private Dictionary<eMonsterID, List<BaseMonster>> monsterPool   = new Dictionary<eMonsterID, List<BaseMonster>>();
    public List<BaseMonster> activeMonsterPool                      = new List<BaseMonster>();

    // 던전 씬에서 몬스터 풀링을 요청하는 것 ( 던전에 필요한 몬스터들만 풀링 요청함 )
    public void PoolingMonster(eMonsterID monsterID)
    {
        for(int i = 0; i < Define.Monster_Pooling_Num; i++)
        {
            GameObject tempMonster = Instantiate(Resources.Load(string.Format("{0}{1}", PATH.MonsterPath, monsterID.ToString())), transform)as GameObject;
            tempMonster.SetActive(false);

            if (!monsterPool.ContainsKey(monsterID))
                monsterPool.Add(monsterID, new List<BaseMonster>());

            monsterPool[monsterID].Add(tempMonster.GetComponent<BaseMonster>());
        }
    }
    
    // 몬스터 생성 위치 지정된 transform에서 이것들 호출해서 몬스터 세팅함
    public BaseMonster GetMonster(eMonsterID monsterID)
    {
        if (!monsterPool.ContainsKey(monsterID) || monsterID == eMonsterID.None)
        {
            Debug.LogError("몬스터풀에 등록되지 않은 몬스터 종류입니다.");
            return null;
        }
        
        // 사용 가능한 몬스터를 찾아서 리턴 없다면 만들어서 리턴
        for(int i = 0; i < monsterPool[monsterID].Count; i++)
        {
            if(monsterPool[monsterID][i].gameObject.activeSelf == false)
            {
                activeMonsterPool.Add(monsterPool[monsterID][i]);
                return monsterPool[monsterID][i];
            }
            if( i == monsterPool[monsterID].Count -1)
                PoolingMonster(monsterID);
        }

        Debug.Log("여기는 호출이 안됩니다.");
        return null;
    }

    public void MissTarget()
    {
        foreach(var node in activeMonsterPool)
        {
            node.SetStateIdle();
            node.target = null;
        }
    }

    public void SetTarget()
    {
        Character target = DungeonMng.Ins.dungeonScene.player;
        foreach (var node in activeMonsterPool)
        {
            node.target = target;
        }
    }
}
