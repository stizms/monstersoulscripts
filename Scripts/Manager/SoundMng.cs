﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip clip;

    public void SetClipInfo(AudioSource source)
    {
        source.clip = clip;
    }
}

public class SoundMng : MonoBehaviour
{
    #region Singleton

    private static SoundMng instance = null;

    public static SoundMng Ins
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<SoundMng>();
                if (instance == null)
                {
                    instance = new GameObject("SoundMng", typeof(SoundMng)).GetComponent<SoundMng>();
                    instance.transform.position = new Vector3(1000, 1000, 1000);
                }
            }
            return instance;
        }
    }

    private void Awake()
    {
        Init();
        DontDestroyOnLoad(this);
    }

    #endregion


    [Range(0f,1f)]
    public float backGroundVolume = 1f;
    [Range(0f,1f)]
    public float soundEffectVolume = 1f;

    // 보스 만났을 때 BGM 바뀌는 플래그
    public bool isBossBGM = false;


    [SerializeField]
    private Sound[] sounds = null;

    // 사운드 매니저가 관리 할 오디오 소스들
    public AudioSource backGroundChannel;
    public List<AudioSource> UIEffectChannel;

    // 게임캐릭터에 등록되어있는 오디오 소스들 객체가 생성될 때 등록하고, 객체가 파괴될 때 제거함.
    public List<AudioSource> gameEffectChannel;
    
    // Awake 대신
    private void Init()
    {
        GameObject backGroundTemp = new GameObject("BackGroundSource");
        backGroundTemp.transform.parent = transform;
        backGroundChannel = backGroundTemp.AddComponent<AudioSource>();
        backGroundChannel.loop = true;

        for (int i = 0; i < 20; i++)
        {
            GameObject UITemp = new GameObject("UIEffectSource_" + i);
            UITemp.transform.parent = transform;
            UIEffectChannel.Add(UITemp.AddComponent<AudioSource>());
            UIEffectChannel[i].loop = false;
        }
    }

    // 외부 사운드 재생 함수
    public void ExternalSoundPlay(string name, eChannel sourceType)
    {
        AudioSource channel = null;

        switch(sourceType)
        {
            case eChannel.BackGround:
                channel = backGroundChannel;
                break;
            case eChannel.UI_Effect:
                for(int i = 0; i < UIEffectChannel.Count; i++)
                {
                    if(UIEffectChannel[i].isPlaying == false)
                    {
                        channel = UIEffectChannel[i];
                        break;
                    }
                }
                break;
        }

        for(int j = 0; j < sounds.Length; j++)
        {
            if(sounds[j].name == name)
            {
                sounds[j].SetClipInfo(channel);
                channel.Play();
                return;
            }
        }
    }

    // 내부 사운드 재생 함수
    public void InternalSoundPlay(string name, AudioSource channel)
    {
        for (int j = 0; j < sounds.Length; j++)
        {
            if (sounds[j].name == name)
            {
                sounds[j].SetClipInfo(channel);
                channel.Play();
                return;
            }
        }
    }

    // 백그라운드 사운드 종료
    public void StopBackGroundSound()
    {
        backGroundChannel.Stop();
    }

    public void ChangeBackgroundVolume(float volume)
    {
        backGroundVolume = volume;
        UIMng.Ins.optionUI.audioOption.UpdateBackGroundVolumeUI(backGroundVolume);
        backGroundChannel.volume = backGroundVolume;
    }

    public void ChangeEffectVolume(float volume)
    {
        soundEffectVolume = volume;
        UIMng.Ins.optionUI.audioOption.UpdateSoundEffectVolumeUI(soundEffectVolume);

        for (int i = 0; i < UIEffectChannel.Count; i++)
        {
            UIEffectChannel[i].volume = soundEffectVolume;
        }

        for (int i = 0; i < gameEffectChannel.Count; i++)
        {
            gameEffectChannel[i].volume = soundEffectVolume;
        }
    }
}

public enum eChannel
{
    BackGround,
    UI_Effect,
    Game_Effect,
}

// 혹시 파일 이름이 다를 수 있으므로 Description 쓰겠음
public enum eSound
{
    [Description("IntroBGM")]
    IntroBGM,

    [Description("LobbyBGM")]
    LobbyBGM,

    [Description("IntroClickSound")]
    IntroClickSound,

    [Description("BtnClickSound")]
    BtnClickSound,

    [Description("BtnSwapHoverSound")]
    BtnSwapHoverSound,

    [Description("CharacterSlotHoverSound")]
    CharacterSlotHoverSound,

    [Description("CharacterSlotClickSound")]
    CharacterSlotClickSound,

    [Description("PopUpClickSound")]
    PopUpClickSound,

    [Description("PopUpHoverSound")]
    PopUpHoverSound,

    [Description("DungeonBGM")]
    DungeonBGM,

    [Description("QuestClear")]
    QuestClear,

    [Description("QuestFailed")]
    QuestFailed,

    [Description("MeetBossBGM")]
    MeetBossBGM,

    [Description("ItemBoxBtnClick")]
    ItemBoxBtnClick,

    [Description("ItemSlotClick")]
    ItemSlotClick,

    [Description("EquipmentChange")]
    EquipmentChange,

    [Description("MoneyChange")]
    MoneyChange,

    [Description("QuestOkaySound")]
    QuestOkaySound,

    [Description("QuestRecieve")]
    QuestRecieve,

    [Description("DungeonLoadingSound")]
    DungeonLoadingSound,

    [Description("QuestNpcOpen")]
    QuestNpcOpen,

    [Description("QuestExitSound")]
    QuestExitSound,

    [Description("ConsumeSlotMove")]
    ConsumeSlotMove,

    [Description("WhetStoneSound")]
    WhetStoneSound,

    [Description("PotionSound")]
    PotionSound,

    [Description("RationSound")]
    RationSound,

    [Description("AlarmSound")]
    AlarmSound,

    [Description("HitSound")]
    HitSound,

    [Description("LootingSound")]
    LootingSound,

    [Description("RollingSound")]
    RollingSound,

    [Description("AttackSound")]
    AttackSound,

    [Description("WeaponDrawSound")]
    WeaponDrawSound,

    [Description("WeaponSheatingSound")]
    WeaponSheatingSound,

    [Description("DeadSound")]
    DeadSound,

    [Description("DamagedSound")]
    DamagedSound,

    [Description("WingSound")]
    WingSound,

    [Description("Rathian_Roar")]
    Rathian_Roar,

    [Description("FireBallSound")]
    FireBallSound,
}