﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMng : MonoBehaviour
{
    #region Singleton

    private static ProjectileMng instance = null;
    public static ProjectileMng Ins
    {
        get
        {
            if (SceneMng.Ins.nowScene > eScene.Dungeon)
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<ProjectileMng>();
                    if (instance == null)
                        instance = new GameObject("ProjectileMng", typeof(ProjectileMng)).GetComponent<ProjectileMng>();
                }
            }
            else
            {
                Debug.Log("투사체 매니저는 던전 씬에서만 사용이 가능한 매니저입니다.");
            }
            return instance;
        }
    }

    #endregion
    
    Dictionary<eProjectile, List<BaseProjectile>> projectilePool = new Dictionary<eProjectile, List<BaseProjectile>>();

    public void PoolingProjectile(eProjectile projectileID)
    {
        if (projectileID == eProjectile.None) return;
        for(int i = 0; i < Define.Projectile_Num; i++)
        {
            GameObject projectile = Instantiate(Resources.Load(string.Format("{0}{1}", PATH.ProjectilePath, projectileID.ToString())), transform) as GameObject;
            projectile.SetActive(false);

            if (!projectilePool.ContainsKey(projectileID))
                projectilePool.Add(projectileID, new List<BaseProjectile>());

            projectilePool[projectileID].Add(projectile.GetComponent<BaseProjectile>());
        }
    }

    public void GetProjectile(eProjectile projectileID, Vector3 createPos, Vector3 targetPos)
    {
        if (projectilePool.ContainsKey(projectileID))
        {
            for (int i = 0; i < projectilePool[projectileID].Count; i++)
            {
                if (projectilePool[projectileID][i].gameObject.activeSelf == false)
                {
                    projectilePool[projectileID][i].transform.position = createPos;
                    projectilePool[projectileID][i].SetData(projectileID, targetPos);
                    projectilePool[projectileID][i].gameObject.SetActive(true);
                    break;
                }
                if (i == projectilePool[projectileID].Count - 1)
                {
                    PoolingProjectile(projectileID);
                }
            }
        }
        else
        {
            PoolingProjectile(projectileID);
            GetProjectile(projectileID, createPos,targetPos);
        }
    }

    public void ReturnToPool(BaseProjectile projectile)
    {
        projectile.gameObject.SetActive(false);
        projectile.transform.position = projectile.transform.parent.position;
    }
}
