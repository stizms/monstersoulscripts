﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 캐릭터 선택씬이나 생성씬에서 사용하는 캐릭터 윈도우 매니저

public class CharacterWindowMng : MonoBehaviour
{
    #region Singleton

    private static CharacterWindowMng instance = null;

    public static CharacterWindowMng Ins
    {
        get
        {
            if (SceneMng.Ins.nowScene == eScene.CharacterChoice)
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<CharacterWindowMng>();
                    if (instance == null)
                    {
                        instance = new GameObject("CharacterWindowMng", typeof(CharacterWindowMng)).GetComponent<CharacterWindowMng>();
                        instance.transform.position = new Vector3(1000, 1000, 1000);
                    }
                }
                return instance;
            }
            else
            {
                Debug.Log("캐릭터 선택 씬에서만 사용할 수 있습니다.");
                return null;
            }
        }
    }

    #endregion

    #region INSPECTOR

    // 프리팹
    public Character    male;
    public Character    female;
    // 윈도우 지점
    public Transform    windowPos;
    // 이전 윈도우 캐릭터
    private Character   prevWindowCharacter;

    #endregion

    public void SetWindowCharacter(UserData userData)
    {
        if (prevWindowCharacter != null)
            prevWindowCharacter.transform.position = new Vector3(1000, 1000, 1000);
        if (userData == null)
            prevWindowCharacter = null;
        else
        {
            // 성별 디폴트 캐릭터 가져옴
            switch (userData.gender)
            {
                case eGender.Male:
                    male.transform.position = windowPos.position;
                    prevWindowCharacter = male;
                    break;
                case eGender.Female:
                    female.transform.position = windowPos.position;
                    prevWindowCharacter = female;
                    break;
            }
            // 장비 장착
            prevWindowCharacter.characterInfo = userData;
            prevWindowCharacter.EquipAllEquipment();
        }
    }
}
