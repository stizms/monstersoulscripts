﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableMng : MonoBehaviour
{
    #region Singleton
    private static TableMng instance = null;

    public static TableMng Ins
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<TableMng>();
                if (instance == null)
                {
                    instance = new GameObject("TableManager", typeof(TableMng)).GetComponent<TableMng>();
                    instance.transform.position = new Vector3(1000, 1000, 1000);
                }
            }
            return instance;
        }
    }


    private void Awake()
    {
        tableDownLoader = GetComponent<TableDownloader>();
        DontDestroyOnLoad(this);
    }
    #endregion

    private TableDownloader tableDownLoader;

    // 테이블 집합
    public ConfigTable              configTb            { get; } = new ConfigTable();

    public WeaponTable              weaponTb            { get; } = new WeaponTable();
    public EquipmentTable           equipmentTb         { get; } = new EquipmentTable();

    public QuestGoalTable           questGoalTb         { get; } = new QuestGoalTable();
    public QuestSectionTable        questSectionTb      { get; } = new QuestSectionTable();
    public QuestTable               questTb             { get; } = new QuestTable();

    public NPCTable                 npcTb               { get; } = new NPCTable();

    public EnvironmentTable         environmentTb       { get; } = new EnvironmentTable();

    public ItemTable                itemTb              { get; } = new ItemTable();
    public ConsumableItemInfoTable  consumeItemInfoTb   { get; } = new ConsumableItemInfoTable();

    public MonsterTable             monsterTb           { get; } = new MonsterTable();

    public ProjectileTable          projectileTb        { get; } = new ProjectileTable();

    public DungeonTable             dungeonTb           { get; } = new DungeonTable();
    
    // 다운로드 다 되었는지 체크
    public bool downloadComplete = false;

    public void StartDownload(eTableCategory _tableCategory)
    {
        tableDownLoader.Download(_tableCategory);
    }

    public ITable GetITable(eTable _eTable)
    {
        switch (_eTable)
        {
            case eTable.Config:
                return configTb;
            case eTable.Weapon:
                return weaponTb;
            case eTable.Equipment:
                return equipmentTb;
            case eTable.QusetGoal:
                return questGoalTb;
            case eTable.QuestSection:
                return questSectionTb;
            case eTable.Quest:
                return questTb;
            case eTable.NPC:
                return npcTb;
            case eTable.Environment:
                return environmentTb;
            case eTable.Item:
                return itemTb;
            case eTable.Consumable_Item_Info:
                return consumeItemInfoTb;
            case eTable.Monster:
                return monsterTb;
            case eTable.Projectile:
                return projectileTb;
            case eTable.Dungeon:
                return dungeonTb;
        }

        return null;
    }
}
