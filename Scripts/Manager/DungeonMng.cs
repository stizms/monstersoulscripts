﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonMng : MonoBehaviour
{
    #region Singleton

    private static DungeonMng instance = null;
    public static DungeonMng Ins
    {
        get
        {
            if(SceneMng.Ins.nowScene > eScene.Dungeon)
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<DungeonMng>();
                    if (instance == null)
                        instance = new GameObject("DungeonMng", typeof(DungeonMng)).GetComponent<DungeonMng>();
                }
            }
            else
            {
                Debug.Log("던전 매니저는 던전 씬에서만 사용이 가능한 매니저입니다.");
            }
            return instance;
        }
    }

    #endregion

    public DungeonScene     dungeonScene;
    public DungeonInventory dungeonItemList = new DungeonInventory();

    public void QuestClear()
    {
        UIMng.Ins.dungeon.StartRemainTimeCheck();
        MonsterMng.Ins.MissTarget();
    }

    public void Revive()
    {
        CharacterMovement character = dungeonScene.player.GetComponent<CharacterMovement>();
        character.Revive();
        character.transform.position        = dungeonScene.startPos.position;
        character.transform.rotation        = Quaternion.Euler(Vector3.zero);
        Camera.main.transform.position      = Camera.main.transform.parent.position;
        Camera.main.transform.localRotation = Quaternion.Euler(Vector3.zero);
        character.playerCam.ResetFirstPosition();
    }
}
