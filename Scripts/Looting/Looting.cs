﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Looting : MonoBehaviour
{
    #region INSPECTOR
    
    public ILooting lootingInfo = null;

    #endregion
    
    private Character tempCharacter;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Character"))
        {
            tempCharacter = other.GetComponent<Character>();
            tempCharacter.lootingTrigger = this;

            if(lootingInfo.CheckCanLooting())
                UIMng.Ins.lootingHud.VisualizeLootingHud(lootingInfo, tempCharacter.hudPos);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Character"))
        {
            if (tempCharacter.lootingTrigger == this)
            {
                tempCharacter.lootingTrigger = null;
                UIMng.Ins.lootingHud.NonVisualizeLootingHud();
            }
        }
    }

    private void OnDisable()
    {
       if(tempCharacter != null)
        {
            if (tempCharacter.lootingTrigger == this)
            {
                tempCharacter.lootingTrigger = null;
                UIMng.Ins.lootingHud.NonVisualizeLootingHud();
            }
        }
    }

    public eItemID LootItem(out int amount)
    {
        return lootingInfo.LootItem(out amount);
    }

    public bool CheckCanLooting()
    {
        return lootingInfo.CheckCanLooting();
    }
}
