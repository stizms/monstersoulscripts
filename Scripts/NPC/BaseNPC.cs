﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Cinemachine;

public class BaseNPC : MonoBehaviour, IInteract
{
    public  eNPCID    NPCID { get { return (eNPCID)Enum.Parse(typeof(eNPCID), name); } }
    public  eNPCType  NPCType;
    public  string    NPCName;
    private Character player = null;

    private void Awake()
    {
        NPCData npcData = TableMng.Ins.npcTb[NPCID];
        NPCType = npcData.NPCType;
        NPCName = npcData.NPCName;
    }

    public virtual void Interacting()
    {
        if (GameMng.Ins.isInteracting)  return;
        GameMng.Ins.Lock();
        if (QuestMng.Ins.nowQuest != null)
        {
            if (SceneMng.Ins.nowScene == eScene.Lobby)
                UIMng.Ins.lobby.lobbyQuestUI.NonVisualizeQuestUI();
            else if (SceneMng.Ins.nowScene > eScene.Dungeon)
                UIMng.Ins.QuestHudOff();
        }
        UIMng.Ins.mouseInteractiveUI.NonVisualizeInteractiveUI();
        UIMng.Ins.lobby.minimapUI.SetActive(false);
        GameMng.Ins.CursorVisualize();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Character"))
        {
            if(player == null)
                player = other.GetComponent<Character>();
            player.nearNPC = this;
            UIMng.Ins.mouseInteractiveUI.VisualizeInteractiveUI(NPCName,player.hudPos);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Character"))
        {
            UIMng.Ins.mouseInteractiveUI.NonVisualizeInteractiveUI();
            player.nearNPC = null;
            player = null;
        }
    }

}
