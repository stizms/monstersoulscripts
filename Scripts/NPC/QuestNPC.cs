﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestNPC : BaseNPC
{
    public override void Interacting()
    {
        base.Interacting();
        UIMng.Ins.lobby.questUI.VisualizeQuestUI();
    }
}
