﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBoxNPC : BaseNPC
{
    public override void Interacting()
    {
        base.Interacting();
        UIMng.Ins.lobby.itemBoxUI.VisualizeItemBoxUI();
    }
}
