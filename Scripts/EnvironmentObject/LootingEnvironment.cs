﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootingEnvironment : MonoBehaviour,ILooting
{

    public eEnvironmentID   environmentID;
    public string           environmentName;
    public int              lootingCount;
    public int              availableLootingCount;
    public Looting          lootingTrigger;
    public eItemID          lootingItem { get; set; }

    // 파싱할 때 필요
    private string parsingEnvironmentName;

    private void Awake()
    {
        parsingEnvironmentName          = name.Substring(0, name.IndexOf("(Clone)"));
        EnvironmentData environmentData = TableMng.Ins.environmentTb[(eEnvironmentID)Enum.Parse(typeof(eEnvironmentID), parsingEnvironmentName)];
        environmentID                   = environmentData.environmentID;
        environmentName                 = environmentData.environmentName;
        lootingCount                    = environmentData.lootingCount;
        availableLootingCount           = lootingCount;
        lootingItem                     = environmentData.lootingItem;
        lootingTrigger                  = GetComponent<Looting>();
    }

    private void Start()
    {
        lootingTrigger.lootingInfo = this;
    }

    public void Init()
    {
        availableLootingCount = lootingCount;
    }

    IEnumerator DelayAndGoBackToPool()
    {
        yield return new WaitForSeconds(3f);
        ReturnToPool();
    }

    private void ReturnToPool()
    {
        gameObject.SetActive(false);
        transform.position = transform.parent.position;
        Init();
    }

    #region LootingFunc

    public eLootingCategory lootingCategory { get { return eLootingCategory.Environment; } }

    public bool CheckCanLooting()
    {
        if (availableLootingCount > 0) return true;
        else
            return false;
    }

    public virtual string GetSpriteName() { return null; }

    public virtual string GetObjectName() { return null; }



    // 광물 같은 경우는 무조건 1개만 뜬다고 함
    public virtual eItemID LootItem(out int amount)
    {
        amount = 0;
        if (availableLootingCount <= 0) return eItemID.None;
        
        // 무작위 개수
        amount = UnityEngine.Random.Range(1, 4);
        int difference = TableMng.Ins.itemTb[lootingItem].characterLimit - UserDataMng.Ins.nowCharacter.characterInventory.GetItemCount(lootingItem);
        if (difference < amount)    amount = difference;

        availableLootingCount--;
        if (availableLootingCount == 0)
        {
            UIMng.Ins.lootingHud.NonVisualizeLootingHud();
            StartCoroutine(DelayAndGoBackToPool());
        }
        return lootingItem;
    }

    #endregion
}
