﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentSpawner : MonoBehaviour
{
    public eEnvironmentID createEnvironmentType;

    public void SetEnvironmentPos()
    {
        LootingEnvironment lootingEnvironment = EnvironmentMng.Ins.GetEnvironment(createEnvironmentType);
        lootingEnvironment.transform.position = transform.position;
        lootingEnvironment.transform.rotation = Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0));
        lootingEnvironment.gameObject.SetActive(true);
    }

}
