﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HerbPlant : LootingEnvironment
{
    public override string GetSpriteName() { return lootingItem.ToString(); }

    public override string GetObjectName() { return TableMng.Ins.itemTb[lootingItem].itemName; }
    
}
