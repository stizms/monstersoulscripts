﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory
{
    // 아이템 리스트
    public Dictionary<eItemID, int> itemList = new Dictionary<eItemID, int>();

    // 초기 아이템 목록 세팅
    public Inventory()
    {
        GetItem(eItemID.Herb, 4);
        GetItem(eItemID.Potion, 10);
    }

    // 아이템 리스트 클리어 ( Read시 필요 )
    public void ItemListClear()
    {
        itemList.Clear();
    }

    public int GetCanGetItemAmount(eItemID itemID, int amount)
    {
        ItemData tempItem = TableMng.Ins.itemTb[itemID];
        if (!itemList.ContainsKey(itemID))
        {
            if (amount > tempItem.characterLimit)
                return tempItem.characterLimit;
            else
                return amount;
        }
        else
        {
            if (GetItemCount(itemID) + amount > tempItem.characterLimit)
                return tempItem.characterLimit - GetItemCount(itemID);
            else
                return amount;
        }
    }

    // 아이템을 얻는 경우 ( 캐릭터 인벤토리로 들어가는 경우 ) // 아이템 개수 Up 
    public virtual int GetItem(eItemID itemID, int amount)
    {
        amount = GetCanGetItemAmount(itemID, amount);
        if (!itemList.ContainsKey(itemID))
        {
            itemList.Add(itemID,amount);
            if(TableMng.Ins.itemTb[itemID].itemType == eItemType.Consume && SceneMng.Ins.nowScene > eScene.Dungeon)
                UIMng.Ins.dungeon.characterInfoUI.consumeItemUI.consumeList.AddLast(itemID);
        }
        else
        {
            itemList[itemID] += amount;
            if (TableMng.Ins.itemTb[itemID].itemType == eItemType.Consume && SceneMng.Ins.nowScene > eScene.Dungeon)
            {
                if(UIMng.Ins.dungeon.characterInfoUI.consumeItemUI.character.selectedItem == itemID)
                    UIMng.Ins.dungeon.characterInfoUI.consumeItemUI.UpdateItemAmountUI();
            }
        }
        return amount;
    }

    // 아이템 개수 얻어오기
    public int GetItemCount(eItemID itemID)
    {
        if (!itemList.ContainsKey(itemID))  return 0;
        else
            return itemList[itemID];
    }

    // 아이템 개수 Down
    public bool DeductItemAmount(eItemID itemID, int amount)
    {
        if (!itemList.ContainsKey(itemID))  return false;

        itemList[itemID] -= amount;
        if (itemList[itemID] <= 0)
        {
            itemList.Remove(itemID);
            return false;
        }
        return true;
    }
}
