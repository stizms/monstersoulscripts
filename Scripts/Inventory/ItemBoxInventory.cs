﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBoxInventory : Inventory
{
    public Dictionary<eEquipment, List<WeaponData>>     weaponItemList  = new Dictionary<eEquipment, List<WeaponData>>();       // 무기
    public Dictionary<eEquipment, List<EquipmentData>>  helmetItemList  = new Dictionary<eEquipment, List<EquipmentData>>();    // 헬멧
    public Dictionary<eEquipment, List<EquipmentData>>  vestItemList    = new Dictionary<eEquipment, List<EquipmentData>>();    // 베스트
    public Dictionary<eEquipment, List<EquipmentData>>  gloveItemList   = new Dictionary<eEquipment, List<EquipmentData>>();    // 장갑
    public Dictionary<eEquipment, List<EquipmentData>>  beltItemList    = new Dictionary<eEquipment, List<EquipmentData>>();    // 벨트
    public Dictionary<eEquipment, List<EquipmentData>>  pantsItemList   = new Dictionary<eEquipment, List<EquipmentData>>();    // 팬츠

    // 초기 아이템 박스 아이템 목록 세팅
    public ItemBoxInventory()
    {
        itemList.Clear();
        
        GetItem(eItemID.Potion, 90);
        GetItem(eItemID.Nutrients, 90);

        GetEquipment(eEquipment.DefaultGreatSword);
        GetEquipment(eEquipment.DefaultChargeAxe);
        GetEquipment(eEquipment.DefaultLance);

        GetEquipment(eEquipment.DefaultHelmet);
        GetEquipment(eEquipment.ChainHelmet);

        GetEquipment(eEquipment.DefaultVest);
        GetEquipment(eEquipment.ChainVest);

        GetEquipment(eEquipment.DefaultGlove);
        GetEquipment(eEquipment.ChainGlove);

        GetEquipment(eEquipment.DefaultBelt);
        GetEquipment(eEquipment.ChainBelt);

        GetEquipment(eEquipment.DefaultPants);
        GetEquipment(eEquipment.ChainPants);
    }

    // 아이템을 얻는 경우
    public override int GetItem(eItemID itemID, int amount)
    {
        if (itemID == eItemID.None) return 0;

        if (!itemList.ContainsKey(itemID))  itemList.Add(itemID, amount);
        else
            itemList[itemID] += amount;

        return 0;
    }

    // 장비를 얻는 경우
    public void GetEquipment(eEquipment equipmentID)
    {
        if (equipmentID == eEquipment.None) return;
        
        if( equipmentID > eEquipment.Weapon && equipmentID < eEquipment.Helmet)
        {
            WeaponData weaponData = TableMng.Ins.weaponTb[equipmentID];
            if(weaponItemList.ContainsKey(equipmentID))
                weaponItemList[equipmentID].Add(weaponData);
            else
            {
                List<WeaponData> tempWeaponList = new List<WeaponData>();
                tempWeaponList.Add(weaponData);
                weaponItemList.Add(equipmentID, tempWeaponList);
            }
        }
        else
        {
            Dictionary<eEquipment, List<EquipmentData>> tempMap = null;
            EquipmentData equipmentData = TableMng.Ins.equipmentTb[equipmentID];
            switch(equipmentData.equipmentCategory)
            {
                case eEquipmentCategory.Helmet:
                    tempMap = helmetItemList;
                    break;
                case eEquipmentCategory.Vest:
                    tempMap = vestItemList;
                    break;
                case eEquipmentCategory.Glove:
                    tempMap = gloveItemList;
                    break;
                case eEquipmentCategory.Belt:
                    tempMap = beltItemList;
                    break;
                case eEquipmentCategory.Pants:
                    tempMap = pantsItemList;
                    break;
            }

            if (tempMap.ContainsKey(equipmentID))
                tempMap[equipmentID].Add(equipmentData);
            else
            {
                List<EquipmentData> tempEquipmentList = new List<EquipmentData>();
                tempEquipmentList.Add(equipmentData);
                tempMap.Add(equipmentID, tempEquipmentList);
            }
        }
    }

    public int GetEquipmentCount(eEquipment equipmentID)
    {
        if (equipmentID == eEquipment.None) return 0;
        
        if(equipmentID > eEquipment.Weapon && equipmentID < eEquipment.Helmet)
        {
            return weaponItemList[equipmentID].Count;
        }
        else if (equipmentID > eEquipment.Helmet && equipmentID < eEquipment.Vest)
        {
            return helmetItemList[equipmentID].Count;
        }
        else if (equipmentID > eEquipment.Vest && equipmentID < eEquipment.Glove)
        {
            return vestItemList[equipmentID].Count;
        }
        else if (equipmentID > eEquipment.Glove && equipmentID < eEquipment.Belt)
        {
            return gloveItemList[equipmentID].Count;
        }
        else if (equipmentID > eEquipment.Belt && equipmentID < eEquipment.Pants)
        {
            return beltItemList[equipmentID].Count;
        }
        else if (equipmentID > eEquipment.Pants)
        {
            return pantsItemList[equipmentID].Count;
        }

        return 0;
    }

    public void SellEquipment(eEquipment equipmentID)
    {
        UserData tempUserData = UserDataMng.Ins.nowCharacter;
        if (equipmentID > eEquipment.Weapon && equipmentID < eEquipment.Helmet)
        {
            tempUserData.money += TableMng.Ins.weaponTb[equipmentID].sellPrice;
            if (weaponItemList[equipmentID].Count > 1)
                weaponItemList[equipmentID].RemoveAt(weaponItemList[equipmentID].Count-1);
            else
                weaponItemList.Remove(equipmentID);
        }
        else
        {
            Dictionary<eEquipment, List<EquipmentData>> tempMap = null;

            EquipmentData eqiupmentData = TableMng.Ins.equipmentTb[equipmentID];
            tempUserData.money += eqiupmentData.sellPrice;
            switch(eqiupmentData.equipmentCategory)
            {
                case eEquipmentCategory.Helmet:
                    tempMap = helmetItemList;
                    break;
                case eEquipmentCategory.Vest:
                    tempMap = vestItemList;
                    break;
                case eEquipmentCategory.Glove:
                    tempMap = gloveItemList;
                    break;
                case eEquipmentCategory.Belt:
                    tempMap = beltItemList;
                    break;
                case eEquipmentCategory.Pants:
                    tempMap = pantsItemList;
                    break;
            }
            if (tempMap[equipmentID].Count > 1)
                tempMap[equipmentID].RemoveAt(tempMap[equipmentID].Count - 1);
            else
                tempMap.Remove(equipmentID);
        }
    }

    // ReadData 할 때 모든 데이터를 일단 지워주기 위함 ( 새로 생성하는 캐릭터에는 호출 안됨 )
    public void InventoryClear()
    {
        ItemListClear();
        weaponItemList.Clear();
        helmetItemList.Clear();
        vestItemList.Clear();
        gloveItemList.Clear();
        beltItemList.Clear();
        pantsItemList.Clear();

    }
}