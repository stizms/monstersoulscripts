﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonInventory : Inventory
{
    private UserData userData;

    public DungeonInventory()
    {
        itemList.Clear();
    }

    // 던전 인벤토리에 들어가는 경우
    public override int GetItem(eItemID itemID, int amount)
    {
        if (itemList.Count >= Define.Dungeon_Inventory_Num) return 0;

        if(itemList.ContainsKey(itemID))
            itemList[itemID] += amount;
        else
            itemList.Add(itemID, amount);

        return 0;
    }

    // 아이템 박스 인벤토리의 아이템과 합치는 함수 ( 퀘스트 완료 후 실행 )
    public void UnionToItemBox(eItemID itemID,int amount)
    {
        if(userData == null) userData = UserDataMng.Ins.nowCharacter;

        userData.itemBoxInventory.GetItem(itemID, amount);
        DungeonMng.Ins.dungeonItemList.itemList.Remove(itemID);
    }

    // 전부 다 박스 인벤토리와 합침
    public void UnionToAllItemBox()
    {
        if (userData == null) userData = UserDataMng.Ins.nowCharacter;

        foreach (var node in itemList)
        {
            userData.itemBoxInventory.GetItem(node.Key, node.Value);
            DungeonMng.Ins.dungeonItemList.itemList.Remove(node.Key);
            if (itemList.Count <= 0)
                break;
        }
    }

    // 모두 매각
    public void SellAllItemBox()
    {
        if (userData == null) userData = UserDataMng.Ins.nowCharacter;

        int cost = 0;
        foreach(var node in itemList)
        {
            cost += (TableMng.Ins.itemTb[node.Key].sellPrice * node.Value);
            DungeonMng.Ins.dungeonItemList.itemList.Remove(node.Key);
            if (itemList.Count <= 0)
                break;
        }
        userData.money += cost;
    }
}
