﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserData
{
    // 기본적인 캐릭터 정보
    public string   characterName;
    public eGender  gender;     
    public int      money;
    public int      hunterRank; // 레벨 같은 개념 ( 퀘스트를 많이 깨면 올라감 )

    // 착용 장비
    public eEquipment   equipWeapon;
    public eEquipment   equipHelmet;
    public eEquipment   equipVest;
    public eEquipment   equipGlove;
    public eEquipment   equipBelt;
    public eEquipment   equipPants;

    // 플레이어의 인벤토리
    public ItemBoxInventory itemBoxInventory;
    public Inventory        characterInventory;

    // 플레이어가 깬 퀘스트 목록
    public Dictionary<eQuestID, eQuestID> clearQuestList = new Dictionary<eQuestID, eQuestID>();

    public UserData()
    {
        Init();   
    }

    // 초기화 함수
    public void Init()
    {
        characterName = "";
        gender = eGender.Male;
        money = 2000;
        hunterRank = 1;

        // 디폴트 무기
        equipWeapon = eEquipment.DefaultGreatSword;
        equipHelmet = eEquipment.DefaultHelmet;
        equipVest   = eEquipment.DefaultVest;
        equipGlove  = eEquipment.DefaultGlove;
        equipBelt   = eEquipment.DefaultBelt;
        equipPants  = eEquipment.DefaultPants;

        // 인벤토리 생성
        itemBoxInventory = new ItemBoxInventory();
        characterInventory = new Inventory();

        clearQuestList.Clear();
    }

    public void AddClearQuestList(eQuestID questID)
    {
        if (!clearQuestList.ContainsKey(questID))
            clearQuestList.Add(questID, questID);
    }
}
