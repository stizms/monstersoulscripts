﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene : MonoBehaviour, IScene
{
    #region INSPECTOR

    public AudioListener mainListener;
    
    #endregion

    protected void Awake()
    {
        StartCoroutine(CheckSceneLoadingComplete());
    }

    // 씬 로딩이 완료 되었는지 확인하는 코루틴
    public virtual IEnumerator CheckSceneLoadingComplete()
    {
        while (true)
        {
            if (SceneMng.Ins.loadingComplete)
            {
                SceneMng.Ins.loadingComplete = false;
                break;
            }
            yield return null;
        }
        InitScene();
}

    // 씬 시작시 해야할 작동 함수
    public virtual void InitScene()
    {
        // 오디오 리스너를 킴
        if(mainListener!= null)
            mainListener.enabled = true;
    }
}
