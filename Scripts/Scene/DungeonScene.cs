﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DungeonScene : Scene
{

    #region INSPECTOR

    public GameObject               blackBoard;
    public Transform                startPos;
    public MinimapFollowCam         minimapCam;
    public GameObject               bossTrigger;
    public List<MonsterSpawner>     monsterSpawnList        = new List<MonsterSpawner>();
    public List<EnvironmentSpawner> environmentSpawnList    = new List<EnvironmentSpawner>();

    #endregion

    // 플레이어 캐릭터
    public Character player;
    
    public string               dungeonName { get; private set; }
    public List<eMonsterID>     monsterTypeList     { get; private set; } = new List<eMonsterID>();     // 생성되는 몬스터 타입 리스트 ( 풀링 할 종류 )
    public List<eEnvironmentID> environmentTypeList { get; private set; } = new List<eEnvironmentID>(); // 생성되는 환경객체 타입 리스트

    public override void InitScene()
    {
        base.InitScene();
        Quest quest = QuestMng.Ins.nowQuest.quest;

        GameMng.Ins.CursorNonVisualize();
        DungeonData dungeonData = TableMng.Ins.dungeonTb[quest.relatedDungeon];
        dungeonName         = dungeonData.dungeonName;
        monsterTypeList     = dungeonData.monsterTypeList;
        environmentTypeList = dungeonData.environmentTypeList;

        RequestMonsterPooling();
        RequestEnvironmentPooling();
        RequestProjectilePooling();
        RequestEffectPooling();
        RequestDamageTextPooling();
        CreatePlayer();
        SetMonsterPos();
        SetEnvironmentPos();

        if (quest.isBossQuest)    bossTrigger.SetActive(true);
        SoundMng.Ins.ExternalSoundPlay(eSound.DungeonBGM.ToDesc(), eChannel.BackGround);
    }

    // 몬스터 풀링 요청
    private void RequestMonsterPooling()
    {
        foreach (var node in monsterTypeList)
        {
            MonsterMng.Ins.PoolingMonster(node);
        }
    }

    // 데미지 텍스트 풀링 요청
    private void RequestDamageTextPooling()
    {
        DamageTextMng.Ins.PoolingText();
    }

    // 환경 풀링 요청
    private void RequestEnvironmentPooling()
    {
        foreach (var node in environmentTypeList)
        {
            EnvironmentMng.Ins.PoolingEnvironment(node);
        }
    }

    // 이펙트 풀링 요청
    private void RequestEffectPooling()
    {
        EffectMng.Ins.PoolingEffect(eEffect.AttackEffect);
        EffectMng.Ins.PoolingEffect(eEffect.YellowConsumeEffect);
        EffectMng.Ins.PoolingEffect(eEffect.BlueConsumeEffect);
        EffectMng.Ins.PoolingEffect(eEffect.GreenConsumeEffect);
    }

    // 투사체 풀링 요청
    private void RequestProjectilePooling()
    {
        foreach(var node in monsterTypeList)
        {
            ProjectileMng.Ins.PoolingProjectile(TableMng.Ins.monsterTb[node].projectileID);
        }
    }

    // 몬스터 위치에 배치
    private void SetMonsterPos()
    {
        foreach(var node in monsterSpawnList)
        {
            node.SetMonsterPos();
        }
    }

    private void SetEnvironmentPos()
    {
        foreach(var node in environmentSpawnList)
        {
            node.SetEnvironmentPos();
        }
    }

    // 플레이어 생성 및 세팅
    private void CreatePlayer()
    {
        // 플레이어 생성 및 세팅
        GameObject tempPlayer = Instantiate(Resources.Load(string.Format("{0}{1}", PATH.CharacterPath, UserDataMng.Ins.characterSlot[UserDataMng.Ins.selectedIndex].gender)), startPos) as GameObject;
        player = tempPlayer.GetComponent<Character>();
        player.transform.localPosition = Vector3.zero;
        player.transform.parent = null;

        minimapCam.followTarget = player;
    }
}
