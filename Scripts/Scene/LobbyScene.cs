﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class LobbyScene : Scene
{
    #region INSPECTOR

    // 캐릭터 시작 위치
    public Transform        startPos;
    public MinimapFollowCam minimapCam;
    public GameObject       blackBoard;

    #endregion
    
    public Character player;

    public override void InitScene()
    {
        base.InitScene();
        UIMng.Ins.lobby.lobbyScene = this;
        GameMng.Ins.CursorNonVisualize();
        CreatePlayer();
        SoundMng.Ins.ExternalSoundPlay(eSound.LobbyBGM.ToDesc(),eChannel.BackGround);
    }

    // 플레이어 생성 및 세팅
    private void CreatePlayer()
    {
        GameObject tempPlayer = Instantiate(Resources.Load(string.Format("{0}{1}", PATH.CharacterPath, UserDataMng.Ins.characterSlot[UserDataMng.Ins.selectedIndex].gender)), startPos) as GameObject;
        player = tempPlayer.GetComponent<Character>();
        player.transform.localPosition  = Vector3.zero;
        player.transform.parent         = null;
        minimapCam.followTarget         = player;
    }
}
