﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// 로딩씬에서 UI 조작
public class IntroScene : Scene
{
    #region INSPECTOR

    public Image            blackBoard; // 처음 밝아지는 효과 주는 검은판 
    public GameObject       titleParticle;
    public Image            titleIcon;
    public TextMeshProUGUI  titleLabel;
    public TextMeshProUGUI  pressAnyKeyLabel; 
    public GameObject       indicator; // 테이블 로딩 기다리는 circle

    #endregion

    // 처음 시작할 때는 로딩 과정을 거치지 않는 것을 제어
    private bool        firstPlay       = true;
    // 터치 가능하게 하는 플래그
    private bool        canTouch        = false;
    // Press Any Key To Start 애니메이션 코루틴
    private Coroutine   pressAnyAnim    = null;
    
    public override IEnumerator CheckSceneLoadingComplete()
    {
        if(GameMng.Ins.firstPlay)
        {
            GameMng.Ins.firstPlay = false;
            // 해상도 고정
            Screen.SetResolution(2960, 1440, true);
            yield return null;
        }
        else
        {
            while (true)
            {
                if (SceneMng.Ins.loadingComplete)
                {
                    SceneMng.Ins.loadingComplete = false;
                    break;
                }
                yield return null;
            }
        }
        InitScene();
    }

    public override void InitScene()
    {
        base.InitScene();
        GameMng.Ins.CursorNonVisualize();
        SoundMng.Ins.ExternalSoundPlay(eSound.IntroBGM.ToDesc(), eChannel.BackGround);
        StartCoroutine(FadeOutBlackBord());
    }

    // 블랙보드 밝아지는 함수
    public IEnumerator FadeOutBlackBord()
    {
        while (true)
        {
            blackBoard.color = Utill.ChangeMinusAlpha(blackBoard.color, 0.05f);

            if (blackBoard.color.a <= 0f)
            {
                blackBoard.gameObject.SetActive(false);
                StartCoroutine(FadeInTitleIcon());
                break;
            }
            yield return new WaitForSeconds(0.03f);
        }
    }

    // 타이틀 아이콘 나오게하기
    IEnumerator FadeInTitleIcon()
    {
        titleIcon.gameObject.SetActive(true);
        
        // 호출 한번만 되게하는 flag
        bool flag = false;
        while(true)
        {
            titleIcon.fillAmount += 0.05f;
            // 일정 이상 나타나면 타이틀 로고 나오게 하기
            if (titleIcon.fillAmount >= 0.5f && flag == false)
            {
                flag = true;
                StartCoroutine(FadeInTitleLabel());
            }
            else if (titleIcon.fillAmount >= 1f)    break;
            yield return new WaitForSeconds(0.04f);
        }
    }

    // 타이틀 나오게하기
    IEnumerator FadeInTitleLabel()
    {
        bool isFlag = false; // Fade In & Out 조절 플래그

        while (true)
        {
            titleLabel.color = Utill.ChangePlusAlpha(titleLabel.color, 0.05f);

            // 파티클 재생 & Press Any Key 시작
            if (titleLabel.color.a >= 1f && titleIcon.fillAmount >= 1f)
            {
                titleParticle.SetActive(true);
                pressAnyAnim = StartCoroutine(FadeInPressBtn());
                GameMng.Ins.CursorVisualize();
                break;
            }
            yield return new WaitForSeconds(0.04f);
        }

        // 타이틀 반짝반짝~
        while (true)
        {
            if (isFlag == false)
            {
                titleLabel.color = Utill.ChangeMinusAlpha(titleLabel.color, 0.05f);
                if (titleLabel.color.a <= 0.8f)
                    isFlag = true;
            }
            else
            {
                titleLabel.color = Utill.ChangePlusAlpha(titleLabel.color, 0.05f);
                if (titleLabel.color.a >= 1f)
                    isFlag = false;
            }
            yield return new WaitForSeconds(0.15f);
        }
    }

    //Press Any Key To Start Fade Animation
    IEnumerator FadeInPressBtn()
    {
        pressAnyKeyLabel.gameObject.SetActive(true);
        bool isFlag = false; // Fade In & Out 조절 플래그

        // 첫 Fade In
        while (true)
        {
            pressAnyKeyLabel.color = Utill.ChangePlusAlpha(pressAnyKeyLabel.color, 0.05f);

            if (pressAnyKeyLabel.color.a >= 0.4f)
            {
                canTouch = true;
                break;
            }
            yield return new WaitForSeconds(0.04f);
        }

        // 이후 애니메이션
        while (true)
        {
            if (isFlag == false)
            {
                pressAnyKeyLabel.color = Utill.ChangeMinusAlpha(pressAnyKeyLabel.color, 0.05f);
                if (pressAnyKeyLabel.color.a <= 0.4f)
                    isFlag = true;
            }
            else
            {
                pressAnyKeyLabel.color = Utill.ChangePlusAlpha(pressAnyKeyLabel.color, 0.05f);
                if (pressAnyKeyLabel.color.a >= 1f)
                    isFlag = false;
            }
            yield return new WaitForSeconds(0.04f);
        }
    }


    private void Update()
    {
        if(canTouch)
        {
            // 어떤 키라도 눌린다면 테이블 받기 시작
            if(Input.anyKeyDown)
            {
                SoundMng.Ins.ExternalSoundPlay(eSound.IntroClickSound.ToDesc(), eChannel.UI_Effect);
                canTouch = false;
                StartCoroutine(MoveCharacterChoicScene());
            }  
        }
    }

    // 캐릭터 초이스 씬으로 이동
    IEnumerator MoveCharacterChoicScene()
    {
        int index = 0;
        StopCoroutine(pressAnyAnim);

        pressAnyKeyLabel.color = Utill.ChangeAlpha(pressAnyKeyLabel.color, 1f);

        // 라벨이 커지는 효과
        while (true)
        {
            pressAnyKeyLabel.transform.localScale += new Vector3(0.1f, 0.03f, 0f);
            index++;
            yield return new WaitForSeconds(0.04f);

            if(index > 2)
            {
                pressAnyKeyLabel.gameObject.SetActive(false);
                indicator.SetActive(true);
                StartCoroutine(StartTableDownload());
                break;
            }
        }

    }

    IEnumerator StartTableDownload()
    {
        // 초기 첫 로딩 시 테이블 받아오기
        if (TableMng.Ins.downloadComplete == false)
        {
            TableMng.Ins.StartDownload(eTableCategory.GameData);
            while (true)
            {
                if (TableMng.Ins.downloadComplete)  break;
                yield return null;
            }
        }
        // 저장데이터 받아오기
        UserDataMng.Ins.ReadUserData();
        SceneMng.Ins.ChangeScene(eScene.CharacterChoice);
    }
}
