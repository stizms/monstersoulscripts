﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScene : Scene
{

    #region INSPECTOR
    
    public Image loadingProgressGaze;

    #endregion

    public override void InitScene()
    {
        GameMng.Ins.CursorNonVisualize();
        SoundMng.Ins.StopBackGroundSound();
        StartCoroutine(MoveNextSceneLoading());
    }

    // 다음 씬 로딩
    IEnumerator MoveNextSceneLoading()
    {
        if(SceneMng.Ins.nextScene > eScene.Dungeon)
            SoundMng.Ins.ExternalSoundPlay(eSound.DungeonLoadingSound.ToDesc(), eChannel.UI_Effect);
        
        // 로딩 화면 조금은 보여주기 위함
        yield return new WaitForSeconds(2f);

        // 로딩할 씬 목록 초기화
        SceneMng.Ins.loadSceneList.Clear();
        eScene nextScene = SceneMng.Ins.nextScene;
        SceneMng.Ins.loadSceneList.Add(nextScene.ToDesc());

        // 다음이 던전 씬이라면 던전 UI 등록
        if ((int)nextScene > (int)eScene.Dungeon)
            SceneMng.Ins.loadSceneList.Add(eScene.DungeonUI.ToDesc());
        // 로비라면 로비 UI 등록
        else if (nextScene == eScene.Lobby)
            SceneMng.Ins.loadSceneList.Add(eScene.LobbyUI.ToDesc());

        SceneMng.Ins.nowScene = nextScene;
        StartCoroutine(SceneMng.Ins.LoadSceneAsync(SceneMng.Ins.loadSceneList, loadingProgressGaze));
    }
}
