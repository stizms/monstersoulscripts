﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using System;

public static class Extension
{

    public static List<string> GetFieldDesc(this Type t)
    {
        List<string> li = new List<string>();
        var arr = t.GetProperties();

        foreach (var val in arr)
        {
            var ar = (DescriptionAttribute[])val.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (ar != null && ar.Length > 0)
            {
                li.Add(ar[0].Description);
            }
        }

        return li;
    }

    // enum
    public static string ToDesc(this System.Enum a_eEnumVal)
    {
        var da = (DescriptionAttribute[])(a_eEnumVal.GetType().GetField(a_eEnumVal.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
        return da.Length > 0 ? da[0].Description : a_eEnumVal.ToString();
    }
}


#region interface

public interface IScene
{
    void InitScene();
}

public interface IInteract
{
    void Interacting();
}

// NPC와 인터렉팅으로 생기는 UI
public interface IInteractingUI
{
    void ExitUI();
}

public interface ILooting
{
    eItemID             lootingItem { get; set; }
    eLootingCategory    lootingCategory { get; }

    bool    CheckCanLooting();
    eItemID LootItem(out int amount);
    string  GetSpriteName();
    string  GetObjectName();

}

public interface IOptionChild
{
    void NonVisualizeOptionChild();
}

#endregion

public enum eTable
{
    None = -1,

    [Description("1557703280")]
    Config,

    [Description("188889908")]
    Weapon,
    [Description("1284650805")]
    Equipment,

    [Description("498023924")]
    QusetGoal,
    [Description("23699971")]
    QuestSection,
    [Description("216558623")]
    Quest,

    [Description("281756726")]
    NPC,

    [Description("1638501231")]
    Monster,

    [Description("1816239544")]
    Projectile,

    [Description("975184442")]
    Environment,

    [Description("1215664287")]
    Item,

    [Description("128452354")]
    Consumable_Item_Info,


    [Description("692913466")]
    Dungeon,

    // Table Count
    Max,
}

public enum eTableCategory
{
    None = -1,

    GameData,
}

public enum eScene
{
    None = -1,

    [Description("IntroScene")]
    Intro,

    [Description("CharacterChoiceScene")]
    CharacterChoice,

    [Description("LobbyScene")]
    Lobby,

    [Description("Lobby_UI_Scene")]
    LobbyUI,

    [Description("LoadingScene")]
    Loading,

    [Description("Dungeon_UI_Scene")]
    DungeonUI,

    // 1000부터 던전 스테이지
    Dungeon = 1000,

    [Description("DungeonScene_1")]
    Dungeon_1,
}

// 캐릭터 성별
public enum eGender
{
    None = -1,
    Male,
    Female,
}

// 저장데이터 카테고리
public enum eUserDataCategory
{
    None = -1,
    CharacterInfo,
    EquipItem,

    WeaponInventory,
    HelmetInventory,
    VestInventory,
    GloveInventory,
    BeltInventory,
    PantsInventory,
    ItemBoxItemInventory,
    CharacterItemInventory,

    ClearQuestList,
}

public enum eWeaponCategory
{
    None = -1,
    GreatSword,
    ChargeAxe,
    Lance,
}

public enum eRare
{
    None = -1,
    Rare1,
}

// 속성 나중에 추가 가능하면 추가
public enum eElement
{
    [Description("없음")]
    None = -1,
    [Description("불")]
    Fire,
    [Description("물")]
    Water,
    [Description("번개")]
    Thunder,
    [Description("얼음")]
    Ice
}

public enum eEquipmentCategory
{
    None = -1,
    Weapon,
    Helmet,
    Vest,
    Glove,
    Belt,
    Pants
}
public enum eEquipment
{
    None = -1,

    Weapon = 10000,
    [Description("DefaultGreatSword")]
    DefaultGreatSword,
    [Description("DefaultChargeAxe")]
    DefaultChargeAxe,
    [Description("DefaultLance")]
    DefaultLance,

    Helmet = 20000,
    [Description("DefaultHelmet")]
    DefaultHelmet,
    [Description("ChainHelmet")]
    ChainHelmet,


    Vest = 30000,
    [Description("DefaultVest")]
    DefaultVest,
    [Description("ChainVest")]
    ChainVest,

    Glove = 40000,
    [Description("DefaultGlove")]
    DefaultGlove,
    [Description("ChainGlove")]
    ChainGlove,


    Belt = 50000,
    [Description("DefaultBelt")]
    DefaultBelt,
    [Description("ChainBelt")]
    ChainBelt,

    Pants = 60000,
    [Description("DefaultPants")]
    DefaultPants,
    [Description("ChainPants")]
    ChainPants,
}
public enum ePlayerState
{
    //기본
    IDLE,
    // 걷기
    WALK,
    // 구르기
    ROLLING,
    // 공격
    Attack,
    //피격 당함
    Damaged,
    //루팅
    Looting,
    //소모성 아이템 소모
    Consume,
    //죽음
    Death,
}

public enum eHPState
{
    //기본
    IDLE,
    Recovery,
    Damaged,
}

public enum eAttackCategory
{
    None = -1,
    Attack1,
    Attack2,
    Attack3,
    Attack4,
}

public enum eUsingWeaponState
{
    // 납도
    SHEATING,
    // 발도
    DRAW
}


// 퀘스트 목표의 타입
public enum eGoalType
{
    // 사냥 퀘스트
    HuntGoal,
    // 수집 퀘스트
    CollectionGoal,
}

// 퀘스트 전체의 타입
public enum eQuestType
{
    [Description("사냥 퀘스트")]
    HuntQuest,
    [Description("수집 퀘스트")]
    CollectionQuest,
}

// 퀘스트 ID
public enum eQuestID
{
    None = -1,
    Q0,
    Q1,
    Q2,
}

// 퀘스트 메인 타겟
public enum eQuestTarget
{
    None = -1,

    Monster = 1000,
    Lizard_Knight,
    Rathian,


    Collection = 10000,
    Herb,
}

// 적 종류
public enum eMonsterID
{
    None = -1,

    // 잡몹류
    
    Lizard_Knight,

    // 보스류
    Rathian,
}

public enum eMonsterState
{
    None = -1,
    Idle,
    Roaming,
    Chasing,
    Attack,
    Death,

    Fly,
    Escape,
    Contacting,
}

public enum eMonsterAttack
{
    None = -1,
    NormalAttack1,
    NormalAttack2,
    SpecialAttack1,
    SpecialAttack2,
}

public enum eItemID
{
    None = -1,

    // Consume
    Herb,
    Potion,
    Ration,
    Nutrients,
    Whetstone,

    // Material
    Lizard_Leather,
    Rathian_Scales
}

public enum eItemType
{
    None = -1,
    Consume,
    Material,
}

public enum eNPCID
{
    NPC_0,
    NPC_1,
}

public enum eNPCType
{
    Quest,
    ItemBox,
}

public enum eAtlasCategory
{
    CharacterChoice,
    Description,
    Quest,
    Monster,
    Item,
    Equipment,
    Minimap,
    Inventory,
    CharacterInfo,
    Dungeon
}

public enum eInventoryCategory
{
    None = -1,
    CharacterInventory,
    ItemBoxInventory,
}

public enum eEffectType
{
    Recovery,
    Stamina_Grow,
    HP_Grow,
    Sharping,
}

public enum eEffect
{
    None = -1,
    AttackEffect,
    YellowConsumeEffect,
    GreenConsumeEffect,
    BlueConsumeEffect,
    
}

public enum eLootingCategory
{
    None = -1,
    Monster,
    Environment
}

public enum eProjectile
{
    None = -1,
    FireBall,
}

public enum eEnvironmentID
{
    None = -1,
    HerbPlant,
}

public enum eQuestResultCategory
{
    Clear,
    Fail,
}

public static class Utill
{

    #region Change Alpha
    // 알파값을 조금씩 올리는 함수
    public static Color ChangePlusAlpha(this Color _color, float _alpha)
    {
        Color temp = _color;
        temp.a += _alpha;
        return temp;
    }

    // 알파값을 조금씩 내리는 함수
    public static Color ChangeMinusAlpha(this Color _color, float _alpha)
    {
        Color temp = _color;
        temp.a -= _alpha;
        return temp;
    }

    // 알파값을 변경하는 함수
    public static Color ChangeAlpha(this Color _color, float _alpha)
    {
        Color temp = _color;
        temp.a = _alpha;
        return temp;
    }
    #endregion

}

public static class PATH
{
    public const string CSVPath         = @"\CSVData\";
    public const string CharacterPath   = @"Character\";
    public const string WeaponPath      = @"Weapon\";
    public const string AtlasPath       = @"Atlas\";
    public const string MonsterPath     = @"Monster\";
    public const string DamageTextPath  = @"DamageText\";
    public const string EffectPath      = @"Effect\";
    public const string ProjectilePath  = @"Projectile\";
    public const string EquipmentPath   = @"Equipment\";
    public const string EnvironmentPath = @"Environment\";
    public const string ScreenShotPath  = @"\Resources\ScreenShot\";
}

public static class Define
{
    // 매직넘버 정의
    public const int    Normal_Inventory_Slot_Num       = 24;
    public const int    Dungeon_Inventory_Num           = 20;
    public const int    Monster_Pooling_Num             = 10;
    public const int    Environment_Pooling_Num         = 10;
    public const int    DamageText_Num                  = 10;
    public const int    Particle_Num                    = 10;
    public const int    Projectile_Num                  = 10;
    public const float  Total_Sharpness                 = 70f;
    public const float  MAX_HP                          = 125f;
    public const float  MAX_STAMINA                     = 150f;
    public const float  ROLLING_STAMINA                 = 25f;
}
