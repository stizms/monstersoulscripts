﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
public partial class TableDownloader : MonoBehaviour
{
    Dictionary<eTable, Tuple<ITable, object, string>> m_mapDownloadList = new Dictionary<eTable, Tuple<ITable, object, string>>();

    public float fPercent { get { return (float)m_nNowDownloadCount / (int)eTable.Max; } }

    public bool Download(eTableCategory _category)
    {
        if (m_TbWWW == null)
        {
            m_TbWWW = gameObject.AddComponent<TableWWW>();
        }
        if (m_mapDownloadList.Count == 0 ||
            (m_mapDownloadList.Count + 1) != m_nSuccessCount)
        {
            m_mapDownloadList.Clear();

            // SetTables ---------------------------------------------------------------------

            switch (_category)
            {
                case eTableCategory.GameData:

                    AddTable<string, ConfigData>();
                    AddTable<eEquipment, WeaponData>();
                    AddTable<eEquipment, EquipmentData>();
                    AddTable<string, QuestGoalData>();
                    AddTable<string, QuestSectionData>();
                    AddTable<eQuestID, QuestData>();
                    AddTable<eNPCID, NPCData>();
                    AddTable<eEnvironmentID, EnvironmentData>();
                    AddTable<eItemID, ItemData>();
                    AddTable<eItemID, ConsumableItemInfoData>();
                    AddTable<eMonsterID, MonsterData>();
                    AddTable<eProjectile, ProjectileData>();
                    AddTable<eScene, DungeonData>();

                    m_nSuccessCount = m_mapDownloadList.Count;
                    // Download Start ----------------------------------------------------------------
                    AllRequest(_category);
                    break;
            }


            return true;
        }

        return false;
    }

    void AddTable<K, V>() where V : TableBase<K>, new()
    {
        V v = new V();
        var tb = TableMng.Ins.GetITable(v.eTb);
        object obj = new TableDataIO<V>();
        string s = v.sFileName;

        m_mapDownloadList.Add(v.eTb, Tuple.Create<ITable, object, string>(tb, obj, s));
    }
}
