﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public partial class TableDownloader : MonoBehaviour
{

    TableWWW m_TbWWW = null;

    int m_nNowDownloadCount = 0;
    int m_nSuccessCount = 0;
    
    // 게임데이터 불러올 때 요청
    void AllRequest(eTableCategory _category)
    {
        foreach (var val in m_mapDownloadList)
        {
            ITableIO io = (ITableIO)val.Value.Item2;
            io.Req(_category, val.Key, m_TbWWW,
            (bResult) =>
            {
                val.Value.Item1.AddList(io.liList);
                m_nNowDownloadCount++;
                
                if(_category == eTableCategory.GameData)
                {
                    if (m_nNowDownloadCount == m_nSuccessCount)
                    {
                        TableMng.Ins.downloadComplete = true;
                    }
                }
            });
        }
    }
}