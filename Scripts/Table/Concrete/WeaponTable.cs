﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WeaponData : TableBase<eEquipment>
{
    // TableBase
    public override eEquipment key => weaponID;
    public override eTable eTb => eTable.Weapon;
    public override string sFileName => "Weapon.csv";

    // 데이터
    public eEquipment       weaponID        { get; private set; }
    public string           weaponName      { get; private set; }
    public eWeaponCategory  weaponCategory  { get; private set; }
    public float            damage          { get; private set; }
    public eRare            rare            { get; private set; }
    public eElement         element         { get; private set; }
    public int              sellPrice       { get; private set; }
    public float            sharpness       { get; private set; }

    public WeaponData() { }

    public WeaponData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, string arg7)
    {
        SetData(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    }

    public WeaponData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, string arg7)
    {
        weaponID = (eEquipment)Enum.Parse(typeof(eEquipment), arg0);
        weaponName = arg1;
        weaponCategory = (eWeaponCategory)Enum.Parse(typeof(eWeaponCategory), arg2);
        damage = float.Parse(arg3);
        rare = (eRare)Enum.Parse(typeof(eRare), arg4);
        element = (eElement)Enum.Parse(typeof(eElement), arg5);
        sellPrice = int.Parse(arg6);
        sharpness = float.Parse(arg7);
    }

    public override void SetData(string[] args)
    {
        SetData(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
    }
}

public class WeaponTable : Table<eEquipment, WeaponData>
{
}
