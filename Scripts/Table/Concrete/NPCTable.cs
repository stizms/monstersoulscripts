﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class NPCData : TableBase<eNPCID>
{
    // TableBase
    public override eNPCID key => NPCID;
    public override eTable eTb => eTable.NPC;
    public override string sFileName => "NPC.csv";

    // 데이터
    public eNPCID NPCID { get; private set; }
    public eNPCType NPCType { get; private set; }
    public string NPCName { get; private set; }

    public NPCData() { }

    public NPCData(string arg0, string arg1, string arg2)
    {
        SetData(arg0, arg1, arg2);
    }

    public NPCData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2)
    {
        NPCID = (eNPCID)Enum.Parse(typeof(eNPCID), arg0);
        NPCType = (eNPCType)Enum.Parse(typeof(eNPCType), arg1);
        NPCName = arg2;
    }

    public override void SetData(string[] args)
    {
        SetData(args[0], args[1], args[2]);
    }
}

public class NPCTable : Table<eNPCID, NPCData>
{
}