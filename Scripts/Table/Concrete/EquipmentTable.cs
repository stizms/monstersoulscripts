﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentData : TableBase<eEquipment>
{
    // TableBase
    public override eEquipment key => equipmentID;
    public override eTable eTb => eTable.Equipment;
    public override string sFileName => "Equipment.csv";

    // 데이터
    public eEquipment           equipmentID         { get; private set; }
    public string               equipmentName       { get; private set; }
    public eEquipmentCategory   equipmentCategory   { get; private set; }
    public eRare                rare                { get; private set; }
    public float                def                 { get; private set; }
    public float                fireTolerance       { get; private set; }
    public float                waterTolerance      { get; private set; }
    public float                thunderTolerance    { get; private set; }
    public float                iceTolerance        { get; private set; }
    public int                  sellPrice           { get; private set; }

    public EquipmentData() { }

    public EquipmentData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, string arg7, string arg8, string arg9)
    {
        SetData(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
    }

    public EquipmentData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, string arg7, string arg8, string arg9)
    {
        equipmentID         = (eEquipment)Enum.Parse(typeof(eEquipment), arg0);
        equipmentName       = arg1;
        equipmentCategory   = (eEquipmentCategory)Enum.Parse(typeof(eEquipmentCategory), arg2);
        rare                = (eRare)Enum.Parse(typeof(eRare), arg3);
        def                 = float.Parse(arg4);
        fireTolerance       = float.Parse(arg5);
        waterTolerance      = float.Parse(arg6);
        thunderTolerance    = float.Parse(arg7);
        iceTolerance        = float.Parse(arg8);
        sellPrice           = int.Parse(arg9);
    }

    public override void SetData(string[] args)
    {
        SetData(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]);
    }
}

public class EquipmentTable : Table<eEquipment, EquipmentData>
{
}
