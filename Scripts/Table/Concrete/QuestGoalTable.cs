﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class QuestGoalData : TableBase<string>
{
    // TableBase
    public override string key => goalID;
    public override eTable eTb => eTable.QusetGoal;
    public override string sFileName => "QuestGoal.csv";
    
    // 데이터
    public string goalID { get; private set; }
    public eGoalType goalType { get; private set; }
    public string description { get; private set; }
    public int requiredAmount { get; private set; }
    public eMonsterID targetMonster { get; private set; }
    public eItemID targetCollection { get; private set; }

    public QuestGoalData() { }

    public QuestGoalData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5)
    {
        SetData(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    public QuestGoalData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5)
    {
        goalID = arg0;
        goalType = (eGoalType)Enum.Parse(typeof(eGoalType),arg1);
        description = arg2;
        requiredAmount = int.Parse(arg3);
        targetMonster = (eMonsterID)Enum.Parse(typeof(eMonsterID), arg4);
        targetCollection = (eItemID)Enum.Parse(typeof(eItemID), arg5);
    }

    public override void SetData(string[] args)
    {
        SetData(args[0], args[1], args[2], args[3], args[4], args[5]);
    }
}

public class QuestGoalTable : Table<string, QuestGoalData>
{
}