﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ProjectileData : TableBase<eProjectile>
{
    // TableBase
    public override eProjectile key => projectileID;
    public override eTable eTb => eTable.Projectile;
    public override string sFileName => "Projectile.csv";

    // 데이터
    public eProjectile  projectileID    { get; private set; }
    public float        damage          { get; private set; }
    public float        speed           { get; private set; }

    public ProjectileData() { }

    public ProjectileData(string arg0, string arg1, string arg2)
    {
        SetData(arg0, arg1, arg2);
    }

    public ProjectileData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2)
    {
        projectileID = (eProjectile)Enum.Parse(typeof(eProjectile), arg0);
        damage = float.Parse(arg1);
        speed = float.Parse(arg2);
    }

    public override void SetData(string[] args)
    {
        SetData(args[0], args[1], args[2]);
    }
}

public class ProjectileTable : Table<eProjectile, ProjectileData>
{
}

