﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MonsterData : TableBase<eMonsterID>
{
    // TableBase
    public override eMonsterID key => monsterID;
    public override eTable eTb => eTable.Monster;
    public override string sFileName => "Monster.csv";

    // 데이터
    public eMonsterID   monsterID { get; private set; }
    public string       monsterName { get; private set; }
    public string       monsterKind { get; private set; }
    public string       weakness { get; private set; }
    public float        MAX_HP { get; private set; }
    public float        damage { get; private set; }
    public float        headDef { get; private set; }
    public float        bodyDef { get; private set; }
    public float        tailDef { get; private set; }
    public float        detectionRange { get; private set; }
    public float        attackRange { get; private set; }
    public float        normalMoveSpeed { get; private set; }
    public float        chasingMoveSpeed { get; private set; }
    public int          lootingCount { get; private set; }
    public eProjectile  projectileID { get; private set; }
    public eItemID      lootingItem { get; private set; }
    public eItemID      rewardItem { get; private set; }
    public bool         isHeadWeakness { get; private set; }
    public bool         isBodyWeakness { get; private set; }
    public bool         isTailWeakness { get; private set; }

    public MonsterData() { }

    public MonsterData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, string arg7, string arg8, string arg9, string arg10, string arg11, string arg12, string arg13, string arg14, string arg15, string arg16, string arg17, string arg18, string arg19)
    {
        SetData(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17, arg18, arg19);
    }

    public MonsterData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, string arg7, string arg8, string arg9, string arg10, string arg11, string arg12, string arg13, string arg14, string arg15, string arg16, string arg17, string arg18, string arg19)
    {
        monsterID = (eMonsterID)Enum.Parse(typeof(eMonsterID), arg0);
        monsterName = arg1;
        monsterKind = arg2;
        weakness = arg3;
        MAX_HP = float.Parse(arg4);
        damage = float.Parse(arg5);
        headDef = float.Parse(arg6);
        bodyDef = float.Parse(arg7);
        tailDef = float.Parse(arg8);
        detectionRange = float.Parse(arg9);
        attackRange = float.Parse(arg10);
        normalMoveSpeed = float.Parse(arg11);
        chasingMoveSpeed = float.Parse(arg12);
        lootingCount = int.Parse(arg13);
        projectileID = (eProjectile)Enum.Parse(typeof(eProjectile), arg14);
        lootingItem = (eItemID)Enum.Parse(typeof(eItemID), arg15);
        rewardItem = (eItemID)Enum.Parse(typeof(eItemID), arg16);
        isHeadWeakness = bool.Parse(arg17);
        isBodyWeakness = bool.Parse(arg18);
        isTailWeakness = bool.Parse(arg19);
    }

    public override void SetData(string[] args)
    {
        SetData(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19]);
    }
}

public class MonsterTable : Table<eMonsterID, MonsterData>
{
}
