﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DungeonData : TableBase<eScene>
{
    // TableBase
    public override eScene key => dungeonID;
    public override eTable eTb => eTable.Dungeon;
    public override string sFileName => "Dungeon.csv";

    // 데이터
    public eScene dungeonID { get; private set; }
    public string dungeonName { get; private set; }
    public List<eMonsterID> monsterTypeList { get; private set; } = new List<eMonsterID>();
    public List<eEnvironmentID> environmentTypeList { get; private set; } = new List<eEnvironmentID>();
    public DungeonData() { }

    public DungeonData(string arg0, string arg1, string arg2,string arg3)
    {
        SetData(arg0, arg1, arg2, arg3);
    }

    public DungeonData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3)
    {
        dungeonID = (eScene)Enum.Parse(typeof(eScene), arg0);
        dungeonName = arg1;

        string[] monsterSplits = arg2.Split('/');

        foreach(var str in monsterSplits)
        {
            monsterTypeList.Add((eMonsterID)Enum.Parse(typeof(eMonsterID), str));
        }

        string[] environmentSplit = arg3.Split('/');

        foreach (var str in environmentSplit)
        {
            environmentTypeList.Add((eEnvironmentID)Enum.Parse(typeof(eEnvironmentID), str));
        }
    }

    public override void SetData(string[] args)
    {
        SetData(args[0], args[1], args[2], args[3]);
    }
}

public class DungeonTable : Table<eScene, DungeonData>
{
}

