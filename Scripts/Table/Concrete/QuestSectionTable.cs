﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class QuestSectionData : TableBase<string>
{
    // TableBase
    public override string key => sectionID;
    public override eTable eTb => eTable.QuestSection;
    public override string sFileName => "QuestSection.csv";

    // 데이터
    public string sectionID { get; private set; }
    public string description { get; private set; }
    public int goalCount { get; private set; }

    public QuestSectionData() { }

    public QuestSectionData(string arg0, string arg1, string arg2)
    {
        SetData(arg0, arg1, arg2);
    }

    public QuestSectionData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2)
    {
        sectionID = arg0;
        description = arg1;
        goalCount = int.Parse(arg2);
    }

    public override void SetData(string[] args)
    {
        SetData(args[0], args[1], args[2]);
    }
}

public class QuestSectionTable : Table<string, QuestSectionData>
{
}