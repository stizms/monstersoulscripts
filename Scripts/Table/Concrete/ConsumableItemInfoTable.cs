﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ConsumableItemInfoData : TableBase<eItemID>
{
    // TableBase
    public override eItemID key => itemID;
    public override eTable eTb => eTable.Consumable_Item_Info;
    public override string sFileName => "Consumable_Item_Info.csv";

    // 데이터
    public eItemID itemID { get; private set; }
    public eEffectType effectType { get; private set; }
    public float effectAmount { get; private set; }
    public float effectTime { get; private set; }

    public ConsumableItemInfoData() { }

    public ConsumableItemInfoData(string arg0, string arg1, string arg2, string arg3)
    {
        SetData(arg0, arg1, arg2, arg3);
    }

    public ConsumableItemInfoData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3)
    {
        itemID = (eItemID)Enum.Parse(typeof(eItemID), arg0);
        effectType = (eEffectType)Enum.Parse(typeof(eEffectType), arg1);
        effectAmount = float.Parse(arg2);
        effectTime = float.Parse(arg3);
    }

    public override void SetData(string[] args)
    {
        SetData(args[0], args[1], args[2], args[3]);
    }
}

public class ConsumableItemInfoTable : Table<eItemID, ConsumableItemInfoData>
{
}

