﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ItemData : TableBase<eItemID>
{
    // TableBase
    public override eItemID key => itemID;
    public override eTable eTb => eTable.Item;
    public override string sFileName => "Item.csv";

    // 데이터
    public eItemID itemID { get; private set; }
    public string itemName { get; private set; }
    public eItemType itemType { get; private set; }
    public string description { get; private set; }
    public int buyPrice { get; private set; }
    public int sellPrice { get; private set; }
    public int characterLimit { get; private set; }
    public int itemBoxLimit { get; private set; }

    public ItemData() { }

    public ItemData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, string arg7)
    {
        SetData(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    }

    public ItemData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, string arg7)
    {
        itemID = (eItemID)Enum.Parse(typeof(eItemID), arg0);
        itemName = arg1;
        itemType = (eItemType)Enum.Parse(typeof(eItemType),arg2);
        description = arg3;
        buyPrice = int.Parse(arg4);
        sellPrice = int.Parse(arg5);
        characterLimit = int.Parse(arg6);
        itemBoxLimit = int.Parse(arg7);
    }

    public override void SetData(string[] args)
    {
        SetData(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
    }
}

public class ItemTable : Table<eItemID, ItemData>
{
}

