﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentData : TableBase<eEnvironmentID>
{
    // TableBase
    public override eEnvironmentID key => environmentID;
    public override eTable eTb => eTable.Environment;
    public override string sFileName => "Environment.csv";

    // 데이터
    public eEnvironmentID environmentID { get; private set; }
    public string environmentName { get; private set; }
    public int lootingCount { get; private set; }
    public eItemID lootingItem { get; private set; }

    public EnvironmentData() { }

    public EnvironmentData(string arg0, string arg1, string arg2, string arg3)
    {
        SetData(arg0, arg1, arg2, arg3);
    }

    public EnvironmentData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3)
    {
        environmentID = (eEnvironmentID)Enum.Parse(typeof(eEnvironmentID), arg0);
        environmentName = arg1;
        lootingCount = int.Parse(arg2);
        lootingItem = (eItemID)Enum.Parse(typeof(eItemID), arg3);
    }

    public override void SetData(string[] args)
    {
        SetData(args[0], args[1], args[2], args[3]);
    }
}

public class EnvironmentTable : Table<eEnvironmentID, EnvironmentData>
{
}
