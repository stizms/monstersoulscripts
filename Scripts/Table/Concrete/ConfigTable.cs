﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigData : TableBase<string>
{
    // TableBase
    public override string key => configID;
    public override eTable eTb => eTable.Config;
    public override string sFileName => "Config.csv";

    // 데이터
    public string configID { get; private set; }
    public string version { get; private set; }

    public ConfigData() { }

    public ConfigData(string arg0, string arg1)
    {
        SetData(arg0, arg1);
    }

    public ConfigData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1)
    {
        configID = arg0;
        version  = arg1;
    }

    public override void SetData(string[] args)
    {
        SetData(args[0], args[1]);
    }
}

public class ConfigTable : Table<string, ConfigData>
{
}