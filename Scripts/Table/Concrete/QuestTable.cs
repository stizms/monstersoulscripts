﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class QuestData : TableBase<eQuestID>
{
    // TableBase
    public override eQuestID key => questID;
    public override eTable eTb => eTable.Quest;
    public override string sFileName => "Quest.csv";

    // 데이터
    public eQuestID         questID         { get; private set; }
    public string           questName       { get; private set; }
    public string           description     { get; private set; }
    public eQuestType       questType       { get; private set; }
    public int              sectionCount    { get; private set; }
    public int              rewardMoney     { get; private set; }
    public List<eQuestID>   openQuest       { get; private set; } = new List<eQuestID>();
    public int              hunterRank      { get; private set; }
    public bool             rankUpFlag      { get; private set; }
    public eScene           relatedDungeon  { get; private set; }
    public eQuestTarget     mainTarget      { get; private set; }
    public int              questLife       { get; private set; }
    public bool             isBossQuest     { get; private set; }

    public QuestData() { }

    public QuestData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, string arg7, string arg8, string arg9, string arg10, string arg11, string arg12)
    {
        SetData(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12);
    }

    public QuestData(string[] a_Val)
    {
        SetData(a_Val);
    }

    public void SetData(string arg0, string arg1, string arg2, string arg3, string arg4, string arg5, string arg6, string arg7, string arg8, string arg9, string arg10, string arg11, string arg12)
    {
        questID = (eQuestID)Enum.Parse(typeof(eQuestID),arg0);
        questName = arg1;
        description = arg2;
        questType = (eQuestType)Enum.Parse(typeof(eQuestType), arg3);
        sectionCount = int.Parse(arg4);
        rewardMoney = int.Parse(arg5);

        string[] openTemp = arg6.Split('/');
        foreach(var str in openTemp)
        {
            openQuest.Add((eQuestID)Enum.Parse(typeof(eQuestID), str));
        }

        hunterRank = int.Parse(arg7);
        int temp = int.Parse(arg8);

        switch(temp)
        {
            case 0:
                rankUpFlag = false;
                break;
            default:
                rankUpFlag = true;
                break;
        }

        relatedDungeon = (eScene)Enum.Parse(typeof(eScene), arg9);
        mainTarget = (eQuestTarget)Enum.Parse(typeof(eQuestTarget), arg10);
        questLife = int.Parse(arg11);
        isBossQuest = bool.Parse(arg12);
    }

    public override void SetData(string[] args)
    {
        SetData(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12]);
    }
}

public class QuestTable : Table<eQuestID, QuestData>
{
}