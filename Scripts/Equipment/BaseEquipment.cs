﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEquipment : MonoBehaviour
{
    // 웨폰 이름
    virtual public eEquipment equipmentID { get { return (eEquipment)Enum.Parse(typeof(eEquipment), parsingEquipmentName); } }
    // 파싱할 때 필요
    private string parsingEquipmentName;

    // 방어구 카테고리
    public eEquipmentCategory   equipmentCategory;
    public eRare                rare;
    public float                def;
    public float                fireTolerance;
    public float                waterTolerance;
    public float                thunderTolerance;
    public float                iceTolerance;

    void Awake()
    {
        parsingEquipmentName        = name.Substring(0, name.IndexOf("(Clone)"));

        EquipmentData equipmentTD   = TableMng.Ins.equipmentTb[equipmentID];
        equipmentCategory           = equipmentTD.equipmentCategory;
        rare                        = equipmentTD.rare;
        def                         = equipmentTD.def;
        fireTolerance               = equipmentTD.fireTolerance;
        waterTolerance              = equipmentTD.waterTolerance;
        thunderTolerance            = equipmentTD.thunderTolerance;
        iceTolerance                = equipmentTD.iceTolerance;

    }

}
