﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapFollowCam : MonoBehaviour
{
    public Character followTarget;

    public void Update()
    {
        if(followTarget != null)
            transform.position = new Vector3(followTarget.transform.position.x, transform.position.y, followTarget.transform.position.z);
    }
}
