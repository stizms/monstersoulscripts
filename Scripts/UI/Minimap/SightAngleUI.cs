﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SightAngleUI : MonoBehaviour
{
    #region INSPECTOR

    public Transform playerCam;

    #endregion
    
    void Update()
    {
        // 카메라가 처다보는 방향 계산
        float width = transform.position.x - playerCam.position.x;
        float height = transform.position.z - playerCam.position.z;
        
        float radian = Mathf.Atan2(height, width);

        float angle = -1*(radian * 180 / Mathf.PI) + 90f;

        transform.rotation = Quaternion.Euler(0, angle, 0);

    }
}
