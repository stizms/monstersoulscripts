﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropItemUI : MonoBehaviour
{
    #region INSPECTOR

    public Text dropAmount;

    #endregion

    int itemAmount = 0;
    eItemID slotItem = eItemID.None;
    int nowAmount = 0;


    ItemPackageSlot targetSlot;
    Inventory characterInventory;
    OptionItemPackageUI itemPackageUI;

    public void VisualizeDropUI(ItemPackageSlot itemPackageSlot, eItemID itemID)
    {
        if (itemID == eItemID.None) return;
        else
        {
            slotItem = itemID;
            targetSlot = itemPackageSlot;
            if (characterInventory == null) characterInventory = UserDataMng.Ins.nowCharacter.characterInventory;
            itemAmount = characterInventory.GetItemCount(itemID);
            nowAmount = 1;
            dropAmount.text = nowAmount.ToString();
        }
        gameObject.SetActive(true);
    }

    public void NonVisualizeDropUI()
    {
        slotItem = eItemID.None;
        targetSlot = null;
        itemAmount = 0;
        nowAmount = 0;
        gameObject.SetActive(false);
    }


    public void OnUpButton()
    {
        if(nowAmount < itemAmount)
        {
            nowAmount++;
            dropAmount.text = nowAmount.ToString();
        }
    }

    public void OnDownButton()
    {
        if(nowAmount > 1)
        {
            nowAmount--;
            dropAmount.text = nowAmount.ToString();
        }
    }

    public void OnDropButton()
    {
        if (itemPackageUI == null) itemPackageUI = UIMng.Ins.optionUI.itemPackageUI;
        if(UserDataMng.Ins.nowCharacter.characterInventory.DeductItemAmount(slotItem,nowAmount))
            targetSlot.itemAmountText.text = (itemAmount - nowAmount).ToString();
        else
        {
            targetSlot.ResetSlot();
            itemPackageUI.itemInfoUI.SetEmptyInfo();
        }
        itemPackageUI.itemInfoUI.SetItemInfo(slotItem);
        itemPackageUI.blackBoard.SetActive(false);
        itemPackageUI.isSelected = false;
        NonVisualizeDropUI();
    }
}
