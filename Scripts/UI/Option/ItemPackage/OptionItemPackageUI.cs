﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionItemPackageUI : MonoBehaviour, IOptionChild
{
    #region INSPECTOR

    public  ItemInfoUI itemInfoUI;
    public  DropItemUI dropItemUI;
    public  GameObject blackBoard;
    public  List<ItemPackageSlot> slotList = new List<ItemPackageSlot>();

    #endregion

    private OptionUI optionUI;
    
    [HideInInspector]
    public bool isSelected = false;

    public void VisualizeItemPackage()
    {
        if (optionUI == null) optionUI = UIMng.Ins.optionUI;

        optionUI.NonVisualizeMoneySection();
        optionUI.NonVisualizeStatusSection();
        optionUI.NonVisualizeOptionSection();
        optionUI.ChangeNotice("아이템을 선택해 버릴 수 있습니다.");

        int index = 0;
        foreach(var item in UserDataMng.Ins.nowCharacter.characterInventory.itemList)
        {
            slotList[index].SetData(item.Key, item.Value);
            index++;
        }
        slotList[0].OnHoverEnter();
        gameObject.SetActive(true);
    }

    // 꺼질 때 초기화
    public void ResetSlotList()
    {
        foreach (var slot in slotList)
        {
            slot.ResetSlot();
            slot.hoverOverlay.SetActive(false);
        }
    }

    public void NonVisualizeOptionChild()
    {
        if (isSelected)
        {
            isSelected = false;
            blackBoard.SetActive(false);
            dropItemUI.NonVisualizeDropUI();
        }
        else
        {
            ResetSlotList();
            optionUI.VisualizeMoneySection();
            optionUI.VisualizeStatusSection();
            optionUI.VisualizeOptionSection();
            optionUI.activeOption = null;
            optionUI.isChildUIActive = false;

            gameObject.SetActive(false);
        }
    }
}
