﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemPackageSlot : InventorySlot
{
    private OptionItemPackageUI itemPackageUI;

    public override void OnHoverEnter()
    {
        if (itemPackageUI == null) itemPackageUI = UIMng.Ins.optionUI.itemPackageUI;

        if (UIMng.Ins.optionUI.prevHover == hoverOverlay) return;
        SoundMng.Ins.ExternalSoundPlay(eSound.PopUpHoverSound.ToDesc(), eChannel.UI_Effect);
        UIMng.Ins.optionUI.HoverChange(hoverOverlay);
        itemPackageUI.itemInfoUI.VisualizeItemInfo(slotItem);
    }

    public override void OnClickBtn()
    {
        itemPackageUI.blackBoard.SetActive(true);
        itemPackageUI.dropItemUI.VisualizeDropUI(this, slotItem);
        itemPackageUI.isSelected = true;
    }
}
