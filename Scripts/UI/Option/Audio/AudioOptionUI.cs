﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioOptionUI : MonoBehaviour, IOptionChild
{

    #region INSPECTOR

    public  Text     backGroundVolumeText;
    public  Text     soundEffectVolumeText;
    public  Slider   backGroundSlider;
    public  Slider   soundEffectSlider;

    #endregion

    private OptionUI optionUI;

    public void VisualizeAudioOptionUI()
    {
        if (optionUI == null) optionUI = UIMng.Ins.optionUI;
        optionUI.NonVisualizeOptionSection();
        optionUI.NonVisualizeMoneySection();
        optionUI.NonVisualizeStatusSection();
        optionUI.ChangeNotice("오디오 환경을 설정합니다.");

        float backVolume = SoundMng.Ins.backGroundVolume;
        backGroundSlider.value = backVolume;
        UpdateBackGroundVolumeUI(backVolume);

        float SEVolume = SoundMng.Ins.soundEffectVolume;
        soundEffectSlider.value = SEVolume;
        UpdateSoundEffectVolumeUI(SEVolume);

        gameObject.SetActive(true);
    }

    public void NonVisualizeOptionChild()
    {
        optionUI.VisualizeMoneySection();
        optionUI.VisualizeOptionSection();
        optionUI.VisualizeStatusSection();
        optionUI.activeOption = null;
        optionUI.isChildUIActive = false;

        gameObject.SetActive(false);
    }

    public void UpdateBackGroundVolumeUI(float volume)
    {
        backGroundVolumeText.text = ((int)(volume * 100f)).ToString();
    }

    public void UpdateSoundEffectVolumeUI(float volume)
    {
        soundEffectVolumeText.text = ((int)(volume * 100f)).ToString();
    }
}
