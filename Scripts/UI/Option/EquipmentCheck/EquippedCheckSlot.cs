﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EquippedCheckSlot : EquipmentSlot
{
    private OptionUI optionUI;

    public override void SetData(eEquipment equipmentID)
    {
        if (optionUI == null) optionUI = UIMng.Ins.optionUI;

        slotEquipment = equipmentID;

        if (slotEquipment == eEquipment.None)
        {
            equipmentIcon.gameObject.SetActive(false);
            equipmentName.text = "";
            return;
        }
        else if (slotEquipment > eEquipment.Weapon && slotEquipment < eEquipment.Helmet)
        {
            WeaponData tempWeapon   = TableMng.Ins.weaponTb[slotEquipment];
            equipmentIcon.sprite    = AtlasMng.Ins.GetSprite(eAtlasCategory.Equipment, tempWeapon.weaponCategory.ToString());
            equipmentName.text      = tempWeapon.weaponName;
        }
        else
        {
            EquipmentData tempEquipment = TableMng.Ins.equipmentTb[equipmentID];
            equipmentIcon.sprite        = AtlasMng.Ins.GetSprite(eAtlasCategory.Equipment, tempEquipment.equipmentCategory.ToString());
            equipmentName.text          = tempEquipment.equipmentName;
        }

        equipmentIcon.gameObject.SetActive(true);
    }

    public override void OnHoverEnter()
    {
        if (optionUI.prevHover == hoverOverlay) return;

        SoundMng.Ins.ExternalSoundPlay(eSound.PopUpHoverSound.ToDesc(), eChannel.UI_Effect);
        optionUI.HoverChange(hoverOverlay);
        optionUI.equipmentCheckUI.equipmentInfoUI.SetData(slotEquipment);
    }
}
