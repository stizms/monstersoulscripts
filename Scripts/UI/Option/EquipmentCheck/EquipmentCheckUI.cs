﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentCheckUI : MonoBehaviour, IOptionChild
{
    #region INSPECTOR

    public  EquippedCheckSlot equippedWeaponSlot;
    public  EquippedCheckSlot equippedHelmetSlot;
    public  EquippedCheckSlot equippedVestSlot;
    public  EquippedCheckSlot equippedGloveSlot;
    public  EquippedCheckSlot equippedBeltSlot;
    public  EquippedCheckSlot equippedPantsSlot;
    public  EquipmentInfoUI   equipmentInfoUI;

    #endregion

    private OptionUI          optionUI;

    public void Init()
    {
        UserData tempUserData = UserDataMng.Ins.nowCharacter;

        equippedWeaponSlot.SetData(tempUserData.equipWeapon);
        equippedHelmetSlot.SetData(tempUserData.equipHelmet);
        equippedVestSlot.SetData(tempUserData.equipVest);
        equippedGloveSlot.SetData(tempUserData.equipGlove);
        equippedBeltSlot.SetData(tempUserData.equipBelt);
        equippedPantsSlot.SetData(tempUserData.equipPants);
    }

    public void ResetSlotList()
    {
        equippedWeaponSlot.ResetSlot();
        equippedHelmetSlot.ResetSlot();
        equippedVestSlot.ResetSlot();
        equippedGloveSlot.ResetSlot();
        equippedBeltSlot.ResetSlot();
        equippedPantsSlot.ResetSlot();
    }

    public void VisualizeEquipmentCheck()
    {
        if (optionUI == null) optionUI = UIMng.Ins.optionUI;

        optionUI.NonVisualizeMoneySection();
        optionUI.NonVisualizeOptionSection();
        optionUI.ChangeNotice("자세한 정보를 확인할 장비를 선택하십시오.");

        Init();

        equippedWeaponSlot.OnHoverEnter();

        gameObject.SetActive(true);
    }

    public void NonVisualizeOptionChild()
    {
        optionUI.VisualizeMoneySection();
        optionUI.VisualizeOptionSection();
        optionUI.activeOption = null;
        optionUI.isChildUIActive = false;

        gameObject.SetActive(false);
    }
}
