﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class OptionButton : MonoBehaviour, IPointerEnterHandler
{
    #region INSPECTOR

    public Text buttonText;
    public GameObject hoverOverlay;
    // 호버될 때 바꿀 noticeSection
    public string hoverDescription;

    #endregion

    private OptionUI optionUI;
    private Color grayColor = new Color(0.6504f,0.6504f,0.6504f);


    public void InActiveButton()
    {
        buttonText.color = grayColor;
    }

    public void ActiveButton()
    {
        buttonText.color = Color.white;
    }

    public void ButtonHoverEnter()
    {
        if (optionUI == null) optionUI = UIMng.Ins.optionUI;

        if (optionUI.prevHover != null && optionUI.prevHover == hoverOverlay) return;
        SoundMng.Ins.ExternalSoundPlay(eSound.CharacterSlotHoverSound.ToDesc(), eChannel.UI_Effect);
        UIMng.Ins.optionUI.HoverChange(hoverOverlay);
        optionUI.ChangeNotice(hoverDescription);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ButtonHoverEnter();
    }
    
    // 아이템 주머니
    public void OnItemPackageBtn()
    {
        optionUI.isChildUIActive = true;
        optionUI.itemPackageUI.VisualizeItemPackage();
        optionUI.activeOption = optionUI.itemPackageUI;
    }

    // 장비 목록
    public void OnEquipmentListBtn()
    {
        optionUI.isChildUIActive = true;
        optionUI.equipmentCheckUI.VisualizeEquipmentCheck();
        optionUI.activeOption = optionUI.equipmentCheckUI;
    }


    // 오디오 옵션
    public void OnAudioOptionBtn()
    {
        optionUI.isChildUIActive = true;
        optionUI.audioOption.VisualizeAudioOptionUI();
        optionUI.activeOption = optionUI.audioOption;
    }


    // 저장할 때 공통적으로 사용되는 코루틴 ( 4초 걸림 )
    IEnumerator SaveCoroutine()
    {
        UserDataMng.Ins.SaveUserData(UserDataMng.Ins.nowCharacter);
        UIMng.Ins.saveUI.gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        UIMng.Ins.saveUI.gameObject.SetActive(false);
        UIMng.Ins.VisualizeMention("저장했습니다.");
        yield return new WaitForSeconds(2f);
        UIMng.Ins.NonVisualizeMention();
    }

    private void ExitPopUp()
    {
        optionUI.isChildUIActive = false;
    }


    #region SaveBtn

    public void OnSaveBtn()
    {
        if (SceneMng.Ins.nowScene > eScene.Dungeon) return;
        optionUI.isChildUIActive = true;
        UIMng.Ins.twoButtonPopUp.SetPopUp("데이터를 저장합니까?", SaveBtn, ExitPopUp, "예", "아니요");
    }

    public void SaveBtn()
    {
        StartCoroutine(SaveBtnCoroutine());
    }

    // 세이브 버튼 용 코루틴
    IEnumerator SaveBtnCoroutine()
    {
        StartCoroutine(SaveCoroutine());
        yield return new WaitForSeconds(4.5f);
        optionUI.isChildUIActive = false;
        optionUI.NonVisualizeMainOption();
        UIMng.Ins.lobby.TurnOnAllUI();
    }

    #endregion

    #region GoBackTitle
    
    public void OnGoBackTitleBtn()
    {
        if (SceneMng.Ins.nowScene > eScene.Dungeon) return;
        optionUI.isChildUIActive = true;
        UIMng.Ins.twoButtonPopUp.SetPopUp("타이틀 화면으로 돌아갑니까?", SaveAndGoBackTitle, ExitPopUp, "예", "아니요");
    }

    private void SaveAndGoBackTitle()
    {
        StartCoroutine(GoBackTitleCoroutine());
    }
    
    IEnumerator GoBackTitleCoroutine()
    {
        StartCoroutine(SaveCoroutine());
        yield return new WaitForSeconds(4f);
        StartCoroutine(FadeInAndGoBackTitle());
    }

    IEnumerator FadeInAndGoBackTitle()
    {
        Image blackBoard;
        blackBoard = UIMng.Ins.lobby.blackBoard;
        blackBoard.gameObject.SetActive(true);
        while (true)
        {
            blackBoard.color = Utill.ChangePlusAlpha(blackBoard.color, 0.05f);

            if (blackBoard.color.a >= 1f)
            {
                Destroy(GameObject.Find("SoundMng"));
                Destroy(GameObject.Find("UIMng"));
                Destroy(GameObject.Find("TableMng"));
                Destroy(GameObject.Find("EquipmentMng"));
                Destroy(GameObject.Find("AtlasMng"));
                Destroy(GameObject.Find("QuestMng"));

                SceneMng.Ins.ChangeScene(eScene.Intro);
                break;
            }
            yield return new WaitForSeconds(0.03f);
        }
    }

    #endregion

    #region ExitGame

    public void OnExitGameBtn()
    {
        if (SceneMng.Ins.nowScene > eScene.Dungeon) return;

        optionUI.isChildUIActive = true;
        UIMng.Ins.twoButtonPopUp.SetPopUp("게임을 종료합니다.\n그렇게 하시겠습니까?", SaveAndExit, ExitPopUp, "예", "아니요");
    }

    private void SaveAndExit()
    {
        StartCoroutine(ExitGameCoroutine());
    }

    IEnumerator ExitGameCoroutine()
    {
        StartCoroutine(SaveCoroutine());
        yield return new WaitForSeconds(4f);
        UnityEditor.EditorApplication.isPlaying = false;
    }

    #endregion
}
