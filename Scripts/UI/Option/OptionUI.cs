﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionUI : MonoBehaviour, IInteractingUI
{
    #region INSPECTOR

    // 옵션
    public GameObject           optionSection;
    public List<OptionButton>   optionBtnList;

    // 알림텍스트
    public Text                 noticeText;

    // 돈
    public GameObject           moneySection;
    public Text                 moneyText;

    // 스테이터스
    public CharacterStatusUI    statusSection;


    // 자식 UI
    public OptionItemPackageUI  itemPackageUI;
    public EquipmentCheckUI     equipmentCheckUI;
    public AudioOptionUI        audioOption; 

    #endregion
    

    [HideInInspector]
    public GameObject prevHover = null;
    [HideInInspector]
    public bool isChildUIActive = false;

    public IOptionChild activeOption = null;


    public void HoverChange(GameObject hoverOverlay)
    {
        if (prevHover != null && prevHover != hoverOverlay)
            prevHover.SetActive(false);
        hoverOverlay.SetActive(true);
        prevHover = hoverOverlay;
    }

    public void VisualizeMainOption()
    {
        //UI 끌거 여기서 작업
        if(SceneMng.Ins.nowScene == eScene.Lobby)
        {
            UIMng.Ins.lobby.TurnOffAllUI();
            optionBtnList[3].ActiveButton();
            optionBtnList[4].ActiveButton();
            optionBtnList[5].ActiveButton();
        }
        else if(SceneMng.Ins.nowScene > eScene.Dungeon)
        {
            UIMng.Ins.dungeon.TurnOffAllUI();
            optionBtnList[3].InActiveButton();
            optionBtnList[4].InActiveButton();
            optionBtnList[5].InActiveButton();
        }

        VisualizeOptionSection();
        VisualizeMoneySection();
        VisualizeStatusSection();
        gameObject.SetActive(true);
        GameMng.Ins.Lock();
    }

    public void NonVisualizeMainOption()
    {
        // UI 킬거 여기서 작업
        if(SceneMng.Ins.nowScene == eScene.Lobby)
            UIMng.Ins.lobby.TurnOnAllUI();
        else if(SceneMng.Ins.nowScene > eScene.Dungeon)
            UIMng.Ins.dungeon.TurnOnAllUI();

        prevHover = null;
        gameObject.SetActive(false);
        GameMng.Ins.CursorNonVisualize();
        GameMng.Ins.UnLock();
    }

    public void ExitUI()
    {
        if (!isChildUIActive)
            NonVisualizeMainOption();
        else
        {
            if(activeOption != null)
                activeOption.NonVisualizeOptionChild();
        }
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(1))
            ExitUI();
    }

    #region OptionSection

    public void VisualizeOptionSection()
    {
        optionSection.SetActive(true);
        optionBtnList[0].ButtonHoverEnter();
    }

    public void NonVisualizeOptionSection()
    {
        if(prevHover != null)
        {
            prevHover.SetActive(false);
            prevHover = null;
        }
        optionSection.SetActive(false);
    }

    #endregion

    #region MoneySection

    public void VisualizeMoneySection()
    {
        moneyText.text = UserDataMng.Ins.nowCharacter.money.ToString();
        moneySection.SetActive(true);
    }

    public void NonVisualizeMoneySection()
    {
        moneySection.SetActive(false);
    }

    #endregion

    #region NoticeSection

    public void ChangeNotice(string str)
    {
        noticeText.text = str;
    }

    #endregion

    #region StatusSection

    public void VisualizeStatusSection()
    {
        statusSection.SetStatus();
        statusSection.gameObject.SetActive(true);
    }

    public void NonVisualizeStatusSection()
    {
        statusSection.gameObject.SetActive(false);
    }

    #endregion
}
