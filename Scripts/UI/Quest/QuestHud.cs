﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestHud : MonoBehaviour
{
    #region INSPECTOR

    public Transform    content;
    public Text         questName;
    public GameObject   sectionPrefab;
    public GameObject   contentPrefab;

    #endregion

    // 초기 세팅
    public void Init()
    {
        Quest quest     = QuestMng.Ins.nowQuest.quest;
        questName.text  = quest.questName;
        AddSectionUI(quest.sections.Peek());
    }

    // section 모두 지우기
    public void ResetContent()
    {
        Destroy(content.gameObject);
        content = Instantiate(contentPrefab, transform).GetComponent<Transform>();
    }

    // clear 했을 때 Dequeue하고 0이 아니면 섹션이 남아 있다면 호출
    public void AddSectionUI(QuestSection section)
    {
        if (QuestMng.Ins.nowQuest == null)  return;
        SectionUI tempSection = Instantiate(sectionPrefab, content).GetComponent<SectionUI>();
        tempSection.SetData(section);
    }

    public void CompleteCheck()
    {
        SectionUI tempSection = content.GetChild(content.childCount - 1).GetComponent<SectionUI>();
        tempSection.CompleteCheck();
    }
}
