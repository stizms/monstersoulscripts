﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SectionUI : MonoBehaviour
{
    #region INSPECTOR

    public Text         sectionText;
    public GameObject   checkSprite;

    #endregion

    public void SetData(QuestSection section)
    {
        sectionText.text = section.description;
    }

    public void CompleteCheck()
    {
        checkSprite.SetActive(true);
    }
}
