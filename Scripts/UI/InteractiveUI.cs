﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractiveUI : MonoBehaviour
{
    public Text interactiveName;
    Transform followingTarget;

    public void VisualizeInteractiveUI(string name,Transform target)
    {
        interactiveName.text = name;
        followingTarget = target;
        gameObject.SetActive(true);
    }

    public void NonVisualizeInteractiveUI()
    {
        gameObject.SetActive(false);
    }

    private void Update()
    {
        if (followingTarget != null)
        {
            Vector3 temp = Camera.main.WorldToScreenPoint(followingTarget.position);
            temp.y += 120;
            transform.position = temp;
        }
    }

}
