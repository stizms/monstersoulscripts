﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpUI : MonoBehaviour
{
    #region INSPECTOR

    // 팝업 내용
    public Text popUpText;

    // 버튼
    public Text btn1Text;
    public Text btn2Text;

    public GameObject btn1Hover;
    public GameObject btn2Hover;

    // Button CallBack
    private System.Action btn1CallBack;
    private System.Action btn2CallBack;

    #endregion
    

    // 버튼 함수와 텍스트 세팅
    public void SetPopUp(string mainText, System.Action func1, string firstBtnText)
    {
        popUpText.text  = mainText;
        btn1CallBack    = func1;
        btn1Text.text   = firstBtnText;
        GameMng.Ins.CursorVisualize();
        gameObject.SetActive(true);
    }

    public void SetPopUp(string mainText, System.Action func1, System.Action func2, string firstBtnText, string secondBtnText)
    {
        popUpText.text  = mainText;
        btn1CallBack    = func1;
        btn2CallBack    = func2;
        btn1Text.text   = firstBtnText;
        btn2Text.text   = secondBtnText;
        GameMng.Ins.CursorVisualize();
        gameObject.SetActive(true);
    }

    public void OnBtn1Click()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.PopUpClickSound.ToDesc(), eChannel.UI_Effect);
        btn1CallBack?.Invoke();
        ResetPopUp();
    }

    public void OnBtn2Click()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.PopUpClickSound.ToDesc(), eChannel.UI_Effect);
        btn2CallBack?.Invoke();
        ResetPopUp();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            SoundMng.Ins.ExternalSoundPlay(eSound.PopUpClickSound.ToDesc(), eChannel.UI_Effect);
            OnBtn2Click();
        }
    }

    // 초기화 하고 끄기
    private void ResetPopUp()
    {
        btn1CallBack = null;
        btn2CallBack = null;
        btn1Hover.gameObject.SetActive(false);
        if(btn2Hover != null)   btn2Hover.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }

    // 버튼 호버이미지를 키는 함수
    public void OnBtnHoverEnter(GameObject hoverImg)
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.PopUpHoverSound.ToDesc(), eChannel.UI_Effect);
        hoverImg.SetActive(true);
    }

    // 버튼 호버이미지를 끄는 함수
    public void OnBtnHoverExit(GameObject hoverImg)
    {
        hoverImg.SetActive(false);
    }
}
