﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class HoverUI : MonoBehaviour, IPointerEnterHandler
{
    #region INSPECTOR

    public GameObject hoverOverlay;

    #endregion

    public abstract void OnHoverEnter();
    public abstract void OnClickBtn();

    public void OnPointerEnter(PointerEventData eventData)
    {
        OnHoverEnter();
    }
}
