﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootingHud : MonoBehaviour
{
    #region INSPECTOR

    public LootingSlot monsterSlot;
    public LootingSlot itemSlot;

    #endregion

    [HideInInspector]
    public LootingSlot nowLootingHud;

    public void VisualizeLootingHud(ILooting lootingInfo, Transform target)
    {
        switch(lootingInfo.lootingCategory)
        {
            case eLootingCategory.Monster:
                monsterSlot.SetData(lootingInfo, target);
                nowLootingHud = monsterSlot;
                break;
            case eLootingCategory.Environment:
                itemSlot.SetData(lootingInfo, target);
                nowLootingHud = itemSlot;
                break;
        }
    }

    public void NonVisualizeLootingHud()
    {
        if(nowLootingHud != null)
        {
            nowLootingHud.gameObject.SetActive(false);
            nowLootingHud = null;
        }
    }
}
