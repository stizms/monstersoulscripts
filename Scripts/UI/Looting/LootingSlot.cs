﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LootingSlot : MonoBehaviour
{
    #region INSPECTOR

    public Image        icon;
    public Text         nameText;
    public GameObject   maxIcon;
    public GameObject   mouseBtnIcon;
    #endregion

    private Transform   target;
    
    // 설정
    public void SetData(ILooting lootingInfo, Transform lookTarget)
    {
        switch(lootingInfo.lootingCategory)
        {
            case eLootingCategory.Monster:
                icon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Monster, lootingInfo.GetSpriteName());
                break;
            case eLootingCategory.Environment:
                icon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Item, lootingInfo.GetSpriteName());
                if(UserDataMng.Ins.nowCharacter.characterInventory.GetItemCount(lootingInfo.lootingItem) < TableMng.Ins.itemTb[lootingInfo.lootingItem].characterLimit)
                {
                    mouseBtnIcon.SetActive(true);
                    maxIcon.SetActive(false);
                }
                else
                {
                    mouseBtnIcon.SetActive(false);
                    maxIcon.SetActive(true);
                }
                break;
        }

        target          = lookTarget;
        nameText.text   = lootingInfo.GetObjectName();
        gameObject.SetActive(true);
    }

    private void Update()
    {
        if (target != null)
        {
            Vector3 temp = Camera.main.WorldToScreenPoint(target.position);
            temp.y += 120;
            transform.position = temp;
        }
    }
}
