﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveUI : MonoBehaviour
{
    #region INSPECTOR

    public Text saveTitle;

    public float fadeSpeed = 1f;
    #endregion

    private bool isInvert = false;

    public void Update()
    {
        if(isInvert)
        {
            saveTitle.color = Utill.ChangeMinusAlpha(saveTitle.color,fadeSpeed * Time.deltaTime);
            if(saveTitle.color.a <= 0.35f)
                isInvert = false;
        }
        else
        {
            saveTitle.color = Utill.ChangePlusAlpha(saveTitle.color, fadeSpeed * Time.deltaTime);
            if (saveTitle.color.a >= 1f)
                isInvert = true;
        }
    }
}
