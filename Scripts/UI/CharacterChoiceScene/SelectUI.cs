﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectUI : MonoBehaviour
{
    #region INSPECTOR

    // 슬롯들
    public List<CharacterSlotUI> slots = new List<CharacterSlotUI>();
    // 스페이스바 이미지
    public GameObject spaceDescription;

    #endregion

    // 초기 시작 때만 SlotHover 예외 없이 실행되게하기위한 플래그
    [HideInInspector]
    public bool isFirstTime = false;

    // 플레이 할 캐릭터를 정하고도 슬롯이 Hover 되는 예외를 막기 위한 Flag;
    [HideInInspector]
    public bool isSceneOut = false;

    // 캐릭터 데이터 지우기
    private void DeleteData()
    {
        int selectedIndex = UserDataMng.Ins.selectedIndex;
        UserDataMng.Ins.DeleteUserData();
        slots[selectedIndex].ChangeUIActive();
        CharacterWindowMng.Ins.SetWindowCharacter(UserDataMng.Ins.characterSlot[selectedIndex]);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1) && !isSceneOut)
        {
            if (!UIMng.Ins.CheckPopUpActive())
            {
                SoundMng.Ins.ExternalSoundPlay(eSound.IntroClickSound.ToDesc(), eChannel.UI_Effect);
                StartCoroutine(MoveToIntroScene());
            }
        }
        
        if(Input.GetKeyDown(KeyCode.Space))
        {   
            if (UserDataMng.Ins.characterSlot[UserDataMng.Ins.selectedIndex] != null)
                InstantiateRemovePopUp();
        }
    }

    // 캐릭터 제거 팝업
    private void InstantiateRemovePopUp()
    {
        UIMng.Ins.twoButtonPopUp.SetPopUp("정말 해당 저장 파일을 제거하시겠습니까?", DeleteData, UIMng.Ins.OnPopupExit, "예", "아니요");
    }

    // 사운드 호출하고 넘기기 위해서 코루틴화 시킴
    IEnumerator MoveToIntroScene()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.IntroClickSound.ToDesc(), eChannel.UI_Effect);
        yield return new WaitForSeconds(0.2f);
        Destroy(GameObject.Find("SoundMng"));
        Destroy(GameObject.Find("UIMng"));
        Destroy(GameObject.Find("TableMng"));
        Destroy(GameObject.Find("EquipmentMng"));
        Destroy(GameObject.Find("AtlasMng"));

        SceneMng.Ins.ChangeScene(eScene.Intro);
    }
}