﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterDataUI : MonoBehaviour
{
    public Text characterName;
    public Text gender;
    public Text hunterRank;
    public Text money;
    
    // 캐릭터 정보 세팅
    public void SetCharacterInfo(UserData userData)
    {
        characterName.text = userData.characterName;
        switch(userData.gender)
        {
            case eGender.Male:
                gender.text = "남자";
                break;
            case eGender.Female:
                gender.text = "여자";
                break;
        }
        money.text = userData.money.ToString();
        hunterRank.text = userData.hunterRank.ToString();
    }
}
