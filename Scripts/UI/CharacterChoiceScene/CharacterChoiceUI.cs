﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterChoiceUI : MonoBehaviour
{
    #region INSPECTOR

    // 플레이할 캐릭터 고르는 UI씬
    public SelectUI     selectSceneUI;
    // 캐릭터 생성 UI씬
    public CreateUI     createSceneUI;
    // Select Your Hunter & Create Your Hunter
    public Text         title;

    public GameObject   blackBoard;
    public GameObject   objectCamera;

    #endregion

    [HideInInspector]
    public GameObject   prevHover;

    private void Awake()
    {
        UIMng.Ins.characterChoice = this;
    }

    public void HoverChange(GameObject hoverOverlay)
    {
        if (prevHover != null && prevHover != hoverOverlay)
            prevHover.SetActive(false);
        hoverOverlay.SetActive(true);
        prevHover = hoverOverlay;
    }
}
