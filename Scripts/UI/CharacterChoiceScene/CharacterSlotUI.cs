﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSlotUI : HoverUI
{
    #region INSPECTOR
    
    public int slotIndex = 0;               // 몇번째 슬롯인지 인덱스
    
    // [ 슬롯 내 UI 변수들 ]
    public GameObject       newGameText;    // 새 게임
    public CharacterDataUI  dataUI;         // 이미 존재하는 정보

    #endregion

    private CharacterChoiceUI   characterChoiceUI;
    private SelectUI            selectUI;
    private UserData[]          userDataArray;

    private void Awake()
    {
        characterChoiceUI = UIMng.Ins.characterChoice;
        selectUI = characterChoiceUI.selectSceneUI;
        userDataArray = UserDataMng.Ins.characterSlot;
    }

    private void OnEnable()
    {
        UserData tempData = userDataArray[slotIndex];
        if(tempData == null)
        {
            newGameText.SetActive(true);
            dataUI.gameObject.SetActive(false);
        }
        else
        {
            newGameText.SetActive(false);
            dataUI.SetCharacterInfo(tempData);
            dataUI.gameObject.SetActive(true);
        }

        if (slotIndex == 0) OnHoverEnter();
    }
    
    public override void OnHoverEnter()
    {

        // 중복 Enter 예외처리
        if (UIMng.Ins.characterChoice.prevHover == hoverOverlay ||UserDataMng.Ins.selectedIndex == slotIndex && selectUI.isFirstTime || selectUI.isSceneOut) return;

        SoundMng.Ins.ExternalSoundPlay(eSound.CharacterSlotHoverSound.ToDesc(), eChannel.UI_Effect);
        // 선택한 슬롯 변경
        UserDataMng.Ins.selectedIndex = slotIndex;
        characterChoiceUI.HoverChange(hoverOverlay);

        // 데이터에 해당하는 캐릭터 그려주기
        CharacterWindowMng.Ins.SetWindowCharacter(userDataArray[slotIndex]);

        // Description 끄고 키기
        if (userDataArray[slotIndex] != null)
            selectUI.spaceDescription.SetActive(true);
        else
            selectUI.spaceDescription.SetActive(false);
    }
    
    public override void OnClickBtn()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.CharacterSlotClickSound.ToDesc(), eChannel.UI_Effect);
        if (userDataArray[slotIndex] == null)
            InstantiateCreatePopUp();
        else
            InstantiateSelectPopUp();
    }

    //새게임과 UserData 오브젝트 교환 함수
    public void ChangeUIActive()
    {
        newGameText.SetActive(!newGameText.activeSelf);
        dataUI.gameObject.SetActive(!dataUI.gameObject.activeSelf);
        selectUI.spaceDescription.SetActive(false);
    }
    

    #region PopUp ButtonCallBack

    // 캐릭터 생성 창 띄우기
    private void LoadCharacterCreateScene()
    {
        selectUI.gameObject.SetActive(false);
        characterChoiceUI.createSceneUI.gameObject.SetActive(true);
        characterChoiceUI.title.text = "Create Your Hunter";
    }

    // 로비씬으로 넘어가기
    private void LoadLobbyScene()
    {
        UserDataMng.Ins.nowCharacter = userDataArray[slotIndex];
        selectUI.isSceneOut = true;
        characterChoiceUI.objectCamera.SetActive(false);
        characterChoiceUI.blackBoard.SetActive(true);
        SceneMng.Ins.ChangeScene(eScene.Lobby);
    }

    #endregion


    // 캐릭터 생성 팝업
    private void InstantiateCreatePopUp()
    {
        UIMng.Ins.twoButtonPopUp.SetPopUp("새로운 캐릭터를 생성하시겠습니까?", LoadCharacterCreateScene, UIMng.Ins.OnPopupExit, "예", "아니요");
    }

    // 캐릭터 선택 팝업
    private void InstantiateSelectPopUp()
    {
        UIMng.Ins.twoButtonPopUp.SetPopUp("해당 캐릭터로 시작하시겠습니까?", LoadLobbyScene, UIMng.Ins.OnPopupExit, "예", "아니요");
    }
}
