﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateUI : MonoBehaviour
{
    #region INSPECTOR
    
    public InputField   nameField;
    // 네임 필드 알람 메시지
    public Text         inputFieldMessage;
    // 남자 토글 (초기화를 위함 )
    public Toggle       maleToggle;

    #endregion
    
    // 새로 만드는 캐릭터의 데이터
    private UserData            createCharacterInfo = null;
    private CharacterChoiceUI   characterChoiceUI;

    private void Awake()
    {
        characterChoiceUI = UIMng.Ins.characterChoice;
    }

    private void OnEnable()
    {
        createCharacterInfo = new UserData();
        Init();
    }

    private void OnDisable()
    {
        createCharacterInfo = null;
    }

    // 초기화 함수
    public void Init()
    {
        nameField.text          = "";
        inputFieldMessage.text  = "";
        maleToggle.isOn         = true;
        CharacterWindowMng.Ins.SetWindowCharacter(createCharacterInfo);
    }

    // 성별 세팅
    public void SetGender(Toggle toggle)
    {
        if (toggle.isOn)
        {
            SoundMng.Ins.ExternalSoundPlay(eSound.BtnClickSound.ToDesc(), eChannel.UI_Effect);
            createCharacterInfo.gender = (eGender)Enum.Parse(typeof(eGender), toggle.name);
            CharacterWindowMng.Ins.SetWindowCharacter(createCharacterInfo);
        }
    }

    #region Button Func
    
    public void OnReturnToSelectScene()
    {
        gameObject.SetActive(false);
        characterChoiceUI.selectSceneUI.gameObject.SetActive(true);
        characterChoiceUI.title.text = "Select Your Hunter";
    }
    
    public void OnBtnCreate()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.BtnClickSound.ToDesc(), eChannel.UI_Effect);
        
        if (string.IsNullOrEmpty(nameField.text))
            inputFieldMessage.text = "닉네임을 입력해주세요.";
        else
        {
            inputFieldMessage.text = "";
            CreateCharacter();
        }
    }

    #endregion

    //캐릭터 데이터 생성
    private void CreateCharacter()
    {
        // 나머지 정보는 이미 있음
        createCharacterInfo.characterName = nameField.text;
        UserDataMng.Ins.SaveUserData(createCharacterInfo);
        UserDataMng.Ins.characterSlot[UserDataMng.Ins.selectedIndex] = createCharacterInfo;
        gameObject.SetActive(false);
        characterChoiceUI.selectSceneUI.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            //팝업이 안켜져있을 때 오른쪽 버튼을 누르면 팝업생성
            if(!UIMng.Ins.CheckPopUpActive())
                UIMng.Ins.twoButtonPopUp.SetPopUp("캐릭터 선택 창으로 돌아갑니다.\n 그렇게 하시겠습니까?", OnReturnToSelectScene, UIMng.Ins.OnPopupExit, "예", "아니요");
        }
    }
}
