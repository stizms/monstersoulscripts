﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlarmHud : MonoBehaviour
{
    #region INSPECTOR

    public Transform    content;
    public GameObject   itemAlarmPrefab;
    public GameObject   questAlarmPrefab;
    public GameObject   mentionAlarmPrefab;
    public float        alarmDisplayTime = 1.5f;

    #endregion


    // 퀘스트 진행 과정마다 나오는 알람
    public void AddQuestAlarm(QuestGoal goal)
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.AlarmSound.ToDesc(), eChannel.UI_Effect);
        AlarmSlot tempAlarm = Instantiate(questAlarmPrefab, content).GetComponent<AlarmSlot>();
        tempAlarm.SetQuestAlarmData(goal);
        Destroy(tempAlarm.gameObject, alarmDisplayTime);
    }

    // 아이템 먹을 때 나오는 알람
    public void AddItemAlarm(eItemID itemID, int amount)
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.AlarmSound.ToDesc(), eChannel.UI_Effect);
        AlarmSlot tempAlarm = Instantiate(itemAlarmPrefab, content).GetComponent<AlarmSlot>();
        tempAlarm.SetItemAlarmData(itemID,amount);
        Destroy(tempAlarm.gameObject, alarmDisplayTime);
    }

    public void AddMentionAlarm(string str)
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.AlarmSound.ToDesc(), eChannel.UI_Effect);
        Transform tempAlarm = Instantiate(mentionAlarmPrefab, content).GetComponent<Transform>();
        tempAlarm.GetComponentInChildren<Text>().text = str;
        Destroy(tempAlarm.gameObject, alarmDisplayTime);
    }

    // 리셋
    public void ResetHud()
    {
        foreach(Transform child in content)
        {
            if (transform == content) continue;
            Destroy(child.gameObject);
        }
    }
}