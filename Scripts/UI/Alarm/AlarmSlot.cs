﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlarmSlot : MonoBehaviour
{
    #region INSPECTOR

    public Image        contentIcon;
    public Text         contentName;
    public Text         amountText;
    public GameObject   maxIcon;  // 이것은 Item에서만 사용함
    // 이것은 quest에서만 사용함
    public Text         requiredAmountText;

    #endregion

    public void SetQuestAlarmData(QuestGoal goal)
    {
        switch(goal.goalType)
        {
            case eGoalType.HuntGoal:
                MonsterData monster     = TableMng.Ins.monsterTb[goal.targetMonster];
                contentIcon.sprite      = AtlasMng.Ins.GetSprite(eAtlasCategory.Monster, monster.monsterID.ToString());
                contentName.text        = monster.monsterName;
                amountText.text         = goal.currentAmount.ToString();
                requiredAmountText.text = string.Format("/{0}",goal.requiredAmount.ToString());
                break;
            case eGoalType.CollectionGoal:
                ItemData item           = TableMng.Ins.itemTb[goal.targetCollection];
                contentIcon.sprite      = AtlasMng.Ins.GetSprite(eAtlasCategory.Item, item.itemID.ToString());
                contentName.text        = item.itemName;
                amountText.text         = goal.currentAmount.ToString();
                requiredAmountText.text = string.Format("/{0}", goal.requiredAmount.ToString());
                break;
        }
    }


    // 아이템 알람 정보 세팅
    public void SetItemAlarmData(eItemID itemID, int amount)
    {
        ItemData item       = TableMng.Ins.itemTb[itemID];
        contentIcon.sprite  = AtlasMng.Ins.GetSprite(eAtlasCategory.Item, item.itemID.ToString());
        contentName.text    = item.itemName;
        amountText.text     = amount.ToString();
        if (UserDataMng.Ins.nowCharacter.characterInventory.GetItemCount(itemID) >= TableMng.Ins.itemTb[itemID].characterLimit)
            maxIcon.SetActive(true);
        else
            maxIcon.SetActive(false);
    }
}
