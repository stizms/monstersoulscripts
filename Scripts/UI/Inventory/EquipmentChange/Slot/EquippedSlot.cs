﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquippedSlot : EquipmentSlot
{
    #region INSPECTOR
    
    public eEquipmentCategory slotCategory;

    #endregion

    private ItemBoxUI           itemBoxUI;
    private EquipmentChangeUI   equipmentChangeUI;
    private bool isFirstTime = true;

    public override void SetData(eEquipment equipmentID)
    {
        slotEquipment = equipmentID;

        if (slotEquipment == eEquipment.None)
        {
            equipmentIcon.gameObject.SetActive(false);
            equipmentName.text = "";
            return;
        }
        else if (slotEquipment > eEquipment.Weapon && slotEquipment < eEquipment.Helmet)
        {
            WeaponData tempWeapon   = TableMng.Ins.weaponTb[slotEquipment];
            equipmentIcon.sprite    = AtlasMng.Ins.GetSprite(eAtlasCategory.Equipment, tempWeapon.weaponCategory.ToString());
            equipmentName.text      = tempWeapon.weaponName;
        }
        else
        {
            EquipmentData tempEquipment = TableMng.Ins.equipmentTb[equipmentID];
            equipmentIcon.sprite        = AtlasMng.Ins.GetSprite(eAtlasCategory.Equipment, tempEquipment.equipmentCategory.ToString());
            equipmentName.text          = tempEquipment.equipmentName;
        }
        equipmentIcon.gameObject.SetActive(true);
    }

    public override void OnClickBtn()
    {
        equipmentChangeUI.firstSlot = this;
        equipmentChangeUI.weaponAreaUI.blackBoard.SetActive(true);
        equipmentChangeUI.equippedAreaUI.blackBoard.SetActive(true);
        equipmentChangeUI.equipmentBoxUI.blackBoard.SetActive(false);
        equipmentChangeUI.weaponAreaUI.description.SetActive(false);
        equipmentChangeUI.equipmentBoxUI.description.SetActive(true);
        equipmentChangeUI.equipmentBoxUI.EqualSlotHover(slotEquipment);
    }

    public override void OnHoverEnter()
    {
        if(isFirstTime)
        {
            itemBoxUI = UIMng.Ins.lobby.itemBoxUI;
            equipmentChangeUI = itemBoxUI.mainItemBoxUI.equipmentChangeUI;
            isFirstTime = false;
        }

        if (UIMng.Ins.lobby.prevHover == hoverOverlay) return;

        SoundMng.Ins.ExternalSoundPlay(eSound.PopUpHoverSound.ToDesc(), eChannel.UI_Effect);
        UIMng.Ins.lobby.HoverChange(hoverOverlay);

        equipmentChangeUI.equipmentBoxUI.SetSlotData(slotCategory);
        equipmentChangeUI.equipmentInfoUI.SetData(slotEquipment);
        switch (slotCategory)
        {
            case eEquipmentCategory.Weapon:
                itemBoxUI.ChangeNoticeText("무기를 변경합니다.");
                break;
            case eEquipmentCategory.Helmet:
                itemBoxUI.ChangeNoticeText("머리 방어구를 변경합니다.");
                break;
            case eEquipmentCategory.Vest:
                itemBoxUI.ChangeNoticeText("몸통 방어구를 변경합니다.");
                break;
            case eEquipmentCategory.Glove:
                itemBoxUI.ChangeNoticeText("팔 방어구를 변경합니다.");
                break;
            case eEquipmentCategory.Belt:
                itemBoxUI.ChangeNoticeText("허리 방어구를 변경합니다.");
                break;
            case eEquipmentCategory.Pants:
                itemBoxUI.ChangeNoticeText("다리 방어구를 변경합니다.");
                break;
        }
    }
}
