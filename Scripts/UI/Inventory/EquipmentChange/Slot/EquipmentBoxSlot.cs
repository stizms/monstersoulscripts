﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentBoxSlot : EquipmentSlot
{
    // 캐싱
    private UserData            tempUserData;
    private ItemBoxUI           itemBoxUI;
    private EquipmentChangeUI   equipmentChangeUI;
    private bool isFirstTime = true;

    public override void SetData(eEquipment equipmentID)
    {
        if(tempUserData == null) tempUserData = UserDataMng.Ins.nowCharacter;
        
        ResetSlot();
        slotEquipment = equipmentID;

        if (slotEquipment == eEquipment.None) return;
        else if (slotEquipment > eEquipment.Weapon && slotEquipment < eEquipment.Helmet)
        {
            WeaponData tempWeapon = TableMng.Ins.weaponTb[slotEquipment];
            equipmentIcon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Equipment, tempWeapon.weaponCategory.ToString());
            if (tempUserData.equipWeapon == tempWeapon.weaponID) 
                equippedSign.SetActive(true);
        }
        else
        {
            EquipmentData tempEquipment = TableMng.Ins.equipmentTb[equipmentID];
            equipmentIcon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Equipment, tempEquipment.equipmentCategory.ToString());
            
            // 헬멧
            if (slotEquipment > eEquipment.Helmet && slotEquipment < eEquipment.Vest)
            {
                if (tempUserData.equipHelmet == slotEquipment)
                    equippedSign.SetActive(true);
            }
            // 베스트
            else if(slotEquipment > eEquipment.Vest && slotEquipment < eEquipment.Glove)
            {
                if (tempUserData.equipVest == slotEquipment)
                    equippedSign.SetActive(true);
            }
            // 글로브
            else if(slotEquipment > eEquipment.Glove && slotEquipment < eEquipment.Belt)
            {
                if (tempUserData.equipGlove == slotEquipment)
                    equippedSign.SetActive(true);
            }
            // 벨트
            else if(slotEquipment > eEquipment.Belt && slotEquipment < eEquipment.Pants)
            {
                if (tempUserData.equipBelt == slotEquipment)
                    equippedSign.SetActive(true);
            }
            // 팬츠
            else if(slotEquipment > eEquipment.Pants)
            {
                if (tempUserData.equipPants == slotEquipment)
                    equippedSign.SetActive(true);
            }
        }
        equipmentIcon.gameObject.SetActive(true);
    }

    public override void OnClickBtn()
    {
        if (slotEquipment == eEquipment.None) return;

        SoundMng.Ins.ExternalSoundPlay(eSound.EquipmentChange.ToDesc(), eChannel.UI_Effect);

        equipmentChangeUI.secondSlot = this;
        // 장비 해제
        if(equipmentChangeUI.firstSlot.slotEquipment == slotEquipment)
        {
            switch(equipmentChangeUI.firstSlot.slotCategory)
            {
                case eEquipmentCategory.Weapon:
                    return;
                case eEquipmentCategory.Helmet:
                    equipmentChangeUI.character.UnEquipHelmet();
                    break;
                case eEquipmentCategory.Vest:
                    equipmentChangeUI.character.UnEquipVest();
                    break;
                case eEquipmentCategory.Glove:
                    equipmentChangeUI.character.UnEquipGlove();
                    break;
                case eEquipmentCategory.Belt:
                    equipmentChangeUI.character.UnEquipBelt();
                    break;
                case eEquipmentCategory.Pants:
                    equipmentChangeUI.character.UnEquipPants();
                    break;
            }
            equipmentChangeUI.firstSlot.SetData(eEquipment.None);
            equippedSign.SetActive(false);
        }
        // 장비 장착 or 교체
        else
        {
            switch (equipmentChangeUI.firstSlot.slotCategory)
            {
                case eEquipmentCategory.Weapon:
                    equipmentChangeUI.character.EquipWeapon(slotEquipment);
                    break;
                case eEquipmentCategory.Helmet:
                    equipmentChangeUI.character.EquipHelmet(slotEquipment);
                    break;
                case eEquipmentCategory.Vest:
                    equipmentChangeUI.character.EquipVest(slotEquipment);
                    break;
                case eEquipmentCategory.Glove:
                    equipmentChangeUI.character.EquipGlove(slotEquipment);
                    break;
                case eEquipmentCategory.Belt:
                    equipmentChangeUI.character.EquipBelt(slotEquipment);
                    break;
                case eEquipmentCategory.Pants:
                    equipmentChangeUI.character.EquipPants(slotEquipment);
                    break;
            }

            if (equipmentChangeUI.firstSlot.slotEquipment == eEquipment.None)
            {
                // 무조건 장착
                equipmentChangeUI.firstSlot.SetData(slotEquipment);
                equippedSign.SetActive(true);
            }
            else
            {
                equipmentChangeUI.equipmentBoxUI.TurnOffPrevEquipSign(equipmentChangeUI.firstSlot.slotEquipment);
                equipmentChangeUI.firstSlot.SetData(slotEquipment);
                equippedSign.SetActive(true);
            }
        }

        // 캐릭터 정보 갱신
        equipmentChangeUI.characterStatusUI.SetStatus();

        equipmentChangeUI.weaponAreaUI.blackBoard.SetActive(false);
        equipmentChangeUI.equippedAreaUI.blackBoard.SetActive(false);
        equipmentChangeUI.equipmentBoxUI.blackBoard.SetActive(true);
        equipmentChangeUI.firstSlot.OnHoverEnter();
        equipmentChangeUI.firstSlot = null;
        equipmentChangeUI.secondSlot = null;
    }

    public override void OnHoverEnter()
    {
        if(isFirstTime)
        {
            itemBoxUI           = UIMng.Ins.lobby.itemBoxUI;
            equipmentChangeUI   = itemBoxUI.mainItemBoxUI.equipmentChangeUI;
            isFirstTime         = false;
        }

        if (UIMng.Ins.lobby.prevHover == hoverOverlay) return;

        SoundMng.Ins.ExternalSoundPlay(eSound.PopUpHoverSound.ToDesc(), eChannel.UI_Effect);
        UIMng.Ins.lobby.HoverChange(hoverOverlay);

        equipmentChangeUI.equipmentInfoUI.SetData(slotEquipment);
        itemBoxUI.ChangeNoticeText("변경할 장비를 선택하십시오.");

        if (slotEquipment > eEquipment.Weapon && slotEquipment < eEquipment.Helmet)
        {
            // 무기
            if(tempUserData.equipWeapon == slotEquipment)
                itemBoxUI.ChangeNoticeText("변경할 장비를 선택하십시오.\n<color=red>(무기는 해체할 수 없습니다.)</color>");
        }
        else if (slotEquipment > eEquipment.Helmet && slotEquipment < eEquipment.Vest)
        {
            // 머리
            if (tempUserData.equipHelmet == slotEquipment)
                itemBoxUI.ChangeNoticeText("변경할 장비를 선택하십시오.\n<color=yellow>(장비 중인 항목을 선택하면 장비를 해체합니다.)</color>");
        }
        else if (slotEquipment > eEquipment.Vest && slotEquipment < eEquipment.Glove)
        {
            // 조끼
            if (tempUserData.equipVest == slotEquipment)
                itemBoxUI.ChangeNoticeText("변경할 장비를 선택하십시오.\n<color=yellow>(장비 중인 항목을 선택하면 장비를 해체합니다.)</color>");
        }
        else if (slotEquipment > eEquipment.Glove && slotEquipment < eEquipment.Belt)
        {
            // 팔
            if (tempUserData.equipGlove == slotEquipment)
                itemBoxUI.ChangeNoticeText("변경할 장비를 선택하십시오.\n<color=yellow>(장비 중인 항목을 선택하면 장비를 해체합니다.)</color>");
        }
        else if (slotEquipment > eEquipment.Belt && slotEquipment < eEquipment.Pants)
        {
            // 허리
            if (tempUserData.equipBelt == slotEquipment)
                itemBoxUI.ChangeNoticeText("변경할 장비를 선택하십시오.\n<color=yellow>(장비 중인 항목을 선택하면 장비를 해체합니다.)</color>");
        }
        else if (slotEquipment > eEquipment.Pants)
        {
            // 다리
            if (tempUserData.equipPants == slotEquipment)
                itemBoxUI.ChangeNoticeText("변경할 장비를 선택하십시오.\n<color=yellow>(장비 중인 항목을 선택하면 장비를 해체합니다.)</color>");
        }
    }
}
