﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EquipmentSlot : HoverUI
{
    #region INSPECTOR

    public Image        equipmentIcon;
    public Text         equipmentName;
    public GameObject   equippedSign;

    #endregion

    public eEquipment slotEquipment;

    public virtual void SetData(eEquipment equipmentID) { }
    
    public override void OnHoverEnter()
    {
    }

    public override void OnClickBtn()
    {
    }

    public void ResetSlot()
    {
        slotEquipment = eEquipment.None;
        equipmentIcon.gameObject.SetActive(false);
        if (equipmentName != null)      equipmentName.text = "";
        if (equippedSign != null)       equippedSign.SetActive(false);
    }
}