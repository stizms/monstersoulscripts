﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentChangeUI : MonoBehaviour
{
    #region INSPECTOR

    public WeaponAreaUI      weaponAreaUI;
    public EquipmentAreaUI   equippedAreaUI;
    public EquipmentBoxUI    equipmentBoxUI;
    public EquipmentInfoUI   equipmentInfoUI;
    public CharacterStatusUI characterStatusUI;
    #endregion

    [HideInInspector]
    public EquippedSlot firstSlot;
    [HideInInspector]
    public EquipmentBoxSlot secondSlot;

    public Character character;

    public void VisualizeUI()
    {
        gameObject.SetActive(true);
        UIMng.Ins.lobby.itemBoxUI.titleText.text = "아이템 BOX ▶ 장비 변경";
        UIMng.Ins.lobby.itemBoxUI.ChangeNoticeText("무기를 변경합니다.");
        weaponAreaUI.Init();
        equippedAreaUI.Init();
        characterStatusUI.SetStatus();
        GameMng.Ins.CursorReset();
    }

    public void ExitUI()
    {
        // 하나 클릭 된 상태
        if(firstSlot != null)
        {
            weaponAreaUI.blackBoard.SetActive(false);
            weaponAreaUI.description.SetActive(true);
            equippedAreaUI.blackBoard.SetActive(false);
            equipmentBoxUI.blackBoard.SetActive(true);
            equipmentBoxUI.description.SetActive(false);
            firstSlot.OnHoverEnter();
            firstSlot = null;
        }
        // 하나도 클릭 안 된 상태
        else
        {
            UIMng.Ins.lobby.itemBoxUI.titleText.text = "아이템 BOX";
            gameObject.SetActive(false);
            weaponAreaUI.ResetSlotList();
            weaponAreaUI.description.SetActive(false);
            equippedAreaUI.ResetSlotList();
            equipmentBoxUI.ResetSlotList();
            equipmentBoxUI.description.SetActive(false);
            equipmentInfoUI.ResetInfo();
            firstSlot = null;
            secondSlot = null;
            UIMng.Ins.lobby.itemBoxUI.mainItemBoxUI.VisualizeMainUI();
            GameMng.Ins.CursorReset();
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            ExitUI();
        }
    }
}
