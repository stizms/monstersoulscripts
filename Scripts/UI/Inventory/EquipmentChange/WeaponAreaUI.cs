﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponAreaUI : MonoBehaviour
{
    #region INSPECTOR

    public EquippedSlot equippedWeaponSlot;
    public GameObject   blackBoard;
    public GameObject   description;

    #endregion

    public void Init()
    {
        equippedWeaponSlot.SetData(UserDataMng.Ins.nowCharacter.equipWeapon);
        description.SetActive(true);
        equippedWeaponSlot.OnHoverEnter();
    }

    public void ResetSlotList()
    {
        equippedWeaponSlot.ResetSlot();
    }
}
