﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStatusUI : MonoBehaviour
{
    #region INSPECTOR

    public Text     hunterRankText;
    public Text     hunterHpText;
    public Text     hunterStaminaText;

    // 공격 스테이터스
    public Text     damageText;
    public Text     elementText;
    public Image    sharpnessGaze;

    // 방어 스테이터스
    public Text     defenseText;
    public Text     fireToleranceText;
    public Text     waterToleranceText;
    public Text     thunderToleranceText;
    public Text     iceToleranceText;
    
    #endregion

    // 옵션 UI에서만 사용
    Character character;
    
    public void SetStatus()
    {
        if(character == null)
        {
            if (SceneMng.Ins.nowScene == eScene.Lobby)
                character = UIMng.Ins.lobby.lobbyScene.player;
            else if(SceneMng.Ins.nowScene > eScene.Dungeon)
                character = DungeonMng.Ins.dungeonScene.player;
        }
        
        // 옵션 UI에서만 사용되는 부분 ( 
        if(hunterRankText != null)
        {
            hunterRankText.text     = character.characterInfo.hunterRank.ToString();
            hunterHpText.text       = character.currentMaxHp.ToString();
            hunterStaminaText.text  = character.currentMaxStamina.ToString();
        }

        // 범용적인 부분
        // 공격 스테이터스
        BaseWeapon equipWeapon      = character.equipWeapon;
        damageText.text             = equipWeapon.originDamage.ToString();
        sharpnessGaze.fillAmount    = equipWeapon.MAX_Sharpness / Define.Total_Sharpness;
        elementText.text            = equipWeapon.element.ToDesc();

        // 방어 스테이터스
        defenseText.text            = character.defense.ToString();
        fireToleranceText.text      = character.fireTolerance.ToString();
        waterToleranceText.text     = character.waterTolerance.ToString();
        thunderToleranceText.text   = character.thunderTolerance.ToString();
        iceToleranceText.text       = character.iceTolerance.ToString();
    }
}
