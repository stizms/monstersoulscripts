﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentInfoUI : MonoBehaviour
{
    #region INSPECTOR

    // 옵션 장비 확인 UI에서만 사용 됨
    public Image    equipmentCategoryIcon;
    // 범용적으로 사용
    public Text     equipmentName;
    public Text     rareText;
    // 무기
    public Text     damageText;
    public Image    sharpnessGaze;
    public Text     elementText;
    // 방어구
    public Text     defenseText;
    public Text     fireToleranceText;
    public Text     waterToleranceText;
    public Text     thunderToleranceText;
    public Text     iceToleranceText;

    public GameObject weaponInfo;
    public GameObject equipmentInfo;

    #endregion


    // 인포 세팅
    public void SetData(eEquipment equipmentID)
    {
        ResetInfo();

        if (equipmentID == eEquipment.None) return;
        else if(equipmentID > eEquipment.Weapon && equipmentID < eEquipment.Helmet)
        {
            // 무기
            WeaponData weaponData= TableMng.Ins.weaponTb[equipmentID];
            if (equipmentCategoryIcon != null)
            {
                equipmentCategoryIcon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Equipment, weaponData.weaponCategory.ToString());
                equipmentCategoryIcon.gameObject.SetActive(true);
            }
            rareText.text               = weaponData.rare.ToString();
            damageText.text             = weaponData.damage.ToString();
            elementText.text            = weaponData.element.ToDesc();
            equipmentName.text          = weaponData.weaponName;
            sharpnessGaze.fillAmount    = weaponData.sharpness / Define.Total_Sharpness;
            weaponInfo.SetActive(true);
        }
        else
        {
            // 방어구
            EquipmentData equipmentData = TableMng.Ins.equipmentTb[equipmentID];
            if (equipmentCategoryIcon != null)
            {
                equipmentCategoryIcon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Equipment, equipmentData.equipmentCategory.ToString());
                equipmentCategoryIcon.gameObject.SetActive(true);
            }
            rareText.text               = equipmentData.rare.ToString();
            defenseText.text            = equipmentData.def.ToString();
            equipmentName.text          = equipmentData.equipmentName;
            iceToleranceText.text       = equipmentData.iceTolerance.ToString();
            fireToleranceText.text      = equipmentData.fireTolerance.ToString();
            waterToleranceText.text     = equipmentData.waterTolerance.ToString();
            thunderToleranceText.text   = equipmentData.thunderTolerance.ToString();
            equipmentInfo.SetActive(true);
        }

    }
    
    public void ResetInfo()
    {
        if (equipmentCategoryIcon != null) equipmentCategoryIcon.gameObject.SetActive(false);

        rareText.text               = "";
        damageText.text             = "";
        elementText.text            = "";
        defenseText.text            = "";
        equipmentName.text          = "";
        sharpnessGaze.fillAmount    = 0f;
        weaponInfo.SetActive(false);
        equipmentInfo.SetActive(false);
    }
}