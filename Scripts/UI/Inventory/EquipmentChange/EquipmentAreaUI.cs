﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentAreaUI : MonoBehaviour
{
    #region INSPECTOR

    public EquippedSlot equippedHelmetSlot;
    public EquippedSlot equippedVestSlot;
    public EquippedSlot equippedGloveSlot;
    public EquippedSlot equippedBeltSlot;
    public EquippedSlot equippedPantsSlot;

    public GameObject blackBoard;
    #endregion

    public void Init()
    {
        UserData tempUserData = UserDataMng.Ins.nowCharacter;

        equippedHelmetSlot.SetData(tempUserData.equipHelmet);
        equippedVestSlot.SetData(tempUserData.equipVest);
        equippedGloveSlot.SetData(tempUserData.equipGlove);
        equippedBeltSlot.SetData(tempUserData.equipBelt);
        equippedPantsSlot.SetData(tempUserData.equipPants);
    }

    public void ResetSlotList()
    {
        equippedHelmetSlot.ResetSlot();
        equippedVestSlot.ResetSlot();
        equippedGloveSlot.ResetSlot();
        equippedBeltSlot.ResetSlot();
        equippedPantsSlot.ResetSlot();
    }
}
