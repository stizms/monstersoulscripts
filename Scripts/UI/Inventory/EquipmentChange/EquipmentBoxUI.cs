﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentBoxUI : MonoBehaviour
{
    #region INSPECTOR

    public GameObject           blackBoard;
    public GameObject           description;
    public List<EquipmentSlot>  equipmentBoxSlots = new List<EquipmentSlot>();

    #endregion

    private UserData tempUserData;
    
    // 장비 확인/판매에서 사용
    public void SetSlotData()
    {
        if(tempUserData == null) tempUserData = UserDataMng.Ins.nowCharacter;

        int slotIndex = 0;

        ResetSlotList();
        
        foreach(var node in tempUserData.itemBoxInventory.helmetItemList)
        {
            equipmentBoxSlots[slotIndex].SetData(node.Key);
            slotIndex++;
        }
        foreach (var node in tempUserData.itemBoxInventory.vestItemList)
        {
            equipmentBoxSlots[slotIndex].SetData(node.Key);
            slotIndex++;
        }
        foreach (var node in tempUserData.itemBoxInventory.gloveItemList)
        {
            equipmentBoxSlots[slotIndex].SetData(node.Key);
            slotIndex++;
        }
        foreach (var node in tempUserData.itemBoxInventory.beltItemList)
        {
            equipmentBoxSlots[slotIndex].SetData(node.Key);
            slotIndex++;
        }
        foreach (var node in tempUserData.itemBoxInventory.pantsItemList)
        {
            equipmentBoxSlots[slotIndex].SetData(node.Key);
            slotIndex++;
        }
        foreach (var node in tempUserData.itemBoxInventory.weaponItemList)
        {
            equipmentBoxSlots[slotIndex].SetData(node.Key);
            slotIndex++;
        }

        equipmentBoxSlots[0].OnHoverEnter();
    }
    
    // 장비 교체 부분에서 사용
    public void SetSlotData(eEquipmentCategory equipmentCategory)
    {
        if (tempUserData == null) tempUserData = UserDataMng.Ins.nowCharacter;

        int slotIndex = 0;
        Dictionary<eEquipment, List<EquipmentData>> tempEquipmentMap = null;

        ResetSlotList();

        switch(equipmentCategory)
        {
            case eEquipmentCategory.Weapon:
                foreach(var node in tempUserData.itemBoxInventory.weaponItemList)
                {
                    equipmentBoxSlots[slotIndex].SetData(node.Key);
                    slotIndex++;
                }
                return;
            case eEquipmentCategory.Helmet:
                tempEquipmentMap = tempUserData.itemBoxInventory.helmetItemList;
                break;
            case eEquipmentCategory.Vest:
                tempEquipmentMap = tempUserData.itemBoxInventory.vestItemList;
                break;
            case eEquipmentCategory.Glove:
                tempEquipmentMap = tempUserData.itemBoxInventory.gloveItemList;
                break;
            case eEquipmentCategory.Belt:
                tempEquipmentMap = tempUserData.itemBoxInventory.beltItemList;
                break;
            case eEquipmentCategory.Pants:
                tempEquipmentMap = tempUserData.itemBoxInventory.pantsItemList;
                break;
        }

        foreach( var node in tempEquipmentMap)
        {
            equipmentBoxSlots[slotIndex].SetData(node.Key);
            slotIndex++;
        }
    }

    public void EqualSlotHover(eEquipment equipmentID)
    {
        if (equipmentID == eEquipment.None) equipmentBoxSlots[0].OnHoverEnter();
        else
        {
            foreach (var node in equipmentBoxSlots)
            {
                if (node.slotEquipment == equipmentID)
                {
                    node.OnHoverEnter();
                    return;
                }
            }
        }
    }

    // 화면 갱신
    public void TurnOffPrevEquipSign(eEquipment equipmentID)
    {
        foreach (var node in equipmentBoxSlots)
        {
            if (node.slotEquipment == equipmentID)
            {
                node.OnHoverEnter();
                return;
            }
        }
    }

    public void ResetSlotList()
    {
        foreach(var node in equipmentBoxSlots)
        {
            node.ResetSlot();
        }
    }
}
