﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemInfoUI : MonoBehaviour
{
    #region INSPECTOR

    public Image        itemIcon;
    public Text         itemDescription;
    public Text         itemName;
    public Text         sellPrice;
    public Text         characterCurrentAmount;
    public Text         characterLimitAmount;
    public Text         ItemBoxCurrentAmount;
    public GameObject   sellPriceParent;
    public GameObject   itemBoxIconParent;
    public GameObject   characterInventoryIconParent;

    #endregion

    // 슬롯 호버
    public void VisualizeItemInfo(eItemID itemID)
    {
        if (itemID != eItemID.None)
            SetItemInfo(itemID);
        else
            SetEmptyInfo();
    }

    // 아이템 정보 세팅
    public void SetItemInfo(eItemID itemID)
    {
        if(itemID != eItemID.None)
        {
            ItemData tempItem       = TableMng.Ins.itemTb[itemID];
            itemName.text           = tempItem.itemName;
            itemDescription.text    = tempItem.description;
            sellPrice.text          = tempItem.sellPrice.ToString();
            itemIcon.sprite         = AtlasMng.Ins.GetSprite(eAtlasCategory.Item, itemID.ToString());

            UserData nowUser            = UserDataMng.Ins.nowCharacter;
            characterLimitAmount.text   = tempItem.characterLimit.ToString();
            ItemBoxCurrentAmount.text   = nowUser.itemBoxInventory.GetItemCount(itemID).ToString();
            characterCurrentAmount.text = nowUser.characterInventory.GetItemCount(itemID).ToString();

            sellPriceParent.SetActive(true);
            itemBoxIconParent.SetActive(true);
            itemIcon.gameObject.SetActive(true);
            characterInventoryIconParent.SetActive(true);
        }
        else
            SetEmptyInfo();
    }

    // 빈 정보 세팅
    public void SetEmptyInfo()
    {
        itemName.text = "";
        itemDescription.text = "";
        sellPriceParent.SetActive(false);
        itemBoxIconParent.SetActive(false);
        itemIcon.gameObject.SetActive(false);
        characterInventoryIconParent.SetActive(false);
    }
}
