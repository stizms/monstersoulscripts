﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryCharacterToBoxUI : MonoBehaviour, IInteractingUI
{
    #region INSPECTOR
        
    public CharacterInventoryUI characterInventoryUI;
    public ItemBoxInventoryUI   boxInventoryUI;
    public ItemInfoUI           itemInfoUI;
    public ItemMoveUI           itemMoveUI;

    #endregion

    [HideInInspector]
    public  CharacterToBoxInventorySlot startSlot = null;
    [HideInInspector]
    public  CharacterToBoxInventorySlot targetSlot = null;

    [HideInInspector]
    public int recentLeftClickIndex = 0;
    [HideInInspector]
    public int recentRightClickIndex = 0;

    private MainItemBoxUI mainItemBoxUI;

    public void VisualizeCharacterToBox()
    {
        if (mainItemBoxUI == null) mainItemBoxUI = UIMng.Ins.lobby.itemBoxUI.mainItemBoxUI;

        UIMng.Ins.lobby.itemBoxUI.titleText.text = "아이템 BOX ▶ 아이템 교체";
        UIMng.Ins.lobby.itemBoxUI.ChangeNoticeText("교체할 아이템 범위를 선택하십시오.");

        characterInventoryUI.VisualizeCharacterInventory();
        CharacterInventoryActive();
        boxInventoryUI.VisualizeItemBoxInventory();
        gameObject.SetActive(true);
        GameMng.Ins.CursorReset();
    }

    public void ExitUI()
    {
        // 둘다 클릭한 경우
        if(startSlot != null && targetSlot != null)
        {
            switch (targetSlot.inventoryCategory)
            {
                case eInventoryCategory.CharacterInventory:
                    characterInventoryUI.blackBoard.SetActive(false);
                    characterInventoryUI.description.SetActive(true);
                    characterInventoryUI.slotList[recentLeftClickIndex].OnHoverEnter();
                    break;
                case eInventoryCategory.ItemBoxInventory:
                    boxInventoryUI.blackBoard.SetActive(false);
                    boxInventoryUI.description.SetActive(true);
                    boxInventoryUI.slotList[recentRightClickIndex].OnHoverEnter();
                    break;
            }
            itemMoveUI.NonVisualizeUI();
            mainItemBoxUI.selectedSlot[1].gameObject.SetActive(false);
            mainItemBoxUI.selectedSlot[1].ResetSlot();
            mainItemBoxUI.selectedSlot.RemoveAt(1);
            targetSlot = null;
        }
        // 한쪽만 클릭한 경우
        else if (startSlot != null)
        {
            if (UIMng.Ins.lobby.prevHover != null)
            {
                UIMng.Ins.lobby.prevHover.SetActive(false);
                UIMng.Ins.lobby.prevHover = null;
            }

            mainItemBoxUI.selectedSlot[0].gameObject.SetActive(false);
            mainItemBoxUI.selectedSlot[0].ResetSlot();
            mainItemBoxUI.selectedSlot.RemoveAt(0);
            
            if(startSlot.inventoryCategory == eInventoryCategory.CharacterInventory)
                CharacterInventoryActive();
            else
                ItemBoxInventoryActive();
            startSlot = null;
        }
        else
        {
            UIMng.Ins.lobby.itemBoxUI.titleText.text = "아이템 BOX";
            startSlot   = null;
            targetSlot  = null;
            recentLeftClickIndex  = 0;
            recentRightClickIndex = 0;
            gameObject.SetActive(false);
            mainItemBoxUI.VisualizeMainUI();
            characterInventoryUI.ResetSlotList();
            boxInventoryUI.ResetSlotList();
            GameMng.Ins.CursorReset();
        }
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(1))
        {
            ExitUI();
        }
    }

    // 캐릭터 인벤토리 키는 것으로 초기화
    public void CharacterInventoryActive()
    {
        itemInfoUI.SetEmptyInfo();
        
        //왼쪽 블랙보드 끄기
        characterInventoryUI.blackBoard.SetActive(false);
        characterInventoryUI.area.SetActive(false);
        characterInventoryUI.description.SetActive(true);
        characterInventoryUI.slotList[recentLeftClickIndex].OnHoverEnter();
        recentLeftClickIndex = 0;

        //오른쪽 블랙보드 키기
        boxInventoryUI.blackBoard.SetActive(true);
        boxInventoryUI.area.SetActive(true);
        boxInventoryUI.description.SetActive(false);
    }

    // 박스 인벤토리 키는 것으로 초기화
    public void ItemBoxInventoryActive()
    {
        itemInfoUI.SetEmptyInfo();
        
        //오른쪽 블랙보드 끄기
        boxInventoryUI.blackBoard.SetActive(false);
        boxInventoryUI.area.SetActive(false);
        boxInventoryUI.description.SetActive(true);
        boxInventoryUI.slotList[recentRightClickIndex].OnHoverEnter();
        recentRightClickIndex = 0;

        //왼쪽 블랙보드 키기
        characterInventoryUI.blackBoard.SetActive(true);
        characterInventoryUI.area.SetActive(true);
        characterInventoryUI.description.SetActive(false);
    }

    // 슬롯 클릭시 startSlot과 targetSlot을 세팅하는 함수
    public void ClickSlot(CharacterToBoxInventorySlot target)
    {
        if (startSlot != null && targetSlot != null)    return;
        else if (startSlot != null)
        {
            targetSlot = target;
            itemMoveUI.SetData(startSlot,targetSlot);
            itemMoveUI.VisualizeUI();
        }
        else
            startSlot = target;
    }
    
    // 인벤토리 상 아이템이 실질적으로 움직이는 함수
    public void MoveItem(int amount)
    {
        eItemID     startItem   = startSlot.slotItem;
        eItemID     targetItem  = targetSlot.slotItem;
        UserData    userData    = UserDataMng.Ins.nowCharacter;

        switch (startSlot.inventoryCategory)
        {
            case eInventoryCategory.CharacterInventory:
                
                if (targetItem == eItemID.None)
                {
                    userData.characterInventory.DeductItemAmount(startItem, amount);
                    userData.itemBoxInventory.GetItem(startItem, amount);
                }
                else
                {
                    userData.itemBoxInventory.DeductItemAmount(targetItem, amount);
                    userData.characterInventory.GetItem(targetItem, amount);

                    if (targetItem != startItem)
                    {
                        userData.characterInventory.DeductItemAmount(startItem, startSlot.itemAmount);
                        userData.itemBoxInventory.GetItem(startItem, startSlot.itemAmount);
                    }
                }

                // [ UI 갱신 ]
                // 넣기만 하는 경우
                if (targetItem == eItemID.None)
                {
                    if (startSlot.itemAmount - amount <= 0)
                        startSlot.ResetSlot();
                    else
                        startSlot.DeductItemAmountText(amount);
                    UnionAndFillEmptySlot(boxInventoryUI.slotList, startItem, amount, userData.itemBoxInventory.GetItemCount(startItem), TableMng.Ins.itemTb[startItem].itemBoxLimit, targetSlot);
                }
                else
                    MoveBoxToCharacterSlot(amount, startSlot, startItem, targetSlot, targetItem, userData);
                break;
            case eInventoryCategory.ItemBoxInventory:
                
                userData.itemBoxInventory.DeductItemAmount(startItem, amount);
                userData.characterInventory.GetItem(startItem, amount);

                if (targetItem != eItemID.None && startItem != targetItem)
                {
                    userData.characterInventory.DeductItemAmount(targetItem, targetSlot.itemAmount);
                    userData.itemBoxInventory.GetItem(targetItem, targetSlot.itemAmount);
                }

                // [ UI 갱신 ]
                MoveBoxToCharacterSlot(amount, targetSlot, targetItem, startSlot, startItem, userData);
                break;
        }

        foreach (var node in UIMng.Ins.lobby.itemBoxUI.mainItemBoxUI.selectedSlot)
        {
            node.gameObject.SetActive(false);
            node.ResetSlot();
        }
        mainItemBoxUI.selectedSlot.Clear();
        itemMoveUI.NonVisualizeUI();
        if (startSlot.inventoryCategory == eInventoryCategory.CharacterInventory)
            CharacterInventoryActive();
        else
            ItemBoxInventoryActive();
        startSlot   = null;
        targetSlot  = null;
    }

    // 아이템 박스에서 캐릭터 인벤토리로 움직일때 쓰는 함수
    // 움직이는 아이템의 개수, 캐릭터 인벤 선택 슬롯, 캐릭터 인벤 아이템, 박스 인벤 선택 슬롯, 박스 인벤 아이템, 유저데이터 )
    private void MoveBoxToCharacterSlot(int amount, InventorySlot characterSlot, eItemID characterInvenItem, InventorySlot boxSlot, eItemID boxInvenItem, UserData userData )
    {
        ItemData boxItemData = TableMng.Ins.itemTb[boxInvenItem];

        // 남아있는 경우
        if (boxSlot.itemAmount - amount > 0)
        {
            if (boxSlot.itemAmount == boxItemData.itemBoxLimit)
            {
                // 다른거 찾고
                InventorySlot tempSlot = null;
                // 짜바리 찾아서 짜바리 들 낮은 애들부터 없애야 함
                foreach (var node in boxInventoryUI.slotList)
                {
                    if (node.slotItem == boxInvenItem && node.itemAmount != boxItemData.itemBoxLimit)
                    {
                        tempSlot = node;
                        break;
                    }
                }

                if (tempSlot == null) tempSlot = boxSlot;

                int deductAmount = DeductBoxSlotAmountText(tempSlot, characterInvenItem, boxInvenItem, characterSlot, userData.itemBoxInventory.GetItemCount(characterInvenItem), amount);
                if (deductAmount > 0)
                {
                    foreach (var node in boxInventoryUI.slotList)
                    {
                        if (node.slotItem == boxInvenItem)
                        {
                            deductAmount = DeductBoxSlotAmountText(node, characterInvenItem, boxInvenItem, characterSlot, userData.itemBoxInventory.GetItemCount(characterInvenItem), deductAmount);
                            if (deductAmount <= 0) break;
                        }
                    }
                }
                UnionAndFillEmptySlot(characterInventoryUI.slotList, boxInvenItem, amount, userData.characterInventory.GetItemCount(boxInvenItem), boxItemData.characterLimit, characterSlot);
            }
            else
            {
                // 타겟 깎고, 다른 곳에 갱신
                boxSlot.DeductItemAmountText(amount);

                if (characterInvenItem != boxInvenItem && !(characterInvenItem == eItemID.None || boxInvenItem == eItemID.None))
                    UnionAndFillEmptySlot(boxInventoryUI.slotList, characterInvenItem, characterSlot.itemAmount, userData.itemBoxInventory.GetItemCount(characterInvenItem), TableMng.Ins.itemTb[characterInvenItem].itemBoxLimit);
                // 무조건 실행
                UnionAndFillEmptySlot(characterInventoryUI.slotList, boxInvenItem, amount, userData.characterInventory.GetItemCount(boxInvenItem), boxItemData.characterLimit, characterSlot);
            }
        }
        // 다른 곳에서 더 깎아야 함 targetSlot은 리셋하고
        else if (boxSlot.itemAmount - amount < 0)
        {
            int temp = amount - boxSlot.itemAmount;
            boxSlot.ResetSlot();
            // 노드 찾아보고 temp만큼 더 까야함
            foreach (var node in boxInventoryUI.slotList)
            {
                if (node.slotItem == boxInvenItem)
                {
                    node.DeductItemAmountText(temp);
                    break;
                }
            }

            if (characterInvenItem != boxInvenItem && !(characterInvenItem == eItemID.None || boxInvenItem == eItemID.None))
                UnionAndFillEmptySlot(boxInventoryUI.slotList, characterInvenItem, characterSlot.itemAmount, userData.itemBoxInventory.GetItemCount(characterInvenItem), TableMng.Ins.itemTb[characterInvenItem].itemBoxLimit, boxSlot);
            UnionAndFillEmptySlot(characterInventoryUI.slotList, boxInvenItem, amount, userData.characterInventory.GetItemCount(boxInvenItem), boxItemData.characterLimit, characterSlot);
        }
        else
        {
            if (characterInvenItem != boxInvenItem && !(characterInvenItem == eItemID.None || boxInvenItem == eItemID.None))
                UnionAndFillEmptySlot(boxInventoryUI.slotList, characterInvenItem, characterSlot.itemAmount, userData.itemBoxInventory.GetItemCount(characterInvenItem), TableMng.Ins.itemTb[characterInvenItem].itemBoxLimit, boxSlot);
            else
                boxSlot.ResetSlot();
            UnionAndFillEmptySlot(characterInventoryUI.slotList, boxInvenItem, amount, userData.characterInventory.GetItemCount(boxInvenItem), boxItemData.characterLimit, characterSlot);
        }
    }

    // 아이템 박스에서 아이템 빼는 함수 ( 현재 차감해야할 슬롯, 캐릭터인벤토리 아티템, 박스 인벤토리 아이템, 아이템 박스로 넘어올 아이템이 있는 슬롯, 현재 캐릭터가 해당 아이템 가지고 있는 개수, 뺄 개수 )
    private int DeductBoxSlotAmountText(InventorySlot slot, eItemID characterInvenItem, eItemID boxInvenItem,InventorySlot originDestiantionSlot ,int reserveAmount, int deductAmount)
    {
        if (slot.itemAmount > deductAmount)
        {
            slot.DeductItemAmountText(deductAmount);
            deductAmount = 0;
            if (characterInvenItem != boxInvenItem && !(characterInvenItem == eItemID.None || boxInvenItem == eItemID.None))
                UnionAndFillEmptySlot(boxInventoryUI.slotList, characterInvenItem, originDestiantionSlot.itemAmount, reserveAmount, TableMng.Ins.itemTb[characterInvenItem].itemBoxLimit);
        }
        else if (slot.itemAmount < deductAmount)
        {
            deductAmount -= slot.itemAmount;
            slot.ResetSlot();
        }
        else
        {
            slot.ResetSlot();
            deductAmount = 0;
            if (characterInvenItem != boxInvenItem && !(characterInvenItem == eItemID.None || boxInvenItem == eItemID.None))
                UnionAndFillEmptySlot(boxInventoryUI.slotList, characterInvenItem, originDestiantionSlot.itemAmount, reserveAmount, TableMng.Ins.itemTb[characterInvenItem].itemBoxLimit);
        }
        return deductAmount;
    }
    
    // 빈칸이 있다면 그 곳에 남은 양을 넣는함수 ( target은 특정위치에 아이템이 들어가야하느냐 아니여도 상관없느냐의 차이 )
    private void UnionAndFillEmptySlot(List<InventorySlot> slotList, eItemID itemID, int changeAmount,int itemCount, int maximum,InventorySlot target = null)
    {
        // 한 칸에 차지할 수 있는 비용 보다 아이템 개수가 적을 때 ( 한칸만 찾아서 배치하면 됨 )
        if (itemCount <= maximum)
        {
            int index = 0;
            int firstEmptyIndex = 0;
            bool isEmptyCheck = false;

            foreach (var node in slotList)
            {
                if (node.slotItem == itemID)
                {
                    if(target != null)
                    {
                        node.ResetSlot();
                        target.SetData(itemID, itemCount);
                    }
                    else
                        node.SetData(itemID, itemCount);
                    break;
                }
                else if( !isEmptyCheck && node.slotItem == eItemID.None)
                {
                    isEmptyCheck = true;
                    firstEmptyIndex = index;
                }
                index++;
            }
            if (index == Define.Normal_Inventory_Slot_Num)
            {
                if (target != null)
                    target.SetData(itemID, itemCount);
                else
                    slotList[firstEmptyIndex].SetData(itemID, itemCount);
            }
        }
        // 한칸에 차지할 수 있는 비용보다 아이템 개수가 많을 때 ( 한 곳이 다찰 때까지 넣어주고 나머지는 다른 곳에 넣어줘야 함 )
        else
        {
            // 동일한 아이템 슬롯들을 보여 Union 함
            foreach (var node in slotList)
            {
                if (node.slotItem == itemID)
                {
                    // 이미 다 차있는거라면 지나감
                    if (node.itemAmount == maximum) continue;

                    if (node.itemAmount + changeAmount > maximum)
                    {
                        int gap = maximum - node.itemAmount;
                        node.PlusItemAmountText(gap);
                        changeAmount -= gap;
                    }
                    else
                    {
                        if (target != null)
                        {
                            int count = node.itemAmount;
                            node.ResetSlot();
                            target.SetData(itemID, count + changeAmount);
                        }
                        else
                            node.PlusItemAmountText(changeAmount);
                        changeAmount = 0;
                        break;
                    }
                }
            }

            // 동일한 아이템 슬롯들이 모두 MAXIMUM이 되었는데도 아이템이 남아있는경우 ( 빈칸에 넣어줌 )
            if (changeAmount > 0)
            {
                if (target != null && changeAmount <= maximum)
                {
                    target.SetData(itemID, changeAmount);
                    return;
                }
                else if (target != null && changeAmount > maximum)
                {
                    target.SetData(itemID, maximum);
                    changeAmount -= maximum;
                }

                foreach (var node in slotList)
                {
                    if (node.slotItem == eItemID.None)
                    {
                        if (changeAmount > maximum)
                        {
                            node.SetData(itemID, maximum);
                            changeAmount -= maximum;
                        }
                        else
                        {
                            if (changeAmount <= 0) break;
                            else
                            {
                                node.SetData(itemID, changeAmount);
                                changeAmount = 0;
                            }
                        }
                    }
                }
            }
        }
    }
}