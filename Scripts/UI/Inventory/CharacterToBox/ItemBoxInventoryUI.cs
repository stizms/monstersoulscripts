﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBoxInventoryUI : MonoBehaviour
{
    #region INSPECTOR

    public List<InventorySlot> slotList = new List<InventorySlot>();

    public GameObject description;
    public GameObject blackBoard;
    public GameObject area;

    #endregion

    private LobbyUI lobby;
    
    // 블랙보드 호버 될 때
    public void AreaHoverEnter()
    {
        if (lobby == null) lobby = UIMng.Ins.lobby;
        if (area.activeSelf)
        {
            if (lobby.prevHover != null)
            {
                lobby.prevHover.SetActive(false);
                lobby.prevHover = null;
            }
            lobby.itemBoxUI.mainItemBoxUI.characterToBoxUI.ItemBoxInventoryActive();
        }
    }

    public void VisualizeItemBoxInventory()
    {
        int index = 0;
        foreach (var item in UserDataMng.Ins.nowCharacter.itemBoxInventory.itemList)
        {
            ItemData itemData = TableMng.Ins.itemTb[item.Key];
            int amount = item.Value;

            while (true)
            {
                if (amount > itemData.itemBoxLimit)
                {
                    slotList[index].SetData(item.Key, itemData.itemBoxLimit);
                    amount -= itemData.itemBoxLimit;
                    index++;
                }
                else
                {
                    slotList[index].SetData(item.Key, amount);
                    index++;
                    break;
                }
            }
        }
    }

    // 초기화
    public void ResetSlotList()
    {
        foreach (var slot in slotList)
        {
            slot.ResetSlot();
            slot.hoverOverlay.SetActive(false);
        }
    }
}
