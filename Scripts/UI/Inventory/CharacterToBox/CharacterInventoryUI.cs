﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInventoryUI : MonoBehaviour
{
    #region INSPECTOR

    public List<InventorySlot> slotList = new List<InventorySlot>();

    public GameObject description;
    public GameObject blackBoard;
    public GameObject area;

    #endregion

    private LobbyUI lobby;

    // 블랙보드 호버 될 때
    public void AreaHoverEnter()
    {
        if (area.activeSelf)
        {
            if (lobby == null) lobby = UIMng.Ins.lobby;
            if (lobby.prevHover != null)
            {
                lobby.prevHover.SetActive(false);
                lobby.prevHover = null;
            }
            lobby.itemBoxUI.mainItemBoxUI.characterToBoxUI.CharacterInventoryActive();
        }
    }

    // 켜질 때 초기 세팅
    public void VisualizeCharacterInventory()
    {
        int index = 0;
        foreach (var item in UserDataMng.Ins.nowCharacter.characterInventory.itemList)
        {
            slotList[index].SetData(item.Key, item.Value);
            index++;
        }
    }

    // 꺼질 때 초기화
    public void ResetSlotList()
    {
        foreach (var slot in slotList)
        {
            slot.ResetSlot();
            slot.hoverOverlay.SetActive(false);
        }
    }
}
