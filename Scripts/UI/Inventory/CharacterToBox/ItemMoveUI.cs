﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemMoveUI : MonoBehaviour
{
    #region INSPECTOR

    public Text     title;
    public Text     amountText;
    public Image    directionArrow;

    #endregion

    private int     amount      = 0;
    private bool    direction   = false;  //아이템의 행방 ( 꺼내기인지 넣기인지! ) false일때는 꺼내기 true 넣기

    private CharacterToBoxInventorySlot startSlot;
    private CharacterToBoxInventorySlot targetSlot;

    public void VisualizeUI()
    {
        gameObject.SetActive(true);
    }

    public void NonVisualizeUI()
    {
        gameObject.SetActive(false);
    }

    public void SetData(CharacterToBoxInventorySlot startSlot, CharacterToBoxInventorySlot targetSlot)
    {
        this.startSlot  = startSlot;
        this.targetSlot = targetSlot;
        UserData userData = UserDataMng.Ins.nowCharacter;
        // 타겟이 아이템 박스이며 비어있는 슬롯인 경우
        if (targetSlot.inventoryCategory == eInventoryCategory.ItemBoxInventory && targetSlot.slotItem == eItemID.None)
        {
            direction               = true;
            directionArrow.sprite   = AtlasMng.Ins.GetSprite(eAtlasCategory.Inventory, "LeftArrow");
            title.text              = "아이템 넣기";
            amount                  = userData.characterInventory.GetItemCount(startSlot.slotItem);
            amountText.text         = amount.ToString();
        }
        else
        {
            direction               = false;
            directionArrow.sprite   = AtlasMng.Ins.GetSprite(eAtlasCategory.Inventory, "RightArrow");
            title.text              = "아이템 꺼내기";

            eItemID tempItem = eItemID.None;
            switch (targetSlot.inventoryCategory)
            {
                case eInventoryCategory.CharacterInventory:
                    tempItem = startSlot.slotItem;
                    break;

                case eInventoryCategory.ItemBoxInventory:
                    tempItem = targetSlot.slotItem;
                    break;
            }
            amount = userData.itemBoxInventory.GetItemCount(tempItem);

            ItemData itemData = TableMng.Ins.itemTb[tempItem];
            int tempCount = itemData.characterLimit - userData.characterInventory.GetItemCount(tempItem);
            if(tempCount < amount)
                amount = tempCount;

            amountText.text = amount.ToString();
        }
    }

    // 개수 올리기
    public void OnUpBtn()
    {
        UserData userData = UserDataMng.Ins.nowCharacter;
        int MAX = 0;
        // 넣기
        if(direction)
            MAX = userData.characterInventory.GetItemCount(startSlot.slotItem);
        //꺼내기
        else
        {
            eItemID tempItem = eItemID.None;
            switch (targetSlot.inventoryCategory)
            {
                case eInventoryCategory.CharacterInventory:
                    tempItem = startSlot.slotItem;
                    break;

                case eInventoryCategory.ItemBoxInventory:
                    tempItem = targetSlot.slotItem;
                    break;
            }
            int tempMax = TableMng.Ins.itemTb[tempItem].characterLimit - userData.characterInventory.GetItemCount(tempItem);
            if (userData.itemBoxInventory.GetItemCount(tempItem) >= tempMax)
                MAX = tempMax;
            else
                MAX = userData.itemBoxInventory.GetItemCount(tempItem);
        }

        if (amount == MAX)  return;

        amount++;
        amountText.text = amount.ToString();
    }

    // 개수 낮추기
    public void OnDownBtn()
    {
        if (amount <= 1)    return;
        amount--;
        amountText.text = amount.ToString();
    }

    // 바꾸기
    public void OnInteractBtn()
    {
        if (amount <= 0)    return;
        UIMng.Ins.lobby.itemBoxUI.mainItemBoxUI.characterToBoxUI.MoveItem(amount);
    }
}