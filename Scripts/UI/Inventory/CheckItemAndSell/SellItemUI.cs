﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SellItemUI : MonoBehaviour
{
    #region INSPECTOR
    
    public Text costText;
    public Text amountText;

    #endregion

    private int         amount      = 0;
    private int         sellPrice   = 0;
    private eItemID     sellItem    = eItemID.None;
    private UserData    userData    = null;

    public void VisualizeUI()
    {
        gameObject.SetActive(true);
    }

    public void NonVisualizeUI()
    {
        sellItem    = eItemID.None;
        sellPrice   = 0;
        gameObject.SetActive(false);
    }

    public void SetData(eItemID itemID)
    {
        if (userData == null) userData = UserDataMng.Ins.nowCharacter;

        sellItem = itemID;
        amount = 1; 
        amountText.text = amount.ToString();
        sellPrice = TableMng.Ins.itemTb[sellItem].sellPrice;
        costText.text = (amount * sellPrice).ToString();
    }

    // 개수 올리기
    public void OnUpBtn()
    {
        int MAX = userData.itemBoxInventory.GetItemCount(sellItem);
        if (amount == MAX)  return;

        amount++;
        amountText.text = amount.ToString();
        costText.text = (amount * sellPrice).ToString();
    }

    // 개수 낮추기
    public void OnDownBtn()
    {
        if (amount <= 1)    return;
        amount--;
        amountText.text = amount.ToString();
        costText.text = (amount * sellPrice).ToString();
    }

    // 바꾸기
    public void OnInteractBtn()
    {
        UIMng.Ins.lobby.itemBoxUI.mainItemBoxUI.checkItemUI.SellItem(amount);
        NonVisualizeUI();
    }
}
