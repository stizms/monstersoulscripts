﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckItemUI : MonoBehaviour, IInteractingUI
{
    #region INSPECTOR

    public ItemBoxInventoryUI   boxInventoryUI;
    public ItemInfoUI           itemInfoUI;
    public SellItemUI           sellItemUI;

    #endregion

    [HideInInspector]
    public CheckItemSlot targetSlot;

    private UserData        userData;
    private MainItemBoxUI   mainItemBoxUI;
    private bool isFirstTime = true;

    public void VisualizeUI()
    {
        if(isFirstTime)
        {
            userData        = UserDataMng.Ins.nowCharacter;
            mainItemBoxUI   = UIMng.Ins.lobby.itemBoxUI.mainItemBoxUI;
            isFirstTime     = false;
        }

        UIMng.Ins.lobby.itemBoxUI.titleText.text = "아이템 BOX ▶ 아이템 확인/매각";
        UIMng.Ins.lobby.itemBoxUI.ChangeNoticeText("매각할 아이템을 선택하십시오.");
        boxInventoryUI.VisualizeItemBoxInventory();
        boxInventoryUI.slotList[0].OnHoverEnter();
        gameObject.SetActive(true);
        GameMng.Ins.CursorReset();
    }

    public void ExitUI()
    {    
        if(targetSlot != null)
        {
            mainItemBoxUI.selectedSlot[0].gameObject.SetActive(false);
            mainItemBoxUI.selectedSlot.RemoveAt(0);
            sellItemUI.NonVisualizeUI();
            boxInventoryUI.blackBoard.SetActive(false);
            targetSlot.OnHoverEnter();
            targetSlot = null;
        }
        else
        {
            UIMng.Ins.lobby.itemBoxUI.titleText.text = "아이템 BOX";
            gameObject.SetActive(false);
            boxInventoryUI.ResetSlotList();
            targetSlot = null;
            mainItemBoxUI.VisualizeMainUI();
            GameMng.Ins.CursorReset();
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            ExitUI();
        }
    }

    public void SellItem(int amount)
    {
        eItemID  targetItem     =   targetSlot.slotItem;
        ItemData sellItemData   =   TableMng.Ins.itemTb[targetItem];
        userData.money          +=  amount * sellItemData.sellPrice;
        userData.itemBoxInventory.DeductItemAmount(targetItem, amount);
        UIMng.Ins.lobby.itemBoxUI.moneyText.text = userData.money.ToString();
        SoundMng.Ins.ExternalSoundPlay(eSound.MoneyChange.ToDesc(), eChannel.UI_Effect);


        // UI 갱신
        if (targetSlot.itemAmount - amount > 0)
        {
            if (targetSlot.itemAmount == sellItemData.itemBoxLimit)
            {
                InventorySlot tempSlot = null;
                foreach (var node in boxInventoryUI.slotList)
                {
                    if (node.slotItem == targetItem && node.itemAmount != sellItemData.itemBoxLimit)
                    {
                        tempSlot = node;
                        break;
                    }
                }

                if (tempSlot == null) tempSlot = targetSlot;

                int deductAmount = DeductBoxSlotAmountText(tempSlot, amount);
                if (deductAmount > 0)
                {
                    foreach (var node in boxInventoryUI.slotList)
                    {
                        if (node.slotItem == targetItem)
                        {
                            deductAmount = DeductBoxSlotAmountText(node, deductAmount);
                            if (deductAmount <= 0) break;
                        }
                    }
                }
            }
            else
                targetSlot.DeductItemAmountText(amount);
        }
        else if (targetSlot.itemAmount - amount < 0)
        {
            int deductAmount = DeductBoxSlotAmountText(targetSlot, amount);
            if (deductAmount > 0)
            {
                foreach (var node in boxInventoryUI.slotList)
                {
                    if (node.slotItem == targetItem)
                    {
                        deductAmount = DeductBoxSlotAmountText(node, deductAmount);
                        if (deductAmount <= 0) break;
                    }
                }
            }
        }
        else
            targetSlot.ResetSlot();

        mainItemBoxUI.selectedSlot[0].gameObject.SetActive(false);
        mainItemBoxUI.selectedSlot.RemoveAt(0);
        mainItemBoxUI.checkItemUI.boxInventoryUI.blackBoard.SetActive(false);
        targetSlot = null;
    }
    
    private int DeductBoxSlotAmountText(InventorySlot slot, int deductAmount)
    {
        if (slot.itemAmount > deductAmount)
        {
            slot.DeductItemAmountText(deductAmount);
            deductAmount = 0;
              
        }
        else if (slot.itemAmount < deductAmount)
        {
            deductAmount -= slot.itemAmount;
            slot.ResetSlot();
        }
        else
        {
            slot.ResetSlot();
            deductAmount = 0;
        }
        return deductAmount;
    }
}
