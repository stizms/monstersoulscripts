﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainItemBoxUI : MonoBehaviour, IInteractingUI
{
    #region INSPECTOR

    public List<ButtonHover> btnList = new List<ButtonHover>();

    public InventoryCharacterToBoxUI    characterToBoxUI;
    public CheckItemUI                  checkItemUI;
    public EquipmentChangeUI            equipmentChangeUI;
    public EquipmentSellUI              equipmentSellUI;

    #endregion

    [HideInInspector]
    public List<InventorySlot> selectedSlot = new List<InventorySlot>();

    private LobbyUI lobbyUI;

    public void Start()
    {
        lobbyUI = UIMng.Ins.lobby;
        equipmentChangeUI.character = lobbyUI.lobbyScene.player;
    }

    public void VisualizeMainUI()
    {
        gameObject.SetActive(true);
        btnList[0].OnHoverEnter();
    }

    public void ExitUI()
    {
        GameMng.Ins.UnLock();
        gameObject.SetActive(false);
        lobbyUI.itemBoxUI.NonVisualizeItemBoxUI();
        if (QuestMng.Ins.nowQuest != null)
            lobbyUI.lobbyQuestUI.VisualizeQuestUI();
        lobbyUI.minimapUI.SetActive(true);
        UIMng.Ins.mouseInteractiveUI.gameObject.SetActive(true);
    }

    // 아이템 교체 버튼을 눌렀을 때
    public void OnCharacterToBoxBtn()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.ItemBoxBtnClick.ToDesc(), eChannel.UI_Effect);
        gameObject.SetActive(false);
        characterToBoxUI.VisualizeCharacterToBox();
    }

    public void OnCheckItemBtn()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.ItemBoxBtnClick.ToDesc(), eChannel.UI_Effect);
        gameObject.SetActive(false);
        checkItemUI.VisualizeUI();
    }

    public void OnEquipmentChangeBtn()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.ItemBoxBtnClick.ToDesc(), eChannel.UI_Effect);
        gameObject.SetActive(false);
        equipmentChangeUI.VisualizeUI();
    }

    public void OnEquipmentSellBtn()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.ItemBoxBtnClick.ToDesc(), eChannel.UI_Effect);
        gameObject.SetActive(false);
        equipmentSellUI.VisualizeUI();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            ExitUI();
        }
    }

}