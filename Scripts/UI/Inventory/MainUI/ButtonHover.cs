﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHover : HoverUI
{
    #region INSPECTOR
    
    public string       noticeStr;

    #endregion

    public override void OnHoverEnter()
    {
        if (SceneMng.Ins.nowScene == eScene.Lobby)
        {
            LobbyUI lobbyUI = UIMng.Ins.lobby;
            if (lobbyUI.prevHover == hoverOverlay) return;
            lobbyUI.HoverChange(hoverOverlay);
            lobbyUI.itemBoxUI.ChangeNoticeText(noticeStr);
        }
        else if (SceneMng.Ins.nowScene > eScene.Dungeon)
        {
            DungeonUI dungeonUI = UIMng.Ins.dungeon;
            if (dungeonUI.prevHover == hoverOverlay) return;
            dungeonUI.HoverChange(hoverOverlay);
            dungeonUI.questRewardUI.dungeonItemUI.dungeonItemBox.itemInfoUI.SetEmptyInfo();
        }
        SoundMng.Ins.ExternalSoundPlay(eSound.CharacterSlotHoverSound.ToDesc(), eChannel.UI_Effect);
    }

    public override void OnClickBtn()
    {
    }
}
