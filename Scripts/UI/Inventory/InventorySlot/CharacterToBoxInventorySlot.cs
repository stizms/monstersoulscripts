﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterToBoxInventorySlot : InventorySlot
{
    #region INSPECTOR

    // 어느쪽 슬롯인지 나타냄
    public eInventoryCategory   inventoryCategory;
    public InventorySlot        relatedSlot;
    public int                  slotIndex;

    #endregion
    
    // 캐싱
    private bool isFirstTime = true;
    private ItemBoxInventoryUI          boxInventoryUI;
    private CharacterInventoryUI        characterInventoryUI;
    private InventoryCharacterToBoxUI   characterToBoxUI;

    public override void OnHoverEnter()
    {
        if(isFirstTime)
        {
            characterToBoxUI        = UIMng.Ins.lobby.itemBoxUI.mainItemBoxUI.characterToBoxUI;
            characterInventoryUI    = characterToBoxUI.characterInventoryUI;
            boxInventoryUI          = characterToBoxUI.boxInventoryUI;
            isFirstTime             = false;
        }

        if (UIMng.Ins.lobby.prevHover == hoverOverlay) return;
        SoundMng.Ins.ExternalSoundPlay(eSound.PopUpHoverSound.ToDesc(), eChannel.UI_Effect);
        UIMng.Ins.lobby.HoverChange(hoverOverlay);
        characterToBoxUI.itemInfoUI.VisualizeItemInfo(slotItem);
    }

    public override void OnClickBtn()
    {
        if (slotItem == eItemID.None && characterToBoxUI.startSlot == null)
            return;

        SoundMng.Ins.ExternalSoundPlay(eSound.ItemSlotClick.ToDesc(), eChannel.UI_Effect);

        relatedSlot.gameObject.SetActive(true);
        if (slotItem != eItemID.None)
            relatedSlot.SetData(slotItem, itemAmount);

        if (UIMng.Ins.lobby.prevHover != null)
        {
            UIMng.Ins.lobby.prevHover.SetActive(false);
            UIMng.Ins.lobby.prevHover = null;
        }

        UIMng.Ins.lobby.itemBoxUI.mainItemBoxUI.selectedSlot.Add(relatedSlot);
        characterToBoxUI.ClickSlot(this);


        switch (inventoryCategory)
        {
            case eInventoryCategory.CharacterInventory:
                characterToBoxUI.recentLeftClickIndex = slotIndex;
                // 현재 내꺼 끄고
                characterInventoryUI.blackBoard.SetActive(true);
                characterInventoryUI.description.SetActive(false);

                // 둘다 선택되지 않았을 경우
                if (!(characterToBoxUI.startSlot != null && characterToBoxUI.targetSlot != null))
                {
                    boxInventoryUI.blackBoard.SetActive(false);
                    boxInventoryUI.area.SetActive(false);
                    boxInventoryUI.description.SetActive(true);

                    ChangeActiveInventory(boxInventoryUI.slotList);
                }
                break;

            case eInventoryCategory.ItemBoxInventory:
                characterToBoxUI.recentRightClickIndex = slotIndex;
                boxInventoryUI.blackBoard.SetActive(true);
                boxInventoryUI.description.SetActive(false);

                if (!(characterToBoxUI.startSlot != null && characterToBoxUI.targetSlot != null))
                {
                    characterInventoryUI.blackBoard.SetActive(false);
                    characterInventoryUI.area.SetActive(false);
                    characterInventoryUI.description.SetActive(true);

                    ChangeActiveInventory(characterInventoryUI.slotList);
                }
                break;
        }
    }

    //  빈슬롯 혹은 동일한 아이템 찾아서 Hover 키기
    public void ChangeActiveInventory(List<InventorySlot> slotList)
    {
        int index = 0;
        int firstNoneIndex = -1;

        // 동일한 아이템 슬롯 찾기
        foreach (var node in slotList)
        {
            if (node.slotItem == slotItem)  break;
            else if (node.slotItem == eItemID.None && firstNoneIndex < 0)
                firstNoneIndex = index;

            index++;
        }

        // 동일한 아이템이 없는 경우
        if (index == Define.Normal_Inventory_Slot_Num)
        {
            if (firstNoneIndex >= 0)
                slotList[firstNoneIndex].OnHoverEnter();
            else
                slotList[0].OnHoverEnter();
        }
        else
            slotList[index].OnHoverEnter();
    }
}
