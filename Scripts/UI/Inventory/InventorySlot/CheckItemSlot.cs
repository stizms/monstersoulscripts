﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckItemSlot : InventorySlot
{
    #region INSPECTOR

    public InventorySlot relatedSlot;

    #endregion

    private CheckItemUI checkItemUI;

    public override void OnHoverEnter()
    {
        if (checkItemUI == null) checkItemUI = UIMng.Ins.lobby.itemBoxUI.mainItemBoxUI.checkItemUI;
        if (UIMng.Ins.lobby.prevHover == hoverOverlay) return;
        SoundMng.Ins.ExternalSoundPlay(eSound.PopUpHoverSound.ToDesc(), eChannel.UI_Effect);
        UIMng.Ins.lobby.HoverChange(hoverOverlay);
        checkItemUI.itemInfoUI.VisualizeItemInfo(slotItem);
    }

    public override void OnClickBtn()
    {
        if (slotItem == eItemID.None || checkItemUI.targetSlot != null) return;

        relatedSlot.SetData(slotItem, int.Parse(itemAmountText.text));
        relatedSlot.gameObject.SetActive(true);

        if (UIMng.Ins.lobby.prevHover != null)
        {
            UIMng.Ins.lobby.prevHover.SetActive(false);
            UIMng.Ins.lobby.prevHover = null;
        }

        UIMng.Ins.lobby.itemBoxUI.mainItemBoxUI.selectedSlot.Add(relatedSlot);

        checkItemUI.boxInventoryUI.blackBoard.SetActive(true);
        checkItemUI.boxInventoryUI.description.SetActive(false);
        checkItemUI.targetSlot = this;
        checkItemUI.sellItemUI.SetData(slotItem);
        checkItemUI.sellItemUI.VisualizeUI();
    }
}
