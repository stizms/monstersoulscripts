﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventorySlot : HoverUI
{
    #region INSPECTOR
    
    public Image    itemIcon;
    public Text     itemAmountText;

    #endregion

    // 슬롯에 배치된 아이템
    public eItemID  slotItem    = eItemID.None;
    [HideInInspector]
    public int      itemAmount  = 0;

    // 아이템 정보 세팅
    public void SetData(eItemID itemID, int amount)
    {
        slotItem = itemID;
        if(slotItem == eItemID.None)
        {
            itemIcon.gameObject.SetActive(false);
            itemAmount = 0;
            itemAmountText.text = "";
        }
        else
        {
            itemIcon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Item, slotItem.ToString());
            itemIcon.gameObject.SetActive(true);
            itemAmount          = amount;
            itemAmountText.text = itemAmount.ToString();
        }
    }

    public void PlusItemAmountText(int amount)
    {
        itemAmount += amount;
        itemAmountText.text = itemAmount.ToString();
    }

    public void DeductItemAmountText(int amount)
    {
        itemAmount -= amount;
        itemAmountText.text = itemAmount.ToString();
    }

    // 초기 상태로 리셋
    public void ResetSlot()
    {
        slotItem    = eItemID.None;
        itemAmount  = 0;
        itemAmountText.text = "";
        itemIcon.gameObject.SetActive(false);
        hoverOverlay.SetActive(false);
    }

    public override void OnHoverEnter()
    {
    }

    public override void OnClickBtn()
    {
    }
}

