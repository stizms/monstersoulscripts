﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentSellUI : MonoBehaviour, IInteractingUI
{
    #region INSPECTOR

    public EquipmentInfoUI      equipmentInfoUI;
    public EquipmentSellInfoUI  equipmentSellInfoUI;
    public EquipmentBoxUI       equipmentBoxUI;
    public GameObject           dontSellText;

    #endregion

    [HideInInspector]
    public EquipmentSlot selectSlot;

    private ItemBoxUI itemBoxUI;

    public void VisualizeUI()
    {
        if (itemBoxUI == null) itemBoxUI = UIMng.Ins.lobby.itemBoxUI;

        itemBoxUI.titleText.text = "아이템 BOX ▶ 장비 확인/매각";
        itemBoxUI.ChangeNoticeText("매각할 장비를 선택하십시오.");
        equipmentBoxUI.SetSlotData();
        gameObject.SetActive(true);
        GameMng.Ins.CursorReset();
    }

    public void ExitUI()
    {
        if(selectSlot != null)
            selectSlot = null;
        else
        {
            selectSlot = null;
            itemBoxUI.titleText.text = "아이템 BOX";
            gameObject.SetActive(false);
            equipmentBoxUI.ResetSlotList();
            equipmentBoxUI.description.SetActive(false);
            equipmentInfoUI.ResetInfo();
            equipmentSellInfoUI.ResetInfo();
            itemBoxUI.mainItemBoxUI.VisualizeMainUI();
            GameMng.Ins.CursorReset();
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            ExitUI();
        }
    }
}
