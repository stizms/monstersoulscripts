﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentCheckBoxSlot : EquipmentSlot
{

    private UserData tempUserData;
    private EquipmentSellUI equipmentSellUI;

    public override void SetData(eEquipment equipmentID)
    {
        if (tempUserData == null) tempUserData = UserDataMng.Ins.nowCharacter;

        // 초기화
        ResetSlot();
        slotEquipment = equipmentID;

        if (slotEquipment == eEquipment.None) return;
        else if (slotEquipment > eEquipment.Weapon && slotEquipment < eEquipment.Helmet)
        {
            WeaponData tempWeapon = TableMng.Ins.weaponTb[slotEquipment];
            equipmentIcon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Equipment, tempWeapon.weaponCategory.ToString());
            if (tempUserData.equipWeapon == tempWeapon.weaponID)
                equippedSign.SetActive(true);
        }
        else
        {
            EquipmentData tempEquipment = TableMng.Ins.equipmentTb[equipmentID];
            equipmentIcon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Equipment, tempEquipment.equipmentCategory.ToString());

            // 헬멧
            if (slotEquipment > eEquipment.Helmet && slotEquipment < eEquipment.Vest)
            {
                if (tempUserData.equipHelmet == slotEquipment)
                    equippedSign.SetActive(true);
            }
            // 베스트
            else if (slotEquipment > eEquipment.Vest && slotEquipment < eEquipment.Glove)
            {
                if (tempUserData.equipVest == slotEquipment)
                    equippedSign.SetActive(true);
            }
            // 글로브
            else if (slotEquipment > eEquipment.Glove && slotEquipment < eEquipment.Belt)
            {
                if (tempUserData.equipGlove == slotEquipment)
                    equippedSign.SetActive(true);
            }
            // 벨트
            else if (slotEquipment > eEquipment.Belt && slotEquipment < eEquipment.Pants)
            {
                if (tempUserData.equipBelt == slotEquipment)
                    equippedSign.SetActive(true);
            }
            // 팬츠
            else if (slotEquipment > eEquipment.Pants)
            {
                if (tempUserData.equipPants == slotEquipment)
                    equippedSign.SetActive(true);
            }
        }

        equipmentIcon.gameObject.SetActive(true);
    }

    public override void OnHoverEnter()
    {
        if (equipmentSellUI == null) equipmentSellUI = UIMng.Ins.lobby.itemBoxUI.mainItemBoxUI.equipmentSellUI;

        if (UIMng.Ins.lobby.prevHover == hoverOverlay) return;

        SoundMng.Ins.ExternalSoundPlay(eSound.PopUpHoverSound.ToDesc(), eChannel.UI_Effect);
        UIMng.Ins.lobby.HoverChange(hoverOverlay);

        equipmentSellUI.equipmentInfoUI.SetData(slotEquipment);
        if (equippedSign.activeSelf)
        {
            equipmentSellUI.equipmentSellInfoUI.gameObject.SetActive(false);
            equipmentSellUI.dontSellText.SetActive(true);
        }
        else
        {
            equipmentSellUI.dontSellText.SetActive(false);
            equipmentSellUI.equipmentSellInfoUI.SetData(slotEquipment);
            equipmentSellUI.equipmentSellInfoUI.gameObject.SetActive(true);
        }
    }

    public override void OnClickBtn()
    {
        // 장착 중인 애랑 똑같은 애는 클릭 되면 안됨
        if (slotEquipment == eEquipment.None || equippedSign.activeSelf == true) return;

        int cost = 0;
        if (slotEquipment > eEquipment.Weapon && slotEquipment < eEquipment.Helmet)
            cost = TableMng.Ins.weaponTb[slotEquipment].sellPrice;
        else
            cost = TableMng.Ins.equipmentTb[slotEquipment].sellPrice;

        equipmentSellUI.selectSlot = this;
        UIMng.Ins.twoButtonPopUp.SetPopUp(string.Format("{0}z에 매각합니다.\n 그렇게 하시겠습니까?", cost),SellEquipment, DontSellEquipment, "예", "아니요");
    }

    #region ButtonFunc

    private void SellEquipment()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.MoneyChange.ToDesc(), eChannel.UI_Effect);
        tempUserData.itemBoxInventory.SellEquipment(slotEquipment);
        UIMng.Ins.lobby.itemBoxUI.moneyText.text = tempUserData.money.ToString();
        equipmentSellUI.selectSlot = null;
        SetData(eEquipment.None);
        OnHoverEnter();
    }

    private void DontSellEquipment()
    {
        equipmentSellUI.selectSlot = null;
    }

    #endregion
}