﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentSellInfoUI : MonoBehaviour
{
    #region INSPECTOR

    public Image    itemIcon;
    public Text     itemName;
    public Text     costText;
    public Text     boxCount;

    #endregion

    public void SetData(eEquipment equipmentID)
    {
        ResetInfo();

        if (equipmentID == eEquipment.None) return;

        if(equipmentID > eEquipment.Weapon && equipmentID < eEquipment.Helmet)
        {
            WeaponData weaponData   = TableMng.Ins.weaponTb[equipmentID];
            itemName.text           = weaponData.weaponName;
            costText.text           = weaponData.sellPrice.ToString();
            itemIcon.sprite         = AtlasMng.Ins.GetSprite(eAtlasCategory.Equipment, weaponData.weaponCategory.ToString());
        }
        else
        {
            EquipmentData equipmentData = TableMng.Ins.equipmentTb[equipmentID];
            itemName.text               = equipmentData.equipmentName;
            costText.text               = equipmentData.sellPrice.ToString();
            itemIcon.sprite             = AtlasMng.Ins.GetSprite(eAtlasCategory.Equipment, equipmentData.equipmentCategory.ToString());
        }
        itemIcon.gameObject.SetActive(true);
        boxCount.text = UserDataMng.Ins.nowCharacter.itemBoxInventory.GetEquipmentCount(equipmentID).ToString();
    }

    public void ResetInfo()
    {
        itemIcon.gameObject.SetActive(false);
        itemName.text = "";
        costText.text = "";
        boxCount.text = "";
    }
}
