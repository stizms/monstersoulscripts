﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemBoxUI : MonoBehaviour
{
    #region INSPECTOR

    public Text titleText;
    public Text noticeText;
    public Text moneyText;
    
    // 초기 버튼 4개
    public MainItemBoxUI mainItemBoxUI;

    #endregion


    // 아이템박스 메인 UI 
    public void VisualizeItemBoxUI()
    {
        moneyText.text = UserDataMng.Ins.nowCharacter.money.ToString();
        mainItemBoxUI.VisualizeMainUI();
        gameObject.SetActive(true);
    }
    
    public void NonVisualizeItemBoxUI()
    {
        gameObject.SetActive(false);
        GameMng.Ins.CursorNonVisualize();
    }

    public void ChangeNoticeText(string str)
    {
        noticeText.text = str;
    }
}
