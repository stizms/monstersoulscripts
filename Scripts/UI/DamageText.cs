﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageText : MonoBehaviour
{
    #region INSPECTOR

    public Text damageText;
    public Vector3 target;
    #endregion

    public void SetData(Vector3 target, float damage, Color color)
    {
        this.target = target;
        damageText.text = damage.ToString();
        damageText.color = color;
        transform.position = Camera.main.WorldToScreenPoint(target);
    }

    public IEnumerator DamageTextRoutine()
    {
        gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
        transform.position = transform.parent.position;
    }

}
