﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestScroll : MonoBehaviour
{
    #region INSPECTOR

    public ScrollRect   scrollView;
    public Transform    content;
    public GameObject   questSlotPrefab;

    #endregion

    List<QuestNode> questList;
    List<QuestSlot> questSlotList = new List<QuestSlot>();

    // 슬롯 생성
    public void CreateQuestSlot()
    {
        questList = QuestMng.Ins.availableQuest.GetQuestList();
        if ( questList.Count != questSlotList.Count)
        {
            int num = questList.Count - questSlotList.Count;
            for(int i = 0; i < num; i++)
            {
                questSlotList.Add(Instantiate(questSlotPrefab, content).GetComponent<QuestSlot>());
            }
        }
        for (int i = questList.Count - 1; i >= 0; i--)
        {
            questSlotList[(questList.Count - 1) - i].SetData(questList[i]);
        }
    }

    public void VisualizeQuestSlots()
    {
        CreateQuestSlot();
        questSlotList[0].OnHoverEnter();
        scrollView.verticalNormalizedPosition = 1;
    }
}
