﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;
using System;

public class NPCQuestUI : MonoBehaviour,IInteractingUI
{
    #region INSPECTOR

    // 퀘스트 왼쪽 편
    public Text         rewardMoneyText;
    public Text         lifeCountText;
    public Text         questNameText;
    public Image        questTypeIcon;
    public Text         questTypeText;
    public Text         questDescription;

    // 퀘스트 오른쪽 편
    public Text         targetName;
    public Image        targetIcon;
    public GameObject   kindGroup;
    public Text         targetKind;
    public Text         hautedAreaText;

    // 약점 & 설명
    public Text         weaknessTitle;
    public Text         weaknessText;

    public QuestScroll  questScroll;
    #endregion

    // 퀘스트 UI 보여주기
    public void VisualizeQuestUI()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.QuestNpcOpen.ToDesc(), eChannel.UI_Effect);
        questScroll.VisualizeQuestSlots();
        gameObject.SetActive(true);
    }

    // 퀘스트 정보 바인딩
    public void BindQuestInfo(Quest questData)
    {
        rewardMoneyText.text    = questData.rewardMoney.ToString();
        lifeCountText.text      = questData.questLife.ToString();
        questNameText.text      = questData.questName;
        questTypeText.text      = questData.questType.ToDesc();
        questDescription.text   = questData.description;
        hautedAreaText.text     = TableMng.Ins.dungeonTb[questData.relatedDungeon].dungeonName;
        questTypeIcon.sprite    = AtlasMng.Ins.GetSprite(eAtlasCategory.Quest, questData.questType.ToString());

        switch (questData.questType)
        {
            case eQuestType.HuntQuest:
                kindGroup.SetActive(true);
                weaknessTitle.text  = "약점";
                weaknessText.transform.localPosition = new Vector3(194, 0, 0);
                MonsterData enemyData   = TableMng.Ins.monsterTb[(eMonsterID)Enum.Parse(typeof(eMonsterID),questData.mainTarget.ToString())];
                targetIcon.sprite       = AtlasMng.Ins.GetSprite(eAtlasCategory.Monster, enemyData.monsterID.ToString());
                targetName.text         = enemyData.monsterName;
                targetKind.text         = enemyData.monsterKind;
                weaknessText.text       = enemyData.weakness;

                break;
            case eQuestType.CollectionQuest:
                kindGroup.SetActive(false);
                weaknessTitle.text = "설명";
                weaknessText.transform.localPosition = new Vector3(16, -122, 0);
                ItemData tempCollection = TableMng.Ins.itemTb[(eItemID)Enum.Parse(typeof(eItemID), questData.mainTarget.ToString())];
                targetIcon.sprite       = AtlasMng.Ins.GetSprite(eAtlasCategory.Item, tempCollection.itemID.ToString());
                targetName.text         = tempCollection.itemName;
                weaknessText.text       = tempCollection.description;
                break;
        }
    }

    public void ExitUI()
    {
        GameMng.Ins.UnLock();

        UIMng.Ins.mouseInteractiveUI.gameObject.SetActive(true);
        UIMng.Ins.lobby.minimapUI.SetActive(true);
        if (QuestMng.Ins.nowQuest != null)
        {
            if (SceneMng.Ins.nowScene == eScene.Lobby)          UIMng.Ins.lobby.lobbyQuestUI.VisualizeQuestUI();
            else if (SceneMng.Ins.nowScene > eScene.Dungeon)    UIMng.Ins.QuestHudOn();
        }
        gameObject.SetActive(false);

        GameMng.Ins.CursorNonVisualize();
    }

    void Update()
    {
        if(!UIMng.Ins.CheckPopUpActive() && Input.GetMouseButtonDown(1))
            ExitUI();
    }
}
