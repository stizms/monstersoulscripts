﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyQuestUI : MonoBehaviour
{
    #region INSPECTOR

    public LobbyLeftQuestUI leftQuestUI;
    public RecieveQuestUI   recieveQuestUI;

    #endregion

    // 퀘스트 UI 키기
    public void VisualizeQuestUI()
    {
        leftQuestUI.gameObject.SetActive(true);
        UIMng.Ins.QuestHudOn();
    }

    // 퀘스트 UI 끄기
    public void NonVisualizeQuestUI()
    {
        leftQuestUI.gameObject.SetActive(false);
        UIMng.Ins.QuestHudOff();
    }
}
