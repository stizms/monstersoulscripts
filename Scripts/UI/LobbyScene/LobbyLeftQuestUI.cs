﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyLeftQuestUI : MonoBehaviour
{
    #region INSPECTOR

    public Text     questName;
    public Image    questTypeIcon;

    #endregion

    private bool isTabDown = false;

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        if(QuestMng.Ins.nowQuest != null)
        {
            questTypeIcon.sprite    = AtlasMng.Ins.GetSprite(eAtlasCategory.Quest, QuestMng.Ins.nowQuest.quest.questType.ToString());
            questName.text          = QuestMng.Ins.nowQuest.quest.questName;
        }
    }

    // 던전 이동
    private void LoadDungeonScene()
    {
        isTabDown = false;
        GameMng.Ins.UnLock();
        UIMng.Ins.lobby.TurnOffAllUI();
        UIMng.Ins.lobby.lobbyScene.blackBoard.SetActive(true);
        SceneMng.Ins.ChangeScene(QuestMng.Ins.nowQuest.quest.relatedDungeon);
    }

    private void PopUpExit()
    {
        isTabDown = false;
        GameMng.Ins.UnLock();
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab) && !GameMng.Ins.isInteracting && QuestMng.Ins.isQuestReady && !isTabDown)
        {
            isTabDown = true;
            UIMng.Ins.twoButtonPopUp.SetPopUp("퀘스트로 출발합니다.\n그렇게 하시겠습니까?", LoadDungeonScene, PopUpExit, "예", "아니요");
            GameMng.Ins.Lock();
        }
    }
}
