﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class LobbyUI : MonoBehaviour
{

    #region INSPECTOR

    public Image            blackBoard;
    public GameObject       minimapUI;
    public NPCQuestUI       questUI;
    public LobbyQuestUI     lobbyQuestUI;
    public ItemBoxUI        itemBoxUI;

    #endregion

    [HideInInspector]
    public GameObject       prevHover;

    // 아 이걸 씬 매니저가 가지고 있어야 하는데...
    [HideInInspector] 
    public LobbyScene       lobbyScene;

    void Awake()
    {
        UIMng.Ins.lobby = this;
        StartCoroutine(FadeOutBlackBord());
    }

    public IEnumerator FadeOutBlackBord()
    {
        GameMng.Ins.isInteracting = true;
        yield return new WaitForSeconds(1f);

        while (true)
        {
            blackBoard.color = Utill.ChangeMinusAlpha(blackBoard.color, 0.05f);

            if (blackBoard.color.a <= 0f)
            {
                blackBoard.gameObject.SetActive(false);
                minimapUI.SetActive(true);
                if (QuestMng.Ins.nowQuest != null)
                    UIMng.Ins.QuestHudOn();
                GameMng.Ins.UnLock();
                break;
            }
            yield return new WaitForSeconds(0.03f);
        }
    }

    public void HoverChange(GameObject hoverOverlay)
    {
        if (prevHover != null && prevHover != hoverOverlay)
            prevHover.SetActive(false);
        hoverOverlay.SetActive(true);
        prevHover = hoverOverlay;
    }

    // 모든 UI 끄기
    public void TurnOffAllUI()
    {
        lobbyQuestUI.NonVisualizeQuestUI();
        UIMng.Ins.mouseInteractiveUI.NonVisualizeInteractiveUI();
        UIMng.Ins.QuestHudOff();
        minimapUI.SetActive(false);
    }

    public void TurnOnAllUI()
    {
        if(QuestMng.Ins.nowQuest != null)
        {
            lobbyQuestUI.VisualizeQuestUI();
            UIMng.Ins.QuestHudOn();
        }
        if (lobbyScene.player.nearNPC != null)
            UIMng.Ins.mouseInteractiveUI.gameObject.SetActive(true);
        minimapUI.SetActive(true);
    }
}