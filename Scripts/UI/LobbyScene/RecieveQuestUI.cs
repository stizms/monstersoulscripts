﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecieveQuestUI : MonoBehaviour
{
    #region INSPECTOR

    public Image    questLogo;
    public Image    questIcon;
    public Text     questTitle;
    
    #endregion

    public void VisualizeRecieveUI(Quest quest)
    {
        questIcon.sprite    = AtlasMng.Ins.GetSprite(eAtlasCategory.Quest, quest.questType.ToString());
        questTitle.text     = quest.questName;
        gameObject.SetActive(true);
        StartCoroutine(QuestTitleFade());
    }

    IEnumerator QuestTitleFade()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.QuestRecieve.ToDesc(), eChannel.UI_Effect);

        while (true)
        {
            questLogo.color     = Utill.ChangePlusAlpha(questLogo.color, 0.05f);
            questIcon.color     = Utill.ChangePlusAlpha(questIcon.color, 0.05f);
            questTitle.color    = Utill.ChangePlusAlpha(questTitle.color, 0.05f);
            if (questTitle.color.a >= 1f)   break;
            yield return new WaitForSeconds(0.04f);
        }

        while (true)
        {
            questLogo.color     = Utill.ChangeMinusAlpha(questLogo.color, 0.05f);
            questIcon.color     = Utill.ChangeMinusAlpha(questIcon.color, 0.05f);
            questTitle.color    = Utill.ChangeMinusAlpha(questTitle.color, 0.05f);
            if (questTitle.color.a <= 0.01f)    break;
            yield return new WaitForSeconds(0.04f);
        }
        
        gameObject.SetActive(false);
        QuestMng.Ins.isQuestReady = true;
        GameMng.Ins.UnLock();
        UIMng.Ins.lobby.lobbyQuestUI.VisualizeQuestUI();
        UIMng.Ins.lobby.minimapUI.SetActive(true);
        UIMng.Ins.mouseInteractiveUI.gameObject.SetActive(true);
    }
}
