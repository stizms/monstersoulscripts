﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestSlot : HoverUI
{
    #region INSPECTOR

    public Image        questIcon;
    public Text         questTitle;
    public Image        hunterRankIcon;
    public Text         hunterRankText;
    public GameObject   clearImage;

    #endregion
    
    private LobbyUI     lobbyUI;
    private QuestNode   questData;
    private Color       customGray = new Color(0.58f, 0.58f, 0.58f);
    
    public void SetData(QuestNode questInfo)
    {
        // 외부에서 호출하는 함수가 있어 Awake보다 먼저 불림
        if (lobbyUI == null) lobbyUI = UIMng.Ins.lobby;

        questData           = questInfo;
        questIcon.sprite    = AtlasMng.Ins.GetSprite(eAtlasCategory.Quest, questData.quest.questType.ToString());
        questTitle.text     = questData.quest.questName;
        hunterRankText.text = questData.quest.hunterRank.ToString();

        if (questData.quest.complete)
        {
            questIcon.color         = customGray;
            questTitle.color        = customGray;
            hunterRankIcon.color    = customGray;
            hunterRankText.color    = customGray;
            clearImage.SetActive(true);
        }
    }
    

    public override void OnHoverEnter()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.PopUpHoverSound.ToDesc(), eChannel.UI_Effect);
        UIMng.Ins.lobby.HoverChange(hoverOverlay);
        BindQuestInfo();
    }

    // 퀘스트 정보 매칭
    private void BindQuestInfo()
    {
        lobbyUI.questUI.BindQuestInfo(questData.quest);
    }

    // 퀘스트 수주
    private void ReceiveQuest()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.QuestOkaySound.ToDesc(), eChannel.UI_Effect);
        QuestMng.Ins.SetQuest(questData);
        UIMng.Ins.questHud.Init();
        lobbyUI.questUI.gameObject.SetActive(false);
        lobbyUI.lobbyQuestUI.recieveQuestUI.VisualizeRecieveUI(questData.quest);

        GameMng.Ins.CursorNonVisualize();
    }

    // 퀘스트 포기
    private void GiveUpQuest()
    {
        QuestMng.Ins.GiveUpQuest();

        UIMng.Ins.questHud.ResetContent();
        lobbyUI.lobbyQuestUI.NonVisualizeQuestUI();
        lobbyUI.questUI.gameObject.SetActive(false);
        lobbyUI.minimapUI.SetActive(true);
        UIMng.Ins.mouseInteractiveUI.gameObject.SetActive(true);

        GameMng.Ins.CursorNonVisualize();
        GameMng.Ins.UnLock();
    }

    // 포기 후 새로운 퀘스트 수주
    private void GiveUpAndReceiveQuest()
    {
        GiveUpQuest();
        lobbyUI.minimapUI.SetActive(false);
        GameMng.Ins.Lock();
        ReceiveQuest();
    }

    public override void OnClickBtn()
    {
        if (questData.quest.complete)   return;
        else
        {
            if(QuestMng.Ins.nowQuest != null)
            {
                if(QuestMng.Ins.nowQuest == questData)
                    UIMng.Ins.twoButtonPopUp.SetPopUp("현재 퀘스트를 포기하시겠습니까?", GiveUpQuest, UIMng.Ins.OnPopupExit, "예", "아니요");
                else
                    UIMng.Ins.twoButtonPopUp.SetPopUp("현재 퀘스트를 포기하시고,\n새로운 퀘스트를 수주하시겠습니까?", GiveUpAndReceiveQuest, UIMng.Ins.OnPopupExit, "예", "아니요");
            }
            else
                UIMng.Ins.twoButtonPopUp.SetPopUp("이 퀘스트를 수주하시겠습니까?", ReceiveQuest, UIMng.Ins.OnPopupExit, "예", "아니요");
        }
    }
}