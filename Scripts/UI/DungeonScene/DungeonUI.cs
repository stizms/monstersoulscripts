﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
public class DungeonUI : MonoBehaviour
{

    #region INSPECTOR

    public Image                blackBoard;
    public GameObject           minimapUI;
    public Hourglass            hourglassUI;
    public GameObject           questClearLogo;
    public GameObject           questFailLogo;
    public QuestRewardUI        questRewardUI;
    public CharacterInfoUI      characterInfoUI;
    public GameObject           dungeonName;
    public BossName             bossName;

    #endregion

    [HideInInspector]
    public GameObject   prevHover;

    private AlarmHud    alarmHud;
    private LootingHud  lootingHud;
    void Awake()
    {
        UIMng.Ins.dungeon = this;
        alarmHud    = UIMng.Ins.alarmHud;
        lootingHud  = UIMng.Ins.lootingHud;
        StartCoroutine(FadeOutBlackBord());
    }

    public void HoverChange(GameObject hoverOverlay)
    {
        if (prevHover != null && prevHover != hoverOverlay)
            prevHover.SetActive(false);
        hoverOverlay.SetActive(true);
        prevHover = hoverOverlay;
    }

    //퀘스트 클리어 시점부터 타임체크
    public void StartRemainTimeCheck()
    {
        StartCoroutine(QuestClearMentionCoroutine());
        hourglassUI.Visualize();
    }


    IEnumerator QuestClearMentionCoroutine()
    {
        UIMng.Ins.VisualizeMention("메인 타겟을 달성하였습니다.");
        yield return new WaitForSeconds(10f);
        UIMng.Ins.NonVisualizeMention();
    }

    public void QuestClearLogoFade()
    {
        GameMng.Ins.Lock();
        StartCoroutine(QuestLogoCoroutine(questClearLogo,eQuestResultCategory.Clear));
    }

    public void QuestFailLogoFade()
    {
        GameMng.Ins.Lock();
        StartCoroutine(QuestLogoCoroutine(questFailLogo,eQuestResultCategory.Fail));
    }

    IEnumerator QuestLogoCoroutine(GameObject questLogo, eQuestResultCategory questResultCategory)
    {
        questLogo.SetActive(true);
        yield return new WaitForSeconds(5f);
        questLogo.SetActive(false);
        StartCoroutine(QuestEndFadeIn(questResultCategory));
    }

    
    // 모든 UI 끄기
    public void TurnOffAllUI()
    {
        alarmHud.ResetHud();
        minimapUI.SetActive(false);
        hourglassUI.NonVisualize();
        characterInfoUI.NonVisualize();
        if(lootingHud.nowLootingHud != null)
            lootingHud.nowLootingHud.gameObject.SetActive(false);
        UIMng.Ins.QuestHudOff();
        UIMng.Ins.NonVisualizeMention();
    }

    public void TurnOnAllUI()
    {
        minimapUI.SetActive(true);
        characterInfoUI.gameObject.SetActive(true);
        if(QuestMng.Ins.nowQuest.quest.complete)
            hourglassUI.gameObject.SetActive(true);
        if(lootingHud.nowLootingHud != null)
            lootingHud.nowLootingHud.gameObject.SetActive(true);
        UIMng.Ins.QuestHudOn();
    }

    #region FadeInAndOut Func

    // 던전 첫 입장시
    public IEnumerator FadeOutBlackBord()
    {
        bool isDungeonName = false;

        GameMng.Ins.isInteracting = true;
        // 1초 대기후 FadeOut;
        yield return new WaitForSeconds(1f);

        while (true)
        {
            blackBoard.color = Utill.ChangeMinusAlpha(blackBoard.color, 0.05f);
            if(blackBoard.color.a <= 0.5f && !isDungeonName)
            {
                isDungeonName = true;
                dungeonName.SetActive(true);
            }

            if (blackBoard.color.a <= 0f)
            {
                blackBoard.gameObject.SetActive(false);
                minimapUI.SetActive(true);
                if (QuestMng.Ins.nowQuest != null)
                    UIMng.Ins.QuestHudOn();
                characterInfoUI.Visualize();
                GameMng.Ins.UnLock();
                break;
            }
            yield return new WaitForSeconds(0.03f);
        }
    }

    // 퀘스트 클리어 시
    public IEnumerator QuestEndFadeIn(eQuestResultCategory questResultCategory)
    {
        blackBoard.gameObject.SetActive(true);
        while (true)
        {
            blackBoard.color = Utill.ChangePlusAlpha(blackBoard.color, 0.05f);

            if (blackBoard.color.a >= 1f)
            {
                yield return new WaitForSeconds(0.3f);
                questRewardUI.Visualize(questResultCategory);
                break;
            }
            yield return new WaitForSeconds(0.03f);
        }
    }

    // 죽을 시
    public IEnumerator DeathFadeIn()
    {
        Camera.main.GetComponent<CinemachineBrain>().enabled = false;
        UIMng.Ins.VisualizeMention("힘이 다했습니다.");
        blackBoard.gameObject.SetActive(true);
        if(QuestMng.Ins.deathCount >= QuestMng.Ins.nowQuest.quest.questLife)
        {
            yield return new WaitForSeconds(1f);
            alarmHud.AddMentionAlarm("기회를 모두 소진하였습니다.");
            yield return new WaitForSeconds(1.5f);
            alarmHud.AddMentionAlarm("귀환합니다.");
            yield return new WaitForSeconds(1.5f);
            UIMng.Ins.NonVisualizeMention();
            alarmHud.ResetHud();
            QuestFailLogoFade();
        }
        else
        {
            yield return new WaitForSeconds(2f);
            while (true)
            {
                blackBoard.color = Utill.ChangePlusAlpha(blackBoard.color, 0.05f);

                if (blackBoard.color.a >= 1f)
                {
                    yield return new WaitForSeconds(0.3f);
                    UIMng.Ins.NonVisualizeMention();
                    DungeonMng.Ins.Revive();
                    StartCoroutine(ReviveFadeOut());
                    break;
                }
                yield return new WaitForSeconds(0.03f);
            }
        }
    }

    public IEnumerator ReviveFadeOut()
    {
        SoundMng.Ins.isBossBGM = false;
        SoundMng.Ins.ExternalSoundPlay(eSound.DungeonBGM.ToDesc(), eChannel.BackGround);
        while (true)
        {
            blackBoard.color = Utill.ChangeMinusAlpha(blackBoard.color, 0.05f);

            if (blackBoard.color.a <= 0f)
            {
                blackBoard.gameObject.SetActive(false);
                DungeonMng.Ins.dungeonScene.player.playerAnimator.SetTrigger("Revive");
                break;
            }
            yield return new WaitForSeconds(0.03f);
        }
    }

    #endregion
}