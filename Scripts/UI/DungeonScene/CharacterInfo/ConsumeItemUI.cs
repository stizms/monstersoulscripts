﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsumeItemUI : MonoBehaviour
{
    #region INSPECTOR

    public Image selectedItemIcon;
    public Image nextItemIcon;
    public Image prevItemIcon;
    public Text  itemNameText;
    public Text  itemAmountText;

    #endregion

    public  ConsumeLinkedList   consumeList = null;
    public  Character           character = null;
    private UserData            userData = null;
    
    public int index = 0;               // 현재 슬롯인덱스
    bool mouseWheelFlag = false;        // 마우스 휠 움직임 통제 플래그 ( false면 할 수 있음, true면 사용 중 )
    float wheelValue = 0f;
    float wheelDelay = 0.15f;

    public bool fixedIndexFlag = false; // index통제 플래그 ( 아이템을 사용하고 나서 인덱스가 땡겨지는데 그때 2칸 이동한 것처럼 안보이게 하기 위함 )

    public void Visualize()
    {
        if (character == null)
        {
            character = DungeonMng.Ins.dungeonScene.player;
            userData = UserDataMng.Ins.nowCharacter;
            consumeList = new ConsumeLinkedList();
            foreach (var node in userData.characterInventory.itemList)
            {
                if (TableMng.Ins.itemTb[node.Key].itemType == eItemType.Consume)
                {
                    consumeList.AddLast(node.Key);
                }
            }
        }

        UpdateConsumeUI();
        gameObject.SetActive(true);
    }

    public void NonVisualize()
    {
        gameObject.SetActive(false);
    }

    public void Update()
    {
        if(!mouseWheelFlag)
        {
            wheelValue = Input.GetAxis("Mouse ScrollWheel");
            if (wheelValue > 0.1f && !mouseWheelFlag)
            {
                mouseWheelFlag = true;
                if(!fixedIndexFlag)
                    index--;
                fixedIndexFlag = false;
                if (index < 0)
                    index = consumeList.count - 1;
                else if (index > consumeList.count - 1)
                    index = 0;
                StartCoroutine(WheelConsumeUI());
            }
            else if (wheelValue < -0.1f && !mouseWheelFlag)
            {
                mouseWheelFlag = true;
                if(!fixedIndexFlag)
                    index++;
                fixedIndexFlag = false;
                if (index < 0)
                    index = consumeList.count - 1;
                else if (index > consumeList.count - 1)
                    index = 0;
                StartCoroutine(WheelConsumeUI());
            }
        }
    }

    IEnumerator WheelConsumeUI()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.ConsumeSlotMove.ToDesc(), eChannel.UI_Effect);
        UpdateConsumeUI();
        yield return new WaitForSeconds(wheelDelay);
        mouseWheelFlag = false;
    }

    private void UpdateConsumeUI()
    {
        ConsumeNode tempNode = consumeList.SearchSelectedItemNode(index);
        character.selectedItem = tempNode.nodeItem;

        // 선택 된 슬롯
        if(tempNode.nodeItem == eItemID.None)
        {
            selectedItemIcon.gameObject.SetActive(false);
            itemNameText.text   = "";
            itemAmountText.text = "";
        }
        else if(tempNode.nodeItem == eItemID.Whetstone)
        {
            selectedItemIcon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Item, tempNode.nodeItem.ToString());
            selectedItemIcon.gameObject.SetActive(true);
            itemNameText.text   = TableMng.Ins.itemTb[tempNode.nodeItem].itemName;
            itemAmountText.text = "∞";
        }
        else
        {
            selectedItemIcon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Item, tempNode.nodeItem.ToString());
            selectedItemIcon.gameObject.SetActive(true);
            itemNameText.text   = TableMng.Ins.itemTb[tempNode.nodeItem].itemName;
            itemAmountText.text = userData.characterInventory.GetItemCount(tempNode.nodeItem).ToString();
        }

        // 오른쪽
        if (tempNode.nextNode.nodeItem == eItemID.None)
            nextItemIcon.gameObject.SetActive(false);
        else
        {
            nextItemIcon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Item, tempNode.nextNode.nodeItem.ToString());
            nextItemIcon.gameObject.SetActive(true);
        }

        // 왼쪽
        if (tempNode.prevNode.nodeItem == eItemID.None)
            prevItemIcon.gameObject.SetActive(false);
        else
        {
            prevItemIcon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Item, tempNode.prevNode.nodeItem.ToString());
            prevItemIcon.gameObject.SetActive(true);
        }
    }
    
    public void UpdateItemAmountUI()
    {
        itemAmountText.text = userData.characterInventory.GetItemCount(character.selectedItem).ToString();
    }

    public void ResetSelectedSlot()
    {
        selectedItemIcon.gameObject.SetActive(false);
        itemAmountText.text = "";
        itemNameText.text   = "";
    }
}

public class ConsumeNode
{
    public eItemID nodeItem;
    public ConsumeNode prevNode;
    public ConsumeNode nextNode;

    public ConsumeNode() { nodeItem = eItemID.None; prevNode = null; nextNode = null; }
    public ConsumeNode(eItemID itemID) { nodeItem = itemID; prevNode = null; nextNode = null; }
}

public class ConsumeLinkedList
{
    private ConsumeNode head  = null;
    private ConsumeNode tail  = null;
    public  int         count = 0;

    public ConsumeLinkedList()
    {
        head = new ConsumeNode();
        tail = new ConsumeNode(eItemID.Whetstone);

        head.prevNode = tail;
        head.nextNode = tail;

        tail.prevNode = head;
        tail.nextNode = head;

        count = 2;
    }

    public void AddLast(eItemID itemID)
    {
        ConsumeNode tempNode = new ConsumeNode(itemID);
        tempNode.prevNode = tail.prevNode;
        tempNode.nextNode = tail;

        tail.prevNode.nextNode = tempNode;
        tail.prevNode = tempNode;

        count++;
    }

    public void RemoveNode(int index)
    {
        ConsumeNode tempNode = head;

        // 현재 슬롯 찾음
        for(int i = 0; i < index; i++)
        {
            tempNode = tempNode.nextNode;
        }

        tempNode.prevNode.nextNode = tempNode.nextNode;
        tempNode.nextNode.prevNode = tempNode.prevNode;
        tempNode = null;

        count--;
    }

    public ConsumeNode SearchSelectedItemNode(int index)
    {
        ConsumeNode tempNode = head;

        for(int i = 0; i < index; i++)
        {
            tempNode = tempNode.nextNode;
        }

        return tempNode;
    }
}
