﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SharpnessUI : MonoBehaviour
{
    #region INSPECTOR

    public Image currentSharpnessGaze;
    public float filledTime = 0.5f;

    #endregion

    private BaseWeapon nowWeapon;

    public void Visualize()
    {
        if(nowWeapon == null)
            nowWeapon = DungeonMng.Ins.dungeonScene.player.equipWeapon;

        UpdateSharpnessGaze();
        gameObject.SetActive(true);
    }

    public void NonVisualize()
    {
        gameObject.SetActive(false);
    }

    public void UpdateSharpnessGaze()
    {
        // 0이 될 경우 빨간색이 한칸 남아있게 함
        if (nowWeapon.currentSharpness > 0)
        {
            // 10은 각 예리도 마다의 할당량 ( 10의 텀으로 예리도가 바뀜 )
            float currentSharpness = nowWeapon.currentSharpness % 10;
            if (currentSharpness == 0)
            {
                ChangeSharpnessImage();
                currentSharpness = 10f;
            }
            currentSharpnessGaze.fillAmount = currentSharpness * 0.1f;
        }
    }

    public void GazeUpdateWithWhetStone()
    {
        ChangeSharpnessImage();
        StartCoroutine(FilledGaze());
    }

    private void ChangeSharpnessImage()
    {
        if (nowWeapon.currentSharpness <= 10)
            currentSharpnessGaze.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.CharacterInfo, "RedSharpness");
        else if (nowWeapon.currentSharpness > 10 && nowWeapon.currentSharpness <= 20)
            currentSharpnessGaze.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.CharacterInfo, "OrangeSharpness");
        else if (nowWeapon.currentSharpness > 20 && nowWeapon.currentSharpness <= 30)
            currentSharpnessGaze.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.CharacterInfo, "YellowSharpness");
        else if (nowWeapon.currentSharpness > 30 && nowWeapon.currentSharpness <= 40)
            currentSharpnessGaze.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.CharacterInfo, "GreenSharpness");
        else if (nowWeapon.currentSharpness > 40 && nowWeapon.currentSharpness <= 50)
            currentSharpnessGaze.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.CharacterInfo, "BlueSharpness");
        else if (nowWeapon.currentSharpness > 50 && nowWeapon.currentSharpness <= 60)
            currentSharpnessGaze.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.CharacterInfo, "WhiteSharpness");
        else if (nowWeapon.currentSharpness > 60 && nowWeapon.currentSharpness <= 70)
            currentSharpnessGaze.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.CharacterInfo, "PurpleSharpness");
        else
            Debug.Log("MAX_Sharpness를 초과했습니다.");
    }


    // 차는 모션
    IEnumerator FilledGaze()
    {
        float targetSharpness = 0f;
        while(true)
        {
            currentSharpnessGaze.fillAmount += filledTime * Time.deltaTime;
            yield return null;
            targetSharpness = nowWeapon.currentSharpness % 10;
            if (targetSharpness == 0)  targetSharpness = 10;
            targetSharpness *= 0.1f;
            if(currentSharpnessGaze.fillAmount >= targetSharpness)
            {
                currentSharpnessGaze.fillAmount = targetSharpness;
                break;
            }
        }
    }
}
