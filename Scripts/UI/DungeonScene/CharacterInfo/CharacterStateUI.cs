﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStateUI : MonoBehaviour
{
    #region INSPECTOR

    public Text     characterName;
    // Hp
    public Image    hpFrame;
    public Image    greenHpImage;
    public Image    redHpImage;
    public Image    whiteHpImage;
    //Stamina
    public Image    staminaFrame;
    public Image    staminaGaze;

    #endregion

    private Character   character;
    private float       perHp;      // fillAmount 0.01당 차지하는 Hp의 양
    private float       perStamina; // fillAmount 0.01당 차지하는 Stamina의 양
    
    public void Visualize()
    {
        if(character == null)
        {
            character   = DungeonMng.Ins.dungeonScene.player;
            perHp       = Define.MAX_HP / 100f;
            perStamina  = Define.MAX_STAMINA / 100f;
        }
        characterName.text              = character.characterInfo.characterName;
        float hpStartFillAmount         = (character.currentMaxHp / perHp) * 0.01f;
        float staminaStartFillAmount    = (character.currentMaxStamina / perStamina) * 0.01f;

        hpFrame.fillAmount      = hpStartFillAmount;
        greenHpImage.fillAmount = hpStartFillAmount;
        staminaFrame.fillAmount = staminaStartFillAmount;
        staminaGaze.fillAmount  = staminaStartFillAmount;

        gameObject.SetActive(true);
    }

    public void GetDamaged()
    {
        TurnOffAnotherGaze();
        if (character.currentHp > 0f)
        {
            redHpImage.fillAmount   = (character.recoveryHp / perHp) * 0.01f;
            greenHpImage.fillAmount = (character.currentHp / perHp) * 0.01f;
            redHpImage.gameObject.SetActive(true);
        }
        else
        {
            redHpImage.gameObject.SetActive(false);
            greenHpImage.fillAmount = 0f;
        }
    }

    public void GetRecovery(float recovery)
    {
        TurnOffAnotherGaze();
        float recoveryAmount = 0f;
        if (character.currentHp + recovery > character.currentMaxHp)
            recoveryAmount = character.currentMaxHp;
        else
            recoveryAmount = (character.currentHp + recovery);
        whiteHpImage.fillAmount = (recoveryAmount / perHp) * 0.01f;
        greenHpImage.fillAmount = (character.currentHp / perHp) * 0.01f;
        whiteHpImage.gameObject.SetActive(true);
    }

    public void UpdateGreenHp()
    {
        if (greenHpImage != null)
            greenHpImage.fillAmount = (character.currentHp / perHp) * 0.01f;
    }

    public void UpdateStaminaGaze()
    {
        if(staminaGaze != null)
            staminaGaze.fillAmount = (character.currentStamina / perStamina) * 0.01f;
    }

    public void TurnOffAnotherGaze()
    {
        redHpImage.gameObject.SetActive(false);
        whiteHpImage.gameObject.SetActive(false);
    }

    public void GetGrowMaxHp()
    {
        hpFrame.fillAmount = (character.currentMaxHp / perHp) * 0.01f;
    }

    public void GetGrowMaxStamina()
    {
        staminaFrame.fillAmount = (character.currentMaxStamina / perStamina) * 0.01f;
    }
}
