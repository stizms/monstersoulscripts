﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInfoUI : MonoBehaviour
{
    #region INSPECTOR

    public CharacterStateUI characterStateUI;
    public ConsumeItemUI    consumeItemUI;
    public SharpnessUI      sharpnessUI;

    #endregion

    public void Visualize()
    {
        characterStateUI.Visualize();
        consumeItemUI.Visualize();
        sharpnessUI.Visualize();
        gameObject.SetActive(true);
    }

    public void NonVisualize()
    {
        gameObject.SetActive(false);
    }
}
