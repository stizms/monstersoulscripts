﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossName : MonoBehaviour
{
    #region INSPECTOR

    public Image    bossIcon;
    public Text     bossKind;
    public Text     bossName;

    #endregion

    public void SetData(string iconSpriteName, string kind, string name)
    {
        bossIcon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Monster, iconSpriteName);
        bossKind.text = kind;
        bossName.text = name;
    }
}
