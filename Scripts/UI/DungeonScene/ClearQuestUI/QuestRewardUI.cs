﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class QuestRewardUI : MonoBehaviour
{
    #region INSPECTOR

    public DungeonItemUI    dungeonItemUI;
    public RewardUI         rewardUI;
    public Text             moneyText;
    public GameObject       moneyObject;
    public Image            backGroundScreenShot;

    #endregion

    public void Visualize(eQuestResultCategory questResultCategory)
    {
        // 세팅
        string pngName = "";
        if (questResultCategory == eQuestResultCategory.Clear)
            pngName = "ClearMoment.png";
        else if (questResultCategory == eQuestResultCategory.Fail)
            pngName = "FailMoment.png";
        Texture2D tex = LoadPNG(string.Format("{0}{1}{2}", Application.dataPath, PATH.ScreenShotPath, pngName));
        Sprite result = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
        backGroundScreenShot.sprite = result;
        backGroundScreenShot.gameObject.SetActive(true);
        moneyText.text = UserDataMng.Ins.nowCharacter.money.ToString();
        gameObject.SetActive(true);

        dungeonItemUI.Visualize();
    }

    private Texture2D LoadPNG(string path)
    {
        Texture2D result = null;
        byte[] bytes;

        if (File.Exists(path))
        {
            bytes = File.ReadAllBytes(path);
            result = new Texture2D(Screen.width, Screen.height);
            result.LoadImage(bytes);
        }
        return result;
    }
}
