﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestLevelHandler : MonoBehaviour
{
    #region INSPECTOR

    public List<Image> questLevelStars = new List<Image>(); 

    #endregion

    public void SetLevelData(int hunterRank)
    {
        int count = hunterRank % 10;
        string spriteName = "";
        if (hunterRank <= 10)
            spriteName = "HR";
        for(int i = 0; i < count; i++)
        {
            questLevelStars[i].sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Quest, spriteName);
            questLevelStars[i].gameObject.SetActive(true);
        }
    }
}
