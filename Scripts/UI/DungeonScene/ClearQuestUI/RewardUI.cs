﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardUI : MonoBehaviour
{
    #region INSPECTOR

    public Text         resultMoneyText;
    public Text         originMoneyText;
    public Text         deductMoneyText;
    public Text         hunterRankText;
    public GameObject   rewardSlot;
    public GameObject   rewardShape;
    public GameObject   rewardBillSlot;
    public GameObject   hunterRankSlot;
    public GameObject   description;

    #endregion

    private int      rewardMoney     = 0;
    private int      deductMoney     = 0;
    private int      resultMoney     = 0;
    private bool     mouseClickFlag  = false;   // 모든 UI가 뜨고 로비로 돌아갈 수 있게 클릭 여부를 받아드리는 플래그
    private Quest    quest           = null;
    private UserData userData        = null;

    public void Visualize()
    {
        quest           = QuestMng.Ins.nowQuest.quest;
        userData        = UserDataMng.Ins.nowCharacter;
        int deathCount  = QuestMng.Ins.deathCount;

        rewardMoney     = quest.rewardMoney;
        if (quest.questLife <= deathCount) deductMoney = rewardMoney;
        else
            deductMoney = ((int)((float)rewardMoney / quest.questLife) * deathCount);   // (보수 / 최대기회) * 죽은 횟수

        resultMoney = rewardMoney - deductMoney;
        
        originMoneyText.text = rewardMoney.ToString();
        deductMoneyText.text = deductMoney.ToString();
        resultMoneyText.text = resultMoney.ToString();
        if (quest.rankUpFlag && quest.complete)
            userData.hunterRank++;
        hunterRankText.text = userData.hunterRank.ToString();
        gameObject.SetActive(true);
        StartCoroutine(RewardAction());
    }

    IEnumerator RewardAction()
    {
        rewardSlot.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        rewardShape.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        rewardBillSlot.SetActive(true);
        userData.money += resultMoney;
        SoundMng.Ins.ExternalSoundPlay(eSound.MoneyChange.ToDesc(), eChannel.UI_Effect);
        UIMng.Ins.dungeon.questRewardUI.moneyText.text = userData.money.ToString();

        // 퀘스트 클리어 시에만 다음 퀘스트가 열림
        if (quest.complete)
            QuestMng.Ins.nowQuest.OpenQuest();

        QuestMng.Ins.nowQuest = null;
        UIMng.Ins.questHud.ResetContent();
        UIMng.Ins.QuestHudOff();
        yield return new WaitForSeconds(0.3f);
        hunterRankSlot.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        description.SetActive(true);
        mouseClickFlag = true;

    }

    IEnumerator SaveData()
    {
        UIMng.Ins.SaveLogoOn();
        UserDataMng.Ins.SaveUserData(userData);
        yield return new WaitForSeconds(2f);    // 문구 띄우고 문구를 어느정도는 보여주기 위함
        UIMng.Ins.SaveLogoOff();
        StartCoroutine(ReturnToLobbyScene());
    }

    IEnumerator ReturnToLobbyScene()
    {
        Image blackBoard = UIMng.Ins.dungeon.blackBoard;
        blackBoard.gameObject.SetActive(true);
        while (true)
        {
            blackBoard.color = Utill.ChangePlusAlpha(blackBoard.color, 0.05f);
            if (blackBoard.color.a >= 1f)
            {
                DungeonMng.Ins.dungeonScene.blackBoard.SetActive(true);
                yield return new WaitForSeconds(0.3f);
                SceneMng.Ins.ChangeScene(eScene.Lobby);
                break;
            }
            yield return new WaitForSeconds(0.03f);
        }
    }

    public void Update()
    {
        if(mouseClickFlag)
        {
            if(Input.GetMouseButtonDown(0))
            {
                mouseClickFlag = false;
                GameMng.Ins.isInteracting = false;
                StartCoroutine(SaveData());
            }
        }
    }


}
