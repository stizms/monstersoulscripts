﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hourglass : MonoBehaviour
{
    #region INSPECTOR

    public Image hourglassGaze;
    public Text remainingTimeText;
    public float remainingTime = 30f;

    #endregion

    private float timeChecker = 0f;
    private float gazeFillAmount = 1f;

    private void Init()
    {
        gazeFillAmount = 1f;
        hourglassGaze.fillAmount = gazeFillAmount;
        remainingTime = 30f;
        remainingTimeText.text = remainingTime.ToString();
    }

    public void Visualize()
    {
        Init();
        gameObject.SetActive(true);
    }

    public void NonVisualize()
    {
        gameObject.SetActive(false);
    }

    private void Update()
    {
        timeChecker += Time.deltaTime;
        if(timeChecker > 1f)
        {
            timeChecker = 0f;
            if (remainingTime > 0f)
            {
                gazeFillAmount -= 0.1f;
                if (remainingTime == 1f)
                    gazeFillAmount = 0f;
                else if (gazeFillAmount <= 0f)
                    gazeFillAmount = 1f;

                hourglassGaze.fillAmount = gazeFillAmount;
                remainingTime--;
                remainingTimeText.text = remainingTime.ToString();
            }
            else
            {
                NonVisualize();
                UIMng.Ins.dungeon.TurnOffAllUI();
                UIMng.Ins.dungeon.QuestClearLogoFade();
            }
        }
    }
}
