﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DungeonItemUI : MonoBehaviour
{
    #region INSPECTOR
    
    public Text                 questName;
    public QuestLevelHandler    questLevelHandler;
    public DungeonItemBoxUI     dungeonItemBox;
    public Image                blackBoard;
    #endregion

    public void Visualize()
    {
        // 퀘스트 정보
        questName.text = QuestMng.Ins.nowQuest.quest.questName;
        questLevelHandler.SetLevelData(QuestMng.Ins.nowQuest.quest.hunterRank);

        // 아이템 정보
        if(DungeonMng.Ins.dungeonItemList.itemList.Count > 0)
            dungeonItemBox.SetSlotsData();
        else
        {
            dungeonItemBox.allUnionButton.SetActive(false);
            dungeonItemBox.allSellButton.SetActive(false);
            dungeonItemBox.exitButton.gameObject.SetActive(true);
            dungeonItemBox.exitButton.OnHoverEnter();
        }

        gameObject.SetActive(true);
        StartCoroutine(FadeOutBlackBord());
    }

    public void NonVisualize()
    {
        gameObject.SetActive(false);
    }

    public IEnumerator FadeOutBlackBord()
    {
        // 1초 대기후 FadeOut;
        yield return new WaitForSeconds(1f);

        while (true)
        {
            blackBoard.color = Utill.ChangeMinusAlpha(blackBoard.color, 0.05f);

            if (blackBoard.color.a <= 0f)
            {
                GameMng.Ins.CursorVisualize();
                blackBoard.gameObject.SetActive(false);
                break;
            }
            yield return new WaitForSeconds(0.03f);
        }
    }
}
