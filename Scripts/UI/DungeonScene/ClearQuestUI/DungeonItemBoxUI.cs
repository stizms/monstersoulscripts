﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonItemBoxUI : MonoBehaviour
{
    #region INSPECTOR

    public List<DungeonInventorySlot> dungeonItemSlots = new List<DungeonInventorySlot>();
    public ItemInfoUI   itemInfoUI;
    public ButtonHover  exitButton;
    public GameObject   allUnionButton;
    public GameObject   allSellButton;

    #endregion

    private QuestRewardUI questRewardUI;

    public void SetSlotsData()
    {
        if (questRewardUI == null) questRewardUI = UIMng.Ins.dungeon.questRewardUI;

        int index = 0;
        foreach(var node in DungeonMng.Ins.dungeonItemList.itemList)
        {
            dungeonItemSlots[index].SetData(node.Key, node.Value);
            index++;
        }
        dungeonItemSlots[0].OnHoverEnter();
    }

    public void ResetSlots()
    {
        foreach(var node in dungeonItemSlots)
        {
            node.ResetSlot();
        }
    }

    public void OnAllUnionBtn()
    {
        UIMng.Ins.twoButtonPopUp.SetPopUp("모두 받습니다.\n그렇게 하시겠습니까?", AllUnionAction, UIMng.Ins.OnPopupExit, "예", "아니요");
    }

    public void AllUnionAction()
    {
        DungeonMng.Ins.dungeonItemList.UnionToAllItemBox();
        foreach(var node in dungeonItemSlots)
        {
            node.ResetSlot();
        }
        questRewardUI.dungeonItemUI.NonVisualize();
        questRewardUI.rewardUI.Visualize();
    }

    public void OnAllSellBtn()
    {
        int cost = 0;
        foreach (var node in DungeonMng.Ins.dungeonItemList.itemList)
        {
            cost += (TableMng.Ins.itemTb[node.Key].sellPrice * node.Value);
        }
        UIMng.Ins.twoButtonPopUp.SetPopUp(string.Format("{0}z에 매각합니다.\n그렇게 하시겠습니까?",cost), AllSellAction, UIMng.Ins.OnPopupExit, "예", "아니요");
    }

    public void AllSellAction()
    {
        DungeonMng.Ins.dungeonItemList.SellAllItemBox();
        foreach (var node in dungeonItemSlots)
        {
            node.ResetSlot();
        }
        SoundMng.Ins.ExternalSoundPlay(eSound.MoneyChange.ToDesc(), eChannel.UI_Effect);
        questRewardUI.moneyText.text = UserDataMng.Ins.nowCharacter.money.ToString();
        questRewardUI.dungeonItemUI.NonVisualize();
        questRewardUI.rewardUI.Visualize();
    }

    public void OnExitBtn()
    {
        SoundMng.Ins.ExternalSoundPlay(eSound.QuestExitSound.ToDesc(), eChannel.UI_Effect);
        questRewardUI.dungeonItemUI.NonVisualize();
        questRewardUI.rewardUI.Visualize();
    }
}
