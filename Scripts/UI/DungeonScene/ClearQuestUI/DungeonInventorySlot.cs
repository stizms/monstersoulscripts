﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DungeonInventorySlot : InventorySlot
{
    private DungeonItemBoxUI dungeonItemBox;

    public override void OnHoverEnter()
    {
        if(dungeonItemBox == null ) dungeonItemBox = UIMng.Ins.dungeon.questRewardUI.dungeonItemUI.dungeonItemBox;

        if (UIMng.Ins.dungeon.prevHover == hoverOverlay) return;
        SoundMng.Ins.ExternalSoundPlay(eSound.PopUpHoverSound.ToDesc(), eChannel.UI_Effect);
        UIMng.Ins.dungeon.HoverChange(hoverOverlay);
        dungeonItemBox.itemInfoUI.VisualizeItemInfo(slotItem);
    }

    public override void OnClickBtn()
    {
        if (slotItem == eItemID.None) return;

        DungeonItemBoxUI dungeonItemBox = UIMng.Ins.dungeon.questRewardUI.dungeonItemUI.dungeonItemBox;
        DungeonMng.Ins.dungeonItemList.UnionToItemBox(slotItem, int.Parse(itemAmountText.text));
        slotItem    = eItemID.None;
        itemAmount  = 0;
        itemAmountText.text = "";
        itemIcon.gameObject.SetActive(false);

        dungeonItemBox.itemInfoUI.SetEmptyInfo();
        if (DungeonMng.Ins.dungeonItemList.itemList.Count <= 0)
        {
            dungeonItemBox.allUnionButton.SetActive(false);
            dungeonItemBox.allSellButton.SetActive(false);
            dungeonItemBox.exitButton.gameObject.SetActive(true);
            dungeonItemBox.exitButton.OnHoverEnter();
        }
    }
}
