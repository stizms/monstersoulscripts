﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DungeonName : MonoBehaviour
{
    #region INSPECTOR

    public Image    dungeonIcon;
    public Text     dungeonName;

    #endregion

    private void Start()
    {
        eScene nowScene = SceneMng.Ins.nowScene;
        dungeonIcon.sprite = AtlasMng.Ins.GetSprite(eAtlasCategory.Dungeon, nowScene.ToString());
        dungeonName.text = TableMng.Ins.dungeonTb[nowScene].dungeonName;
        StartCoroutine(DungeonNameCoroutine());
    }

    IEnumerator DungeonNameCoroutine()
    {
        while (true)
        {
            dungeonIcon.color = Utill.ChangePlusAlpha(dungeonIcon.color, 0.03f);
            dungeonName.color = Utill.ChangePlusAlpha(dungeonName.color, 0.03f);
            if(dungeonName.color.a >= 0.75f)
                break;
            yield return new WaitForSeconds(0.03f);
        }

        yield return new WaitForSeconds(1f);

        while(true)
        {
            dungeonIcon.color = Utill.ChangeMinusAlpha(dungeonIcon.color, 0.03f);
            dungeonName.color = Utill.ChangeMinusAlpha(dungeonName.color, 0.03f);
            if (dungeonName.color.a <= 0f)
                break;
            yield return new WaitForSeconds(0.03f);
        }

        gameObject.SetActive(false);
    }
}
